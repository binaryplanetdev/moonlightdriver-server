/***
 * MoonlightDriver Server
 * 달빛기사 앱을 위한 node.js 서버입니다.
 */

global.rootPath		= __dirname;
global.config		= require(rootPath + '/inc/config');
var cluster			= require('cluster');
var numCPUs			= require('os').cpus().length;

var Logger			= require(rootPath + '/lib/Logger');
var SVRUtils		= require(rootPath + '/lib/SVRUtils');
var PushMessage		= require(rootPath + '/lib/PushMessage');

global.logger		= new Logger(config.LOG.FILE.INFO, config.LOG.FILE.ERROR);
global.svr_utils	= new SVRUtils();

//Exception Handler 등록
process.on("uncaughtException", function (err) {
	logger.error("uncaughtException: " + err.stack);
});

String.prototype.startsWith = function( str ) {
	return this.substring(0, str.length) === str;
};

String.prototype.endsWith = function( str ) {
	return this.substring( this.length - str.length, this.length ) === str;
};

Date.prototype.getWeekOfYear = function() {
    var onejan = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
}

if (cluster.isMaster) {	
	for(var i = 0; i < numCPUs; i++) {
		cluster.fork();
	}
	
	cluster.on('online', function (_worker) {
		logger.info("[worker #" + _worker.process.pid + "] Created.");
	});

	cluster.on("exit", function(_worker, _code) {
		logger.info("[worker #" + _worker.process.pid + "] Closed.");
	});
} else {
	global.error_code	= require(rootPath + '/inc/error_codes');
	global.DB			= require(rootPath + '/lib/Database');
	
	var restify			= require('restify');
	var server			= restify.createServer();
	var redis 			= require('redis');
	
	global.redis_pub	= redis.createClient(config.REDIS.PORT, config.REDIS.HOST);
	global.redis_sub	= redis.createClient(config.REDIS.PORT, config.REDIS.HOST);
	global.workerPushData = {};
	
	// 서버에 당도한 것을 환영하오. 낯선 Request여...
	// 모든 API 호출은 이 곳을 지나간다.
	var preFunc	= function (req, res, next) {
		if(req.url != "/driver/update") {
			logger.info("url : " + req.url + ", params : " + req.body);
		}
		
		var oneof = false;

		// Response는 꼭 UTF-8로.
		res.charSet('utf-8');

		if (req.headers.origin) {
			res.header('Access-Control-Allow-Origin', req.headers.origin);
			oneof = true;
		}

		if (req.headers['access-control-request-method']) {
			res.header('Access-Control-Allow-Methods', req.headers['access-control-request-method']);
			oneof = true;
		}

		if (req.headers['access-control-request-headers']) {
			res.header('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
			oneof = true;
		}

		if (oneof) {
			res.header('Access-Control-Max-Age', 60 * 60 * 24 * 365);
		}

		// Token을 Param으로 보내도 Header로 보내주자.
		if (req.params.hasOwnProperty('token')) {
			req.headers['token'] = req.params.token;
		}

		next();
	};

	server.use(restify.fullResponse());
	server.use(restify.bodyParser());
	server.use(restify.queryParser());
	server.use(restify.gzipResponse());
	server.use(preFunc);
	server.on('uncaughtException', function (req, res, route, err) {
		// 에러는 걸러주자.
//	    winston.info(err);
		errFunction(err, req, res);
	});
	
	// 만약 Error가 발생하면 여기로 온다.
	global.errFunction = function (err, req, res) {
	    // 에러는 로깅하고 보내주자.
		try {
			if(err) {
				logger.error("errFunction : [" + req._url.path + "]");
				logger.error(err.stack);
			} else {
				logger.error("errFunction : [" + req._url.path + "]");
			}
			
			res.send({ result: error_code.INTERNAL_ERROR });
		} catch(e) {
			if(e) {
				logger.error("errFunction : [" + req._url.path + "]");
				logger.error(e.stack);
			} else {
				logger.error("errFunction : [" + req._url.path + "]");
			}
		}
	};

	// 어디서든 routes를 통해 모듈로 접근 가능.
	var routes = {
	    call: require(rootPath + '/routes/call.js'),
	    taxi: require(rootPath + '/routes/taxi.js'),
	    bus: require(rootPath + '/routes/bus.js'),
	    busnew: require(rootPath + '/routes/bus_new.js'),
	    driver: require(rootPath + '/routes/driver.js'),
		file: require(rootPath + '/routes/file.js'),
		employee: require(rootPath + '/routes/employee.js'),
		board: require(rootPath + '/routes/board.js'),
		notice: require(rootPath + '/routes/notice.js'),
		question: require(rootPath + '/routes/question.js'),
		crackdown: require(rootPath + '/routes/crackdown.js'),
		user: require(rootPath + '/routes/user.js'),
		chauffeur_company: require(rootPath + '/routes/chauffeur_company.js'),
		virtual_number: require(rootPath + '/routes/virtual_number.js'),
		beacon: require(rootPath + '/routes/beacon.js')
	};

	for (var r in routes) if (routes.hasOwnProperty(r)) {
		routes[r].initialize(server);
	}
	
	server.get(/.*/, restify.serveStatic({
		directory: rootPath + "/public"
	}));
	
	// 포트는 8888번.
	server.listen(8888, function() {
		logger.info(" :: MoonlightDriver 시작합니다. :: ");
	});
	
	process.on("exit", function (err) {
		logger.info("[worker #" + process.pid + "] closed.");
	});
	
	redis_sub.on("message", function (_channel, _recv_data) {
		logger.info("message() recv_msg from tcp channel : " + _channel + ", data : " + _recv_data);
		
		var recv_data = JSON.parse(_recv_data);
		
		switch(_channel) {
			case "virtualNumberRes" :
				if(workerPushData[recv_data.unique_id]) {
					var pushMsg = new PushMessage();
					var message = {
						type: 'register_virtual_number',
						virtual_number: recv_data.virtual_number,
						result : recv_data.result
					};
					
					pushMsg.sendMessage(message, [workerPushData[recv_data.unique_id].gcmId], function(_err) {
						if(_err) {
							logger.error(_err);
							logger.error(_err.stack);
						}
						
						delete(workerPushData[recv_data.unique_id]);
					});
				}
				break;
		}
	});
	
	redis_sub.subscribe("virtualNumberRes");
}
