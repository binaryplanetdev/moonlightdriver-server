global.rootPath	= __dirname;
global.config	= require(rootPath + '/inc/config');
var Logger		= require(rootPath + '/lib/Logger');
var redis 		= require('redis');

global.logger	= new Logger();
global.redis_pub = redis.createClient(config.REDIS.PORT, config.REDIS.HOST);
global.redis_sub = redis.createClient(config.REDIS.PORT, config.REDIS.HOST);

var connManager	= require(rootPath + '/lib/tcp/connection_manager');
var connStream	= require(rootPath + '/lib/tcp/connection_stream');

connManager.connect(config.VIRTUAL_NUMBER.PORT, config.VIRTUAL_NUMBER.HOST, connStream);

redis_sub.on("message", function (_channel, _recv_data) {
	logger.info("message() recv_msg from restify channel : " + _channel + ", data : " + _recv_data);
	
	var recv_data = JSON.parse(_recv_data);
	
	switch(_channel) {
		case "virtualNumberReq" :
			if(recv_data.event_type == "send_register_req") {
				connStream.sendRegisterPacket(recv_data.unique_id, recv_data.org_number);
			} else if(recv_data.event_type == "send_unregister_req") {
				connStream.sendUnregisterPacket(recv_data.virtual_number);
			}
			break;
	}
});

redis_sub.subscribe("virtualNumberReq");

//process.on('message', function(_recv_data) {
////	logger.info("message() recv_msg from master : " + JSON.stringify(_recv_data));
//	
//	switch(_recv_data.event_type) {
//		case "connection" :
//			connManager.connect(_recv_data.port, _recv_data.host, connStream);
//			break;
//		case "send_register_req" :
//			connStream.sendRegisterPacket(_recv_data.unique_id, _recv_data.org_number);
//			break;
//		case "send_unregister_req" :
//			connStream.sendUnregisterPacket(_recv_data.virtual_number);
//			break;
//	}
//});

process.on("uncaughtException", function (err) {
	logger.error("tcp client process uncaughtException: " + err.stack);
});

String.prototype.startsWith = function( str ) {
	return this.substring(0, str.length) === str;
};

String.prototype.endsWith = function( str ) {
	return this.substring( this.length - str.length, this.length ) === str;
};
