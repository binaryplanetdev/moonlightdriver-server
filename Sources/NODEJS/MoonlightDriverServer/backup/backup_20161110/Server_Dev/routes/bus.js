var Async		= require('async');
var Bus			= DB.model('Bus');
var Alba		= DB.model('Alba');
var Nosun       = DB.model('Nosun');
var ObjectId	= DB.Types.ObjectId;

function initialize (server) {
	server.get('/bus', busList);
	server.get('/bus2', busList2);
	server.get('/bus/:busId', busOne);
	server.post('/bus', createBus);
	server.post("/bus/:busId/comment", addComment);
	server.get("/bus/:busId/comment", getComment);
	server.post('/bus/:busId/edit', edit);
	server.post('/bus/:busId/desc', recordDesc);
	server.post('/bus/:busId/phone', recordPhone);
	server.post('/bus/:busId/record', recordBus);
	server.post('/login', loginAlba);
	server.post('/nosun', recordNosun);
	server.get('/nosun/:nosunId/get', checkNosun);
}

/***
 * HTTP GET /bus
 * REQUEST PARAMETERS : len(한번에 가져올 길이), pageNo(페이지 수)
 * RESPONSE PARAMETERS : result(결과값), buses(버스 목록), length(총 버스 수)
 */
function busList (req, res) {
	if (checkParams(['len', 'pageNo'], req.params)) {
		paramFunction(req, res);
        return;
	}

	var len = parseInt(req.params.len);
	var pageNo = parseInt(req.params.pageNo);

	Async.waterfall([
		function (next) {
			Bus.find({}).sort({ _id: -1 }).skip((pageNo - 1) * len).limit(len).exec(next);
		}, function (buses, next) {
			for (var i in buses) if (buses.hasOwnProperty(i)) {
				buses[i] = buses[i].getData();
			}

			Bus.find({}).sort({ _id: -1 }).skip((pageNo - 1) * len).limit(len).exec(function (err, list) {
				next(err, buses, list);
			});
		}, function (buses, list, next) {
			res.send({ result: main.result.SUCCESS, buses: buses, length: list.length });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function busList2(req, res){
	if (checkParams(['lat', 'lng', 'range'], req.params)) {
		paramFunction(req, res);
        return;
	}

	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	var range = parseFloat(req.params.range);

	var lngBase = range / 88800;
	var latBase = range / 1100000;

	var query	= {
		'lists.lat': { $gte: lat - latBase, $lte: lat + latBase },
		'lists.lng': { $gte: lng - lngBase, $lte: lng + lngBase }
	};

	Async.waterfall([
		function (next) {
			Bus.find(query).sort({ _id: -1 }).limit(20).exec(next);
		}, function (buses, next) {
			for (var i in buses) if (buses.hasOwnProperty(i)) {
				buses[i] = buses[i].getData();
			}

			Bus.find(query).sort({ _id: -1 }).limit(20).exec(function (err, list) {
				next(err, buses, list);
			});
		}, function (buses, list, next) {
			res.send({ result: main.result.SUCCESS, buses: buses, length: list.length });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

/***
 * HTTP GET /bus/:busId
 * REQUEST PARAMETERS : busId (bus의 id값)
 * RESPONSE PARAMETERS : result(결과값), bus(버스)
 */
function busOne (req, res) {
	if (checkParams(['busId'], req.params)) {
		paramFunction(req, res);
        return;
	}

	Async.waterfall([
		function (next) {
			Alba.authorize(req.params.token).exec(next);
		}, function (alba, next) {
			if (!alba) {
                res.send({ result: main.result.NOT_AUTHORIZED });
				return;
			}

			Bus.findById(new ObjectId(req.params.busId)).exec(next);
		}, function (bus, next) {
			if (!bus) {
				// TODO 예외처리를 해주자.
				res.send({ result: main.result.NO_SUCH_BUS });
				return;
			}

			res.send({ result: main.result.SUCCESS, bus: bus.getData() });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

/***
 * HTTP POST /bus
 * REQUEST PARAMETERS : name (이름), desc (구간 등 설명)
 * RESPONSE PARAMETERS : result(결과값), bus(버스)
 */
function createBus (req, res) {
	if (checkParams(['name', 'desc'], req.params)) {
		paramFunction(req, res);
        return;
	}

	Async.waterfall([
		function (next) {
			Alba.authorize(req.params.token).exec(next);
		}, function (alba, next) {
			if (!alba) {
                res.send({ result: main.result.NOT_AUTHORIZED });
				return;
			}

			var bus = new Bus({
				name: req.params.name,
				desc: req.params.desc,
				lists: []
			});

			bus.save(function (err) {
				next(err, bus);
			});
		}, function (bus, next) {
			res.send({ result: main.result.SUCCESS, bus: bus.getData() });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}
function getComment (req, res) {
	if (checkParams(['busId','len','pageNo'], req.params)) {
		paramFunction(req, res);
        return;
	}

	Async.waterfall([
		function (next) {
			Bus.findById(new ObjectId(req.params.busId)).exec(next);
		}, function (bus, next) {
			if (!bus) {
				// TODO 예외처리를 해주자.
				res.send({ result: main.result.NO_SUCH_BUS });
				return;
			}
			var length = parseInt(req.params.len);
			var pageNo = parseInt(req.params.pageNo);

			var commentlist = [];

			for(var i=length*(pageNo-1)+1;i<=length*(pageNo-1)+1+(length-1);i++)
				if(bus.comment[i-1])
					commentlist.push(bus.comment[i-1]);

			res.send({ result: main.result.SUCCESS, bus: commentlist });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function addComment (req, res) {
	if(checkParams(['busId', "message", "nickname"],req.params)) {
		paramFunction(req, res);
		return;
	}

	Async.waterfall([
        function (next) {
            Bus.findOne({ _id: new ObjectId(req.params.busId) }).exec(next);
        }, function (bus, next) {
            if (!bus) {
                res.send({ result: main.result.NO_SUCH_BUS });
                return;
            }
            var newObject = {
	            message: req.params.message,
				nickname: req.params.nickname
            };

			bus.comment.push(newObject);
            bus.save(function (err) {
				if (err) next(err);
				else {
					res.send({ result: main.result.SUCCESS, bus: bus.getData() });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

/***
 * HTTP POST /bus/:busId/record
 * REQUEST PARAMETERS : lat, lng (위/경도), name (정류장 이름), busId (버스 ID)
 * RESPONSE PARAMETERS : result(결과값), bus(버스)
 */
function recordBus (req, res) {
	if (checkParams(['lat', 'lng', 'name', 'busId'], req.params)) {
		paramFunction(req, res);
        return;
	}

	Async.waterfall([
		function (next) {
			Alba.authorize(req.params.token).exec(next);
		}, function (alba, next) {
			if (!alba) {
                res.send({ result: main.result.NOT_AUTHORIZED });
				return;
			}

			Bus.findById(new ObjectId(req.params.busId)).exec(next);
		}, function (bus, next) {
			if (!bus) {
				// TODO 예외처리를 해주자.
				res.send({ result: main.result.NO_SUCH_BUS });
				return;
			}

			var newObject = {
				index: 	bus.lists.length + 1,
				text: 	req.params.name,
				lat: 	parseFloat(req.params.lat),
				lng: 	parseFloat(req.params.lng)
			};

			bus.lists.push(newObject);
			bus.save(function (err) {
				if (err) next(err);
				else {
					res.send({ result: main.result.SUCCESS, bus: bus.getData() });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

/***
 * HTTP POST /bus/:busId/edit
 */
function edit(req, res) {
	if (checkParams(['busId'], req.params)) {
		paramFunction(req, res);
        return;
	}

	Async.waterfall([
		function (next) {
			Alba.authorize(req.params.token).exec(next);
		}, function (alba, next) {
			if (!alba) {
                res.send({ result: main.result.NOT_AUTHORIZED });
				return;
			}

			Bus.findById(new ObjectId(req.params.busId)).exec(next);
		}, function (bus, next) {
			if (!bus) {
				// TODO 예외처리를 해주자.
				res.send({ result: main.result.NO_SUCH_BUS });
				return;
			}

			if (req.params.hasOwnProperty("name")) bus.name = req.params.name;
			if (req.params.hasOwnProperty("desc")) bus.desc = req.params.desc;
			if (req.params.hasOwnProperty("phone")) bus.phone = req.params.phone;
			bus.save(function (err) {
				if (err) next(err);
				else {
					res.send({ result: main.result.SUCCESS, bus: bus.getData() });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

/***
 * HTTP POST /bus/:busId/phone
 */
function recordPhone (req, res) {
	if (checkParams(['phone', 'busId'], req.params)) {
		paramFunction(req, res);
        return;
	}

	Async.waterfall([
		function (next) {
			Bus.findById(new ObjectId(req.params.busId)).exec(next);
		}, function (bus, next) {
			if (!bus) {
				// TODO 예외처리를 해주자.
				res.send({ result: main.result.NO_SUCH_BUS });
				return;
			}

			bus.phone = req.params.phone;
			bus.save(function (err) {
				if (err) next(err);
				else {
					res.send({ result: main.result.SUCCESS, bus: bus.getData() });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

/***
 * HTTP POST /bus/:busId/desc
 */
function recordDesc (req, res) {
	if (checkParams(['desc', 'busId'], req.params)) {
		paramFunction(req, res);
        return;
	}

	Async.waterfall([
		function (next) {
			Alba.authorize(req.params.token).exec(next);
		}, function (alba, next) {
			if (!alba) {
                res.send({ result: main.result.NOT_AUTHORIZED });
				return;
			}

			Bus.findById(new ObjectId(req.params.busId)).exec(next);
		}, function (bus, next) {
			if (!bus) {
				// TODO 예외처리를 해주자.
				res.send({ result: main.result.NO_SUCH_BUS });
				return;
			}

			bus.desc = req.params.desc;
			bus.save(function (err) {
				if (err) next(err);
				else {
					res.send({ result: main.result.SUCCESS, bus: bus.getData() });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}


/***
 * HTTP POST /alba/login
 * REQUEST PARAMETERS : id, password
 * RESPONSE PARAMETERS : result, token, alba
 */
function loginAlba (req, res) {

	if (checkParams(['id', 'password'], req.params)) {
		paramFunction(req, res);
		return;
	}

	Async.waterfall([
		function (next) {
			Alba.login(req.params.id, req.params.password).exec(next);
		}, function (alba, next) {
            if (!alba) {
                res.send({ result: main.result.NOT_AUTHORIZED });
                return;
            }

            alba.generateToken();
			alba.save(function (err) {
				next(err, alba);
			});
		}, function (alba, next) {
			res.send({ result: main.result.SUCCESS, alba: alba.getData(), token: alba.token });
            		next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function recordNosun(req, res)
{
	if (checkParams(['nosunId','first', 'last'], req.params)) {
		paramFunction(req, res);
        return;
	}
	Async.waterfall([
        function (next) {
        	var nosun = new Nosun();

        	nosun.nosunId = req.params.nosunId;
            nosun.first = parseInt(req.params.first);
            nosun.last  = parseInt(req.params.last);

            nosun.save(function (err) {
                next(err, nosun);
            });
        }, function (nosun, next) {
            res.send({ result: main.result.SUCCESS, nosun: nosun.getData() });
            next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

function checkNosun(req, res)
{
	if(checkParams(['nosunId'],req.params)) {
		paramFunction(req, res);
		return;
	}
	Async.waterfall([
        function (next) {
            Nosun.findOne({ nosunId: req.params.nosunId}).exec(next);
        }, function (nosun, next) {
            if (!nosun) {
                res.send({ result: main.result.NO_SUCH_NOSUN });
                return;
            }

            nosun = nosun.getData();

            next(null, { result: main.result.SUCCESS, nosun: nosun });
        }, function (data, next) {
            res.send(data);
            next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

exports.initialize = initialize;
exports.busList = busList;
exports.busList2 = busList2;
exports.busOne = busOne;
exports.createBus = createBus;
exports.recordBus = recordBus;
exports.loginAlba = loginAlba;
exports.recordNosun = recordNosun;
exports.checkNosun = checkNosun;
exports.addComment = addComment;
exports.getComment = getComment;