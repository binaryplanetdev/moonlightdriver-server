var Async					= require('async');
var Crackdown				= DB.model('Crackdown');
var _LogCrackdown			= DB.model('_LogCrackdown');
var _LogCrackdownComment	= DB.model('_LogCrackdownComment');
var CrackdownAction			= DB.model('CrackdownAction');
var _LogCrackdownAction		= DB.model('_LogCrackdownAction');
var User      				= DB.model('User');
var ObjectId				= DB.Types.ObjectId;

function initialize (server) {
	server.post('/crackdown/list', getCrackdownList);
	server.post('/crackdown/detail', getCrackdownDetail);
	server.post('/crackdown/register', registerCrackdown);
	server.post('/crackdown/action', updateCrackdownAction);
	server.post('/crackdown/comment/register', registerCrackdownComment);
}

function getCrackdownList(req, res) {
	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	var range = parseFloat(req.params.range);
	
	Async.waterfall([
 		function (next) {
		    Crackdown.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
				}
			], function(err, crackdowns) {
				next(err, crackdowns);
			});
 		},
 		function(crackdowns, next) {
 			var retCrackdown = [];
 			if(crackdowns) {
 				var listLength = crackdowns.length;
 				for(var i = 0; i < listLength; i++) {
 					retCrackdown.push({
 						crackdownId:	crackdowns[i]._id,
 						sigugun:		crackdowns[i].sigugun,
 						sido:			crackdowns[i].sido,
 						category:		crackdowns[i].category,
 						dislikeCount:	crackdowns[i].dislikeCount,
 						likeCount:		crackdowns[i].likeCount,
 						address:		crackdowns[i].address,
 						from:			crackdowns[i].from,
 						locations:		crackdowns[i].locations,
 						createTime:		crackdowns[i].createTime,
 						updatedTime:	crackdowns[i].updatedTime
 					});
 				}
 			}
 			
 			res.send({ result: main.result.SUCCESS, crackdown_list: retCrackdown });
			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function getCrackdownDetail(req, res) {
	if(checkParams(['crackdownId', 'userId'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
 		function (next) {
 			Crackdown.findById(new ObjectId(req.params.crackdownId)).exec(next);
 		},
 		function(crackdown_info, next) {
 			if(!crackdown_info) {
 				res.send({ result: main.result.NO_SUCH_CRACKDOWN });
				return;
 			}
 			
 			CrackdownAction.findOne({crackdownId: req.params.crackdownId, userId: req.params.userId}).exec(function(err, crackdown_action) {
  				if(err) {
  					next(err);
  				} else {
  					var retCrackdownInfo = crackdown_info.getData();
  					retCrackdownInfo.isUserLike = false;
  					retCrackdownInfo.isUserDislike = false;
  					if(crackdown_action) {
  						if(crackdown_action.actionType == "like") {
  							retCrackdownInfo.isUserLike = true;
  						} else if(crackdown_action.actionType == "dislike") {
  							retCrackdownInfo.isUserDislike = true;
  						}
  					}
  		 			
  		 			res.send({ result: main.result.SUCCESS, crackdown_detail: retCrackdownInfo});
  					next();
  				}
  			});
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function registerCrackdown(req, res) {
	if(checkParams(['userId', 'lat', 'lng', 'sido', 'sigugun', 'address', 'registrant', 'reportType'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	
	Async.waterfall([
 		function (next) {
 			User.findById(new ObjectId(req.params.userId)).exec(next);
 		},
 		function(user_info, next) {
 			if(!user_info) {
 				res.send({ result: main.result.NO_SUCH_USER });
				return;
 			}
 			
 			user_info = user_info.getData();
 			var crackdownData = new Crackdown({
 				userId:			req.params.userId,
 				registrant:		req.params.registrant,
 				sigugun:		req.params.sigugun,
 				sido:			req.params.sido,
 				category:		"POLICE",
 				dislikeCount:	0,
 				likeCount:		0,
 				address:		req.params.address,
 				from:			"달빛기사",
 				reportType:		req.params.reportType,
 				locations: {
 					type : "Point",
 					coordinates: [lng, lat]
 				},
 				comment: [],
 				createTime:		new Date().getTime(),
 				updatedTime:	new Date().getTime()
 			});
 			
 			crackdownData.save(function(err) {
				if(err) {
					next(err);
				} else {
					res.send({ result: main.result.SUCCESS });
					next(null, crackdownData);
				}
			});
 		},
 		function(crackdownData, next) {
 			crackdownData = crackdownData.getData();
 			var log_crackdown = new _LogCrackdown({
 				crackdownId:	crackdownData.crackdownId,
 				userId:			crackdownData.userId,
 				registrant:		crackdownData.registrant,
 				sigugun:		crackdownData.sigugun,
 				sido:			crackdownData.sido,
 				category:		crackdownData.category,
 				dislikeCount:	crackdownData.dislikeCount,
 				likeCount:		crackdownData.likeCount,
 				address:		crackdownData.address,
 				from:			crackdownData.from,
 				locations:		crackdownData.locations,
 				reportType:		crackdownData.reportType,
 				createTime:		crackdownData.createTime,
 				updatedTime:	crackdownData.updatedTime,
 				log_type:		"register",
 				logDate: 		new Date()
 			});
 			
 			log_crackdown.save();
 			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function registerCrackdownComment(req, res) {
	if(checkParams(['parentIndex', 'crackdownId', 'depth', 'userId', 'registrant', 'comment'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	var parentIndex = parseInt(req.params.parentIndex);
	var depth = parseInt(req.params.depth);
	
	Async.waterfall([
 		function (next) {
 			Crackdown.findById(new ObjectId(req.params.crackdownId)).exec(next);
 		},
 		function(crackdown_info, next) {
 			if(!crackdown_info) {
 				res.send({ result: main.result.NO_SUCH_CRACKDOWN });
				return;
 			}
 			
 			User.findById(new ObjectId(req.params.userId)).exec(function(err, user_info) {
 				if(err) {
 					next(err);
 				} else {
 					next(null, crackdown_info, user_info);
 				}
 			});
 		},
 		function(crackdown_info, user_info, next) {
 			if(!user_info) {
 				res.send({ result: main.result.NO_SUCH_USER });
				return;
 			}
 			
 			var newCommentData = {
 				userId:			req.params.userId,
 				registrant:		req.params.registrant,
 				contents:		req.params.comment,
 				createTime:		new Date().getTime()
 			};
 			
 			if(depth == 1) {
 				newCommentData.sub_comments = [];
 				crackdown_info.comments.push(newCommentData);
 			} else if(depth == 2) {
 				if(crackdown_info.comments.length > 0 && crackdown_info.comments[parentIndex]) {
					crackdown_info.comments[req.params.parentIndex].sub_comments.push(newCommentData);
 				} else {
 					res.send({ result: main.result.NOT_ENOUGH_PARAMS });
 					return;
 				}
 			} else {
 				res.send({ result: main.result.NOT_ENOUGH_PARAMS });
				return;
 			}
 			
 			crackdown_info.save(function(err) {
 				next(err, crackdown_info, user_info);
 			});
 		},
 		function(crackdown_info, user_info, next) {
 			res.send({ result: main.result.SUCCESS, comment_list: crackdown_info.comments });
 			
 			crackdown_info = crackdown_info.getData();
 			var newCommentData;
 			if(depth == 1) {
 				newCommentData = crackdown_info.comments[crackdown_info.comments.length - 1];
 			} else if(depth == 2) {
 				newCommentData = crackdown_info.comments[parentIndex].sub_comments[crackdown_info.comments[parentIndex].sub_comments.length - 1];
 			}
 			
 			if(newCommentData) {
 				var log_crackdown_comment = new _LogCrackdownComment({
 					crackdownCommentId:	newCommentData._id,
 					crackdownId:	crackdown_info.crackdownId,
 					depth:			depth,
 					userId:			user_info.userId,
 					registrant:		newCommentData.registrant,
 					contents:		newCommentData.contents,
 					createTime:		newCommentData.createTime,
 					logType: 		"register",
 					logDate: 		new Date()
 				});
 				
 				log_crackdown_comment.save();
 			}
 			
 			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function updateCrackdownAction(req, res) {
	if(checkParams(['crackdownId', 'userId', 'actionType'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
	 	function (next) {
	 		Crackdown.findById(new ObjectId(req.params.crackdownId)).exec(next);
	 	},
  		function(crackdown_info, next) {
  			if(!crackdown_info) {
  				res.send({ result: main.result.NO_SUCH_CRACKDOWN });
 				return;
  			}
  			
  			User.findById(new ObjectId(req.params.userId)).exec(function(err, user_info) {
  				if(err) {
  					next(err);
  				} else {
  					next(null, crackdown_info, user_info);
  				}
  			});
  		},
  		function(crackdown_info, user_info, next) {
  			if(!user_info) {
  				res.send({ result: main.result.NO_SUCH_USER });
 				return;
  			}
  			
  			CrackdownAction.findOne({crackdownId: req.params.crackdownId, userId: req.params.userId}).exec(function(err, crackdown_action) {
  				if(err) {
  					next(err);
  				} else {
  					next(null, crackdown_info, user_info, crackdown_action);
  				}
  			});
  		},
  		function(crackdown_info, user_info, crackdown_action, next) {
  			if(crackdown_action) {
  				if(crackdown_action.actionType == "like") {
  					res.send({ result: main.result.DUPLICATE_LIKE });
  				} else if(crackdown_action.actionType == "dislike") {
  					res.send({ result: main.result.DUPLICATE_DISLIKE });
  				} else {
  					res.send({ result: main.result.DUPLICATE_LIKE });
  				}
  				
 				return;
  			}
  			
  			if(req.params.actionType == "like") {
  				crackdown_info.likeCount = parseInt(crackdown_info.likeCount) + 1;
  			} else if(req.params.actionType == "dislike") {
  				crackdown_info.dislikeCount = parseInt(crackdown_info.dislikeCount) + 1;
  			} else if(req.params.actionType == "share") {
  				
  			} else {
  				res.send({ result: main.result.NOT_ENOUGH_PARAMS });
 				return;
  			}
  			
  			crackdown_info.save(function(err) {
  				if(err) {
 					next(err);
 				} else {
 					next(null, crackdown_info, user_info, crackdown_action);
 				}
  			});
  		},
  		function(crackdown_info, user_info, crackdown_action, next) {
  			var retCrackdownInfo = crackdown_info.getData();
  			user_info = user_info.getData();
  			var crackdown_action = new CrackdownAction({
  				crackdownId:	retCrackdownInfo.crackdownId,
  				userId:			user_info.userId,
  				actionType:		req.params.actionType,
  				createTime:		new Date().getTime()
  			});
  			
  			crackdown_action.save(function(err) {
  				if(err) {
 					next(err);
 				} else {
 					if(crackdown_action) {
						if(crackdown_action.actionType == "like") {
							retCrackdownInfo.isUserLike = true;
						} else if(crackdown_action.actionType == "dislike") {
							retCrackdownInfo.isUserDislike = true;
						}
					}
 						
 					res.send({ result: main.result.SUCCESS, crackdown_detail: retCrackdownInfo });
 					next(null, crackdown_action);
 				}
  			});
  		},
  		function(crackdown_action, next) {
  			crackdown_action = crackdown_action.getData();
  			var log_crackdown_action = new _LogCrackdownAction({
  				crackdownActionId:	crackdown_action.crackdownActionId,
  				crackdownId:	crackdown_action.crackdownId,
  				userId:			crackdown_action.userId,
  				actionType:		crackdown_action.actionType,
  				logDate: 		new Date()
  			});
  			
  			log_crackdown_action.save();
  			next();
  		}
  	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

exports.initialize = initialize;
exports.getCrackdownList = getCrackdownList;
exports.getCrackdownDetail = getCrackdownDetail;
exports.registerCrackdown = registerCrackdown;
exports.updateCrackdownAction = updateCrackdownAction;
exports.registerCrackdownComment = registerCrackdownComment;
