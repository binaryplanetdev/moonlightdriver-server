var crypto		= require('crypto');
var mongoose	= require('mongoose');
var Schema		= mongoose.Schema;
var DBConf		= require('./conf/db.json');
var mongoURI	= "mongodb://" + DBConf.id + ":" + DBConf.pw + "@" + DBConf.host + ":" + DBConf.port + "/" + DBConf.database;

// ------------ 주의 -------------- //
// 한번 설정하면 운영 중에 바꿔선 안됩니다. //
var Salt = {
	header:		"ASDF_QWERTY_QAZWSXEDC_M00NL1GHTDR1V3RAUTH0R1Z3SALTK3Y_[",
	footer:		"]_DR1V3RT0US3RT0M0N3YAHSALTYKEYES_QAZWSXEDC_QWERTY_ASDF"
};

/***
 * 데이터를 단방향으로 암호화한다. [비밀번호 등의 데이터]
 *
 * @param	origin	원래의 데이터
 * @return	암호화된 Hash
 */
function encryptData (origin) {
	// 다섯번 정도 Salting을 해준다.
	for (var i = 0; i < 5; i++) {
		origin = crypto.createHash('sha512').update(Salt.header + origin + Salt.footer).digest('hex');
	}
	
	return origin;
}

// 기존 입력받은 값으로 토큰을 만들어낸다.
// NO MORE MD5. LET'S SHA1!
function makeToken (origin) {
	return crypto.createHash('sha1').update(origin).digest('hex');
}

/*
// 기사에 해당되는 DB Schema
var DriverSchema = new Schema({
	id:			String,
	password:	String, // 여기에 직접 접근하지 말자.
	token:		String, // 여기도 직접 접근하지 말자.
	phone:		String,
	name:		String,
	insurance:	String,
	isValid:	{ type: Boolean, default: false }
});
*/
var UserSchema = new Schema({
	gcmId:			String,
	device_uuid:	String,
	email:			String,
	phone:			String,
	name:			String,
	macAddr:		String,
	profileImage:	String,
	locations: {
		type:	String,
		coordinates: [Number, Number]
	},
	createdDate: { type: Date, default: Date.now },
	lastLoginDate: { type: Date, default: Date.now }
});

UserSchema.methods.getData = function () {
	return {
		userId:			this._id,
		device_uuid:	this.device_uuid,
		gcmId:			this.gcmId,
		email:			this.email,
		phone:			this.phone,
		name:			this.name,
		macAddr:		this.macAddr,
		profileImage:	this.profileImage,
		locations:		this.locations,
		lastLoginDate:	this.star,
		starnum:		this.starnum
	};
};

var DriverSchema = new Schema({
	gcmId:			String,
	email:			String,
	phone:			String,
	name:			String,
	version:		String,
	driverLicense:	String,
	insuranceCompanyId:	String,
	insuranceNumber:	String,
	cert: {
		automatic:	Boolean,
		number:		String
	},
	insurance:		String,
	address:		String,
	birth:			String,
	gender:			Boolean, // true면 남자, false면 여자.
	isValid: { type: Boolean, default: false },
	point: { type: Number, default: 0.0 },
	worksAt:		String,
	macAddr:		String,
	image:			String,
	ynLogin:		String,
	star:			Number,
	starnum:		Number,
	createdDate: { type: Date, default: Date.now },
	updatedDate: { type: Date, default: Date.now },
	lastLoginDate: { type: Date, default: Date.now }
});

DriverSchema.methods.getData = function () {
	return {
		driverId:	this._id,
		gcmId:		this.gcmId,
		email:		this.email,
		phone:		this.phone,
		name:		this.name,
		version:	this.version,
		driverLicense:	this.driverLicense,
		insuranceCompanyId:	this.insuranceCompanyId,
		insuranceNumber:	this.insuranceNumber,
		cert:		this.cert,
		insurance: 	this.insurance,
		address: 	this.address,
		birth:		this.birth,
		gender: 	this.gender,
		isValid:	this.isValid,
		pointer:	this.pointer,
		worksAt:	this.worksAt,
		macAddr:	this.macAddr,
		ynLogin:	this.ynLogin,
		image:		this.image,
		star:		this.star,
		starnum:	this.starnum
	};
};

DriverSchema.methods.changePassword = function (password) {
	this.password = encryptData(password);
};

DriverSchema.methods.generateToken = function () {
	this.token = makeToken(new Date().getTime() + this.phone + this.password);
};

DriverSchema.statics.login = function (id, password) {
	return this.findOne({ id: id, password: encryptData(password) });
};

DriverSchema.statics.authorize = function (token) {
	return this.findOne({ token: token });
};

var CompanySchema = new Schema({
	name:			String,
	representative:	String,
	registration:	String,
	phone:			String,
	fax:			String
});

var AllianceSchema = new Schema({
	companyId :		String,
	name:			String,
	phone:			String
});

var AdminSchema = new Schema({
	companyId :		String,
	allianceId:		String,
	name:			String,
	phone:			String,
	level:			Number
});

var AccountSchema = new Schema({
	id:				String,
	pw:				String,
	name:			String,
	phone:			String,
	email:			String,
	level:			Number,
	ynPush:			String,
	createdDate:	Number,
	updatedDate:	Number
});

// 기사 Call에 해당하는 DB Schema
var CallSchema = new Schema({
	callType: { type: String, enum: ["normal", "auction"] },
	status:	{ type: String, enum: ["wait", "catch", "start", "end"] },
	start: {
		text:		String,
		location: {
			type: { type: String },
			coordinates: [ Number, Number ]
		}
	},
	through: {
		text:		String,
		location: {
			type: { type: String },
			coordinates: [ Number, Number ]
		}
	},
	end: {
		text:		String,
		location: {
			type: { type: String },
			coordinates: [ Number, Number ]
		}
	},
	money:			Number,
	byCard:			Boolean,
	user: {
		realPhone:	String,
		phone:		String
	},
	driver: {
		driverId:	String,
		phone:		String
	},
	etc:			String,
	createdAt: { type: Date, default: Date.now }
}, {
	// 이걸 true로 둘 경우엔 DB Documents에 _v라는 버전 key 항목이 생성됨.
	versionKey:		false
});

CallSchema.methods.getData = function () {
	return {
		callId:		this._id,
		callType:	this.callType,
		status:		this.status,
		start:		this.start,
		through:	this.through,
		end:		this.end,
		money:		this.money,
		byCard:		this.byCard,
		user:		this.user,
		driver:		this.driver,
		etc:		this.etc,
		createdAt: 	this.createdAt
	};
};

/***
 * 위도, 경도, 반경을 입력하면 알아서 목록을 출력하는 쿼리를 만들어준다.
 *
 * @param	type	콜 타입(normal, auction, both)
 * @param	lat		Latitude
 * @param	lng		Longitude
 * @param	range	범위(m 단위)
 * @return	쿼리문과 정렬까지 되어있다. exec로 실행시켜준다.
 * ex) Call.searchList("both", lat, lng, range).exec( function (err, lists) { ... } );
 */
CallSchema.statics.searchList = function (type, lat, lng, range) {
	// 내가 쿼리문을 짠다. 홍홍홍.
	// 위도 1도는 111km이고, 경도 1도  88.8km이다.
	var lngBase = range / 88800;
	var latBase = range / 1100000;
	var query	= { 
		isValid: true,
		isDone: false,
		'start.lat': { 
			$gte: lat - latBase, 
			$lte: lat + latBase
		},
		'start.lng': {
			$gte: lng - lngBase,
			$lte: lng + lngBase
		}
	};

	// BOTH 일 때는 둘 다 검색되도록 하고, 그게 아니라면 TYPE을 지정해주자.
	if (type != "both"){
		query.callType = type;
	}
	
	return this.find(query).sort({ _id: -1 });
};

var TaxiCarpoolSchema = new Schema({
	driverId:		String,
	registrant:		String,
	isValid:		Boolean,
	isPush:			Boolean,
	isCatch:		Boolean,
	start: {
		text:		String,
		location: {
			type : { type: String },
	        coordinates : [ Number, Number ]
		}
	},
	end: {
		text:		String,
		location: {
			type : { type: String },
	        coordinates : [ Number, Number ]
		}
	},
	money:			Number,
	host:			String,
	drivers: {
		max: 		Number,
		now:		Number,
		list:	[{
			phone:	String,
			isAccept: Boolean,
			status: String
		}]
	},
	time: 			Number,
	divideN: { type: Boolean, default: false }
}, {
	versionKey:		false
});

TaxiCarpoolSchema.methods.getData = function () {
	var retVal = {
		taxiId:		this._id,
		driverId:	this.driverId,
		registrant:	this.registrant,
		isValid:	this.isValid,
		isPush:		this.isPush,
		isCatch:	this.isCatch,
		start:		this.start,
		end:		this.end,
		money:		this.money,
		host:		this.host,
		drivers:	this.drivers,
		time:		this.time,
		divideN: 	this.divideN
	};
	
	for (var i in retVal.drivers.list) {
		if (retVal.drivers.list.hasOwnProperty(i)) {
			retVal.drivers.list[i]._id = undefined;
		}
	}
	
	return retVal;
};

var TaxiFindSchema = new Schema({
	driverId:		String,
	phone:			String,
	gcmId:			String,
	ynLogin:		String,
	date:			{ type: Number },
	location: {
		type: { type: String },
		coordinates: [ Number, Number ]
	},
	isEnable: { type: Boolean, default: true }
});

TaxiFindSchema.methods.getData = function () {
	return {
		driverId:	this.driverId,
		phone:		this.phone,
		gcmId:		this.gcmId,
		ynLogin:	this.ynLogin,
		date:		this.date,
		location: {
			type: 			this.location.type,
			coordinates: 	this.location.coordinates
		}
	};
};

TaxiFindSchema.methods.getListData = function () {
	return {
		driverId:	this.driverId,
		phone:		this.phone,
		location: {
			type:			this.location.type,
			coordinates:	this.location.coordinates
		}
	};
};

var LocationSchema = new Schema({
	callId:     String,
	userType:   Boolean,    // True면 기사고, False면 이용자다.
	typeId:     String,     // 기사 혹은 이용자의 ObjectId
	date:       Number,     // 시간 Timestamp다. DB에서 시간 순으로 Sort할 땐 Date보단 Number지.
	lat:        Number,
	lng:        Number
}, {
	versionKey: false
});

LocationSchema.methods.getData = function () {
	return {
		lat:	this.lat,
		lng:	this.lng
	};
};

var BusSchema = new Schema({
	name: 		String,
	desc: 		String,
	phone:      String,
	category:   Number,
	lists: 		[{
		index: 	Number,
		text: 	String,
		lat: 	Number,
		lng: 	Number,
		createdAt: { type: Date, default: Date.now }
	}],
	comment: [{
		createdAt: { type: Date, default: Date.now },
		message: String,
		nickname: String
	}]
});

BusSchema.methods.getData = function () {
	var retVal = {
		busId:		this._id,
		name:		this.name,
		desc:		this.desc,
		lists:		this.lists,
		comment:	this.comment,
		category:	this.category
	};

	for (var i in retVal.lists) {
		if (retVal.lists.hasOwnProperty(i)) {
			retVal.lists[i]._id = undefined;
		}
	}	

	return retVal;
};

BusSchema.statics.searchList = function (lat, lng, range) {
	// 내가 쿼리문을 짠다. 홍홍홍.
	// 위도 1도는 111km이고, 경도 1도  88.8km이다.
	var lngBase = range / 88800;
	var latBase = range / 1100000;
	var query	= {
		'start.lat': { 
			$gte: lat - latBase, 
			$lte: lat + latBase 
		},
		'start.lng': { 
			$gte: lng - lngBase, 
			$lte: lng + lngBase 
		}
	};
	
	return this.find(query).sort({ _id: -1 });
};

var BusNewSchema = new Schema({
	name: 		String,
	desc: 		String,
	phone:      String,
	category:   Number,
	lists: 		[{
		index: 	Number,
		text: 	String,
		lat: 	Number,
		lng: 	Number,
		createdAt: { type: Date, default: Date.now }
	}],
	comment: [{
		createdAt: { type: Date, default: Date.now },
		message: String,
		nickname: String
	}]
});

BusNewSchema.methods.getData = function () {
	var retVal = {
		busId:		this._id,
		name:		this.name,
		desc:		this.desc,
		lists:		this.lists,
		comment:	this.comment,
		category:	this.category
	};

	for (var i in retVal.lists) {
		if (retVal.lists.hasOwnProperty(i)) {
			retVal.lists[i]._id = undefined;
		}
	} 

	return retVal;
};

var PushSchema = new Schema({
	isDriver:	Boolean,      // 기사 여부 확인
	phone:		String,          // 푸쉬를 보낼 전화번호
	token: {
		pushType:	Boolean,  // TRUE : GCM / FALSE : APNS
		tokenData:	String   // TOKEN USER STRING
	}
});

var AlbaSchema = new Schema({
	id: 		String,
	password: 	String,
	name: 		String,
	phone: 		String,
	token: {
		data:	String,
		expire:	Number
	}
});

AlbaSchema.methods.changePassword = function (password) {
	this.password = encryptData(password);
};

AlbaSchema.methods.generateToken = function () {
	this.token = {
		data: makeToken(new Date().getTime() + this.phone + this.password),
		expire: new Date().getTime() + (1000 * 60 * 60 * 24) // 만료는 하루로 설정.
	};
};

AlbaSchema.methods.getData = function () {
	return {
		albaId: this._id,
		id: 	this.id,
		name: 	this.name,
		phone: 	this.phone
	};
};

AlbaSchema.statics.login = function (id, password) {
	return this.findOne({ id: id, password: encryptData(password) });
};

AlbaSchema.statics.authorize = function (token) {
	// TODO: 절대 수정하지 마시오. "token.expire is GREATER THAN current time"이 맞음.
	return this.findOne({ 'token.data': token, 'token.expire': { $gte: new Date().getTime() } });
};

var NosunSchema = new Schema({
	id:			String,
	nosunId:	String,
	first:		Number,
	last:		Number
});

NosunSchema.methods.getData = function () {
	return {
		id:			this.id,
		nosunId:	this.nosunId,
		first:		this.first,
		last:		this.last
	};
};

var ChatSchema = new Schema({
	busId:			String,
	drivers: [{
		driverId:	String,
		gcmId:		String,
		nickName:	String,
		joinDate: { type: Date, default: Date.now }
	}],
	createDate: { type: Date, default: Date.now }
});

ChatSchema.methods.getData = function () {
	var retVal = {
		chatId:		this._id,
		busId:		this.busId,
		drivers:	this.drivers,
		createDate:	this.createDate
	};
	
	for (var i in retVal.drivers) {
		if (retVal.drivers.hasOwnProperty(i)) {
			retVal.drivers[i]._id = undefined;
		}
	}
	
	return retVal;
};

var VersionSchema = new Schema({
	version:	String,
	isValid:	Boolean
});

VersionSchema.methods.getData = function () {
	return {
		version:	this.version,
		isValid:	this.isValid
	};
};

var UserVersionSchema = new Schema({
	version:	String,
	isValid:	Boolean
});

UserVersionSchema.methods.getData = function () {
	return {
		version:	this.version,
		isValid:	this.isValid
	};
};

var ShuttlePointSchema = new Schema({
	busId:		String,
	index:		Number,
	text:		String,
	arsId:		String,
	firstTime:	String,
	lastTime:	String,
	saturdayFirstTime:	String,
	saturdayLastTime:	String,
	sundayFirstTime:	String,
	sundayLastTime:		String,
	exceptOutback:	Number,
	locations: {
		type: { type: String },
		coordinates: [ Number, Number ]
	},
	createdAt: { type: Date, default: Date.now }
});

ShuttlePointSchema.methods.getData = function () {
	var retVal = {
		shuttlePointsId:	this._id,
		busId:				this.busId,
		index:				this.index,
		text:				this.text,
		arsId:				this.arsId,
		firstTime:			this.firstTime,
		lastTime:			this.lastTime,
		saturdayFirstTime:	this.saturdayFirstTime,
		saturdayLastTime:	this.saturdayLastTime,
		sundayFirstTime:	this.sundayFirstTime,
		sundayLastTime:		this.sundayLastTime,
		exceptOutback:		this.exceptOutback,
		locations:			this.locations,
		createAt:			this.createAt
	};
	
	return retVal;
};

var ShuttleInfoSchema = new Schema({
	name:       String,
	desc:       String,
	phone:      String,
	category:   Number,
	realTimeCategoryName: String,
	lineColor:	String,
	comment: [{
		createdAt: Number,
		messageType: String,	// normal, fuw, fuw_location
		message: String,
		driverId: String,
		nickName: String,
		lat: Number,
		lng: Number
	}],
	wokitoki: [{
		date: String,
		messageSrc: String,
		driverId: String,
		nickName: String,
		phone: String,
		lat: Number,
		lng: Number
	}],
	createdAt: { type: Date, default: Date.now },
	firstTime:	String,
	lastTime:	String,
	saturdayFirstTime:		String,
	saturdayLastTime:		String,
	sundayFirstTime:		String,
	sundayLastTime:			String,
	runFirstTime:			String,
	runLastTime:			String,
	saturdayRunFirstTime:	String,
	saturdayRunLastTime:	String,
	sundayRunFirstTime:		String,
	sundayRunLastTime:		String,
	holidayDate:			String,
	term:		Number,
	totalTime:	Number
});

ShuttleInfoSchema.methods.getData = function () {
	var retVal = {
		busId:					this._id,
		name:					this.name,
		desc:					this.desc,
		comment:				this.comment,
		wokitoki:				this.wokitoki,
		category:				this.category,
		realTimeCategoryName:	this.realTimeCategoryName,
		lineColor:				this.lineColor,
		firstTime:				this.firstTime,
		lastTime:				this.lastTime,
		saturdayFirstTime:		this.saturdayFirstTime,
		saturdayLastTime:		this.saturdayLastTime,
		sundayFirstTime:		this.sundayFirstTime,
		sundayLastTime:			this.sundayLastTime,
		runFirstTime:			this.runFirstTime,
		runLastTime:			this.runLastTime,
		saturdayRunFirstTime:	this.saturdayRunFirstTime,
		saturdayRunLastTime:	this.saturdayRunLastTime,
		sundayRunFirstTime:		this.sundayRunFirstTime,
		sundayRunLastTime:		this.sundayRunLastTime,
		holidayDate:			this.holidayDate,
		term:					this.term,
		totalTime:				this.totalTime
	};
	
	return retVal;
};

var ShuttleCategorySchema = new Schema({
	categoryIndex:	Number,
	categoryName:	String
});

ShuttleCategorySchema.methods.getData = function () {
	var retVal = {
		categoryId:		this._id,
		categoryIndex:	this.categoryIndex,
		categoryName:	this.categoryName
	};
	
	return retVal;
};

var ShuttleRealTimePositionSchema = new Schema({
	categoryName:	String,
	locations: {
		type: { type: String },
		coordinates: [ Number, Number ]
	},
	updateDate: Number
});

ShuttleRealTimePositionSchema.method.getData = function() {
	var retVal = {
		categoryName:	this.categoryName,
		locations:		this.locations
	};
	
	return retVal;
};

var CallHistorySchema = new Schema({
	date:       String,
	si_name:    String,
	gu_name:    String,
	dong_name:	String,
	call_count: Number
});

CallHistorySchema.methods.getData = function () {
	var retVal = {
		callHistoryId:  this._id,
		si_name:   this.si_name,
		gu_name:   this.gu_name,
		dong_name: this.dong_name,
		call_count: this.call_count
	};
	
	return retVal;
};

var FrequentlyUsedWordSchema = new Schema({
	word:		String,
	usedCount:	Number,
	priority:	Number,
	createdDate: { type: Date, default: Date.now }
});

FrequentlyUsedWordSchema.methods.getData = function () {
	var retVal = {
		wordId:		this._id,
		word:		this.word,
		usedCount:	this.usedCount,
		priority:	this.priority
	};
	
	return retVal;
};

var BoardSchema = new Schema({
	driverId:		String,
	nickName:		String,
	title:			String,
	category:		String,
	sex:			String,
	age:			String,
	career:			String,
	divide:			String,
	range:			String,
	start:			String,
	time:			String,
	target:			String,
	car:			String,
	contact:		String,
	etc:			String,
	comment: [{
		nickName:	String,
		driverId:	String,
		comment:	String,
		createdAt:	Number
	}],
	createdDate:	Number
});

BoardSchema.methods.getData = function () {
	var retVal = {
		boardId:	this._id,
		driverId:	this.driverId,
		nickName:	this.nickName,
		title:		this.title,
		category:	this.category,
		sex:		this.sex,
		age:		this.age,
		career:		this.career,
		divide:		this.divide,
		range:		this.range,
		start:		this.start,
		time:		this.time,
		target:		this.target,
		car:		this.car,
		contact:	this.contact,
		etc:		this.etc,
		comment:	this.comment,
		createdDate:	this.createdDate
	};
	
	for (var i in retVal.comment) {
		if (retVal.comment.hasOwnProperty(i)) {
			retVal.comment[i]._id = undefined;
		}
	}
	
	return retVal;
};

BoardSchema.methods.getListData = function () {
	var retVal = {
		boardId:	this._id,
		nickName:	this.nickName,
		title:		this.title,
		category:	this.category,
		commentCount:	this.comment.length,
		createdDate:	this.createdDate
	};
	
	return retVal;
};

var NoticeSchema = new Schema({
	seq:		Number,
	title:		String,
	contents:	String,
	image:		String,
	link:		String,
	startDate:	Number,
	endDate:	Number,
	createdDate: Number,
	updatedDate: Number
});

NoticeSchema.methods.getData = function () {
	return {
		noticeId:		this._id,
		seq:			this.seq,
		title:			this.title,
		contents:		this.contents,
		image:			this.image,
		link:			this.link,
		startDate:		this.startDate,
		endDate:		this.endDate,
		createdDate:	this.createdDate,
		updatedDate:	this.updatedDate
	};
};

var InsuranceCompanySchema = new Schema({
	companyName:	String,
	numberFormat:	String,
	regExp:			String,
	createdDate: { type: Date, default: Date.now }
});

InsuranceCompanySchema.methods.getData = function () {
	return {
		insuranceCompanyId:	this._id,
		companyName:		this.companyName
	};
};

var QuestionSchema = new Schema({
	seq:		Number,
    driverId:   String,
    phone:      String,
    title:      String,
    contents:   String,
    answerCount:     Number,
    locations:   {
        type:   { type: String },
        coordinates: [Number, Number]
    },
    createdDate: Number,
    updatedDate: Number
});

QuestionSchema.methods.getData = function () {
    return {
        questionId : this._id,
		seq: this.seq,
        driverId: this.driverId,
        title: this.title,
        contents: this.contents,
        phone: this.phone,
        answerCount: this.answerCount,
        locations: this.locations,
        createdDate: this.createdDate,
        updatedDate: this.updatedDate
    };
};

QuestionSchema.methods.getListData = function () {
    return {
        questionId : this._id,
        driverId:	this.driverId,
        phone: this.phone,
        answerCount: this.answerCount,
        createdDate: this.createdDate
    };
};

var QuestionAnswerSchema = new Schema({
	questionId:	String,
	answer:	String,
	createdDate: Number
});

QuestionAnswerSchema.methods.getData = function() {
	return {
		answerId:		this._id,
		questionId:		this.questionId,
		answer:			this.answer,
		createdDate:	this.createdDate
	};
};

var BusStationSchema = new Schema({
	stationIndex:		Number,
	arsId:				String,
	stationName:		String,
	gpsX:				Number,
	gpsY:				Number,
	localStationId:		String,
	stationId:			Number,
	location:   {
        type:   { type: String },
        coordinates: [Number, Number]
    }
});

BusStationSchema.methods.getData = function() {
	return {
		arsId:				this.arsId,
		stationName:		this.stationName,
		gpsX:				this.gpsX,
		gpsY:				this.gpsY,
		localStationId:		this.localStationId,
		stationId:			this.stationId,
		location:			this.location
	};
};

var BusRouteSchema = new Schema({
	busRouteId:			String,
	localBusRouteId:	String,
	busRouteName:		String,
	busRouteType:		Number,
	busRouteTypeName:	String,
	busRouteArea:		String,
	startStationName:	String,
	endStationName:		String,
	firstTime:			String,
	lastTime:			String,
	term:				String
});

BusRouteSchema.methods.getData = function() {
	return {
		busRouteId:			this.busRouteId,
		localBusRouteId:	this.localBusRouteId,
		busRouteName:		this.busRouteName,
		busRouteType:		this.busRouteType,
		busRouteTypeName:	this.busRouteTypeName,
		busRouteArea:		this.busRouteArea,
		startStationName:	this.startStationName,
		endStationName:		this.endStationName,
		firstTime:			this.firstTime,
		lastTime:			this.lastTime,
		term:				this.term
	};
};

var BusRouteStationSchema = new Schema({
	busRouteStationId:	Number,
	busRouteId:			String,
	stationSequence:	Number,
	startTime:			String,
	endTime:			String,
	direction:			String,
	stationId:			Number
});

BusRouteStationSchema.methods.getData = function() {
	return {
		busRouteStationId:	this.busRouteStationId,
		busRouteId:			this.busRouteId,
		stationSequence:	this.stationSequence,
		startTime:			this.startTime,
		endTime:			this.endTime,
		direction:			this.direction,
		stationId:			this.stationId
	};
};

var SubwayStationSchema = new Schema({
	stationIndex:		Number,
	stationCode:		String,
	stationName:		String,
	stationCodeDaum:	String,
	lineNumber:			String,
	gpsX:				String,
	gpsY:				Number,
	location:   {
        type:   { type: String },
        coordinates: [Number, Number]
    }
});

SubwayStationSchema.methods.getData = function() {
	return {
		stationIndex:		this.stationIndex,
		stationCode:		this.stationCode,
		stationName:		this.stationName,
		stationCodeDaum:	this.stationCodeDaum,
		lineNumber:			this.lineNumber,
		gpsX:				this.gpsX,
		gpsY:				this.gpsY,
		location:			this.location
	};
};

var SubwayTimeSchema = new Schema({
	stationTimeIndex:		Number,
	stationCode:			String,
	inOutTag:				String,
	weekTag:				String,
	firstTime:				String,
	firstOrgStationCode:	String,
	firstOrgStationName:	String,
	firstDestStationCode:	String,
    firstDestStationName:	String,
    lastTime:				String,
    lastOrgStationCode:		String,
    lastOrgStationName:		String,
    lastDestStationCode:	String,
    lastDestStationName:	String
});

SubwayTimeSchema.methods.getData = function() {
	return {
		stationTimeIndex:		this.stationTimeIndex,
		stationCode:			this.stationCode,
		inOutTag:				this.inOutTag,
		weekTag:				this.weekTag,
		firstTime:				this.firstTime,
		firstOrgStationCode:	this.firstOrgStationCode,
		firstOrgStationName:	this.firstOrgStationName,
		firstDestStationCode:	this.firstDestStationCode,
		firstDestStationName:	this.firstDestStationName,
		lastTime:				this.lastTime,
		lastOrgStationCode:		this.lastOrgStationCode,
		lastOrgStationName:		this.lastOrgStationName,
		lastDestStationCode:	this.lastDestStationCode,
		lastDestStationName:	this.lastDestStationName
	};
};

var CrackdownSchema = new Schema({
	userId:			String,
	registrant:		String,
	sigugun:		String,
	sido:			String,
	category:		String,
	dislikeCount:	Number,
	likeCount:		Number,
	address:		String,
	from:			String,
	reportType:		String,
	picture:		String,
	locations: {
		type : { type: String },
		coordinates: [Number, Number]
	},
	comments: [{
		userId:			String,
		registrant:		String,
		contents:		String,
		sub_comments: [{
			userId:			String,
			registrant:		String,
			contents:		String,
			createTime:		Number
		}],
		createTime:		Number
	}],
	createTime:		Number,
	updatedTime:	Number
});

CrackdownSchema.methods.getData = function() {
	return {
		crackdownId:	this._id,
		userId:			this.userId,
		registrant:		this.registrant,
		picture:		this.picture,
		sigugun:		this.sigugun,
		sido:			this.sido,
		category:		this.category,
		dislikeCount:	this.dislikeCount,
		likeCount:		this.likeCount,
		address:		this.address,
		from:			this.from,
		reportType:		this.reportType,
		locations:		this.locations,
		comments: 		this.comments,
		createTime:		this.createTime,
		updatedTime:	this.updatedTime
	};
};

var CrackdownActionSchema = new Schema({
	crackdownId:	String,
	userId:			String,
	actionType:		{ type: String, enum: ["like", "dislike", "share"] },
	createTime:		Number
});

CrackdownActionSchema.methods.getData = function() {
	return {
		crackdownActionId:	this._id,
		crackdownId:		this.crackdownId,
		userId:				this.userId,
		actionType:			this.actionType,
		createTime:			this.createTime
	};
};

var ChauffeurCompanySchema = new Schema({
	companyName:		String,
	description:		String,
	useCount:			Number,
	region:				String,
	telephoneOperator:	String,
	stopByFee:			String,
	standByFee:			String,
	createTime:			Number
});

ChauffeurCompanySchema.methods.getData = function() {
	return {
		chauffeurCompanyId:	this._id,
		companyName:		this.companyName,
		description:		this.description,
		useCount:			this.useCount,
		region:				this.region,
		telephoneOperator:	this.telephoneOperator,
		stopByFee:			this.stopByFee,
		standByFee:			this.standByFee
	};
};

var BookmarkChauffeurCompanySchema = new Schema({
	chauffeurCompanyId:	String,
	userId:				String,
	createTime:			Number
});

BookmarkChauffeurCompanySchema.methods.getData = function() {
	return {
		bookmarkChauffeurCompanyId: this._id,
		chauffeurCompanyId:			this.chauffeurCompanyId,
		userId:						this.userId
	};
};

var CounterSchema = new Schema({
	_id: 	String,
	seq:	Number
});

CounterSchema.statics.getNextSequence = function (_name, _callback) {
    return this.findOneAndUpdate(
        { _id: _name },
        { $inc: { seq: 1} },
        { new: true },
		_callback
    );
};

var _LogDriverSchema = new Schema({
	driverId:		String,
	gcmId:			String,
	email:			String,
	phone:			String,
	name:			String,
	version:		String,
	driverLicense:	String,
	insuranceCompanyId:	String,
	insuranceNumber:	String,
	cert: {
		automatic:	Boolean,
		number:		String
	},
	insurance:		String,
	address:		String,
	birth:			String,
	gender:			Boolean, // true면 남자, false면 여자.
	isValid: { type: Boolean, default: false },
	point: { type: Number, default: 0.0 },
	worksAt:		String,
	macAddr:		String,
	image:			String,
	star:			Number,
	starnum:		Number,
	logType: { type: String, enum: ["reg_auto", "reg_full", "edit", "update_confirm", "login", "log_out"] },	// reg_auto(자동가입), reg_full(회원가입), edit(정보수정), update_confirm(기사인증정보업데이트)
	logDate: { type: Date, default: Date.now }
});

var _LogTaxiSchema = new Schema({
	driverId:		String,
	taxiId:			String,
	isValid:		Boolean,
	isPush:			Boolean,
	isCatch:		Boolean,
	start: {
		text:		String,
		location: {
			type : { type: String },
	        coordinates : [ Number, Number ]
		}
	},
	end: {
		text:		String,
		location: {
			type : { type: String },
	        coordinates : [ Number, Number ]
		}
	},
	money:			Number,
	host:			String,
	drivers: {
		max: 		Number,
		list:	[{
			phone:	String,
			isAccept: Boolean
		}]
	},
	divideN: { type: Boolean, default: false },
	message:		String,
	req_driverId:	String,	// 동승 신청 driverId
	logType: { type: String, enum: ["create_share", "edit_share", "req_share", "accept_share", "decline_share"] },	// create_share(카풀 생성), req_share(동승신청), accept_share(동승 수락), decline_share(동승 거부)
	logDate: { type: Date, default: Date.now }
}, {
	versionKey:		false
});

var _LogTaxiFindSchema = new Schema({
	driverId:	String,
	phone:		String,
	gcmId:		String,
	ynLogin:	String,
	date:		{ type: Number },
	location: {
		type: { type: String },
		coordinates: [ Number, Number ]
	},
	isEnable: { type: Boolean, default: true },
	logDate: { type: Date, default: Date.now }
});

var _LogChatSchema = new Schema({
	busId:			String,
	drivers: [{
		driverId:	String,
		gcmId:		String,
		nickName:	String,
		joinDate: { type: Date, default: Date.now }
	}],
	sendDriverId:	String,
	recvDriverId:	String,
	message:		String,
	lat:			Number,
	lng:			Number,
	messageType: { type: String, enum: ["msg", "notice", "req_location", "res_location"] }, // msg(일반메시지), notice(입장/퇴장), req_location(위치 확인 요청), res_location(위치 확인 응답)
	logDate: { type: Date, default: Date.now }
});

var _LogCallSchema = new Schema({
	callId:			String,
	callType: { type: String, enum: ["normal", "auction"] },
	status:	{ type: String, enum: ["wait", "catch", "start", "end"] },
	start: {
		text:		String,
		location: {
			type: { type: String },
			coordinates: [ Number, Number ]
		}
	},
	through: {
		text:		String,
		location: {
			type: { type: String },
			coordinates: [ Number, Number ]
		}
	},
	end: {
		text:		String,
		location: {
			type: { type: String },
			coordinates: [ Number, Number ]
		}
	},
	money:			Number,
	byCard:			Boolean,
	user: {
		realPhone:	String,
		phone:		String
	},
	driver: {
		driverId:	String,
		phone:		String
	},
	etc:			String,
	createdAt: { type: Date, default: Date.now },
	logType: { type: String, enum: ["create", "catch", "start", "stop", "cancel"] },
	logDate: { type: Date, default: Date.now } 
});

var _LogBoardSchema = new Schema({
	driverId:	String,
	nickName:	String,
	title:		String,
	category:	String,
	sex:		String,
	age:		String,
	career:		String,
	divide:		String,
	range:		String,
	start:		String,
	time:		String,
	target:		String,
	car:		String,
	contact:	String,
	etc:		String,
	logStatus:	String,	// regist, edit, delete
	logDate: { type: Date, default: Date.now }
});

var _LogBoardCommentSchema = new Schema({
	boardId:	String,
	driverId:	String,
	nickName:	String,
	comment:	String,
	logStatus:	String,	// regist
	logDate: { type: Date, default: Date.now }
});

var _LogNoticeSchema = new Schema({
	seq:		Number,
	title:		String,
	contents:	String,
	image:		String,
	link:		String,
	startDate:	Number,
	endDate:	Number,
	createdDate: Number,
	updatedDate: Number,
	logStatus: String,
	logDate: { type: Date, default: Date.now }
});

var _LogInsuranceCompanySchema = new Schema({
	companyName:	String,
	numberFormat:	String,
	regExp:			String,
	createdDate: { type: Date, default: Date.now },
	logType: { type: String, enum: ["regist", "edit"] },
	logDate: { type: Date, default: Date.now }
});

var _LogQuestionSchema = new Schema({
	seq:		Number,
	driverId: 	String,
	phone:		String,
	title:		String,
	contents:	String,
	locations: {
		type:	String,
		coordinates: [Number, Number]
	},
	createdDate: 	Number,
	updatedDate: 	Number,
	logType: 		{ type: String, enum: ["regist", "edit", "answer"] },
	logDate: 		{ type: Date, default: Date.now }
});

var _LogQuestionAnswerSchema = new Schema({
	answerId:		String,
    questionId: 	String,
    answer:   		String,
    createdDate: 	Number,
	logDate: 		{ type: Date, default: Date.now }
});

var _LogUserSchema = new Schema({
	gcmId:			String,
	email:			String,
	device_uuid:	String,
	phone:			String,
	name:			String,
	macAddr:		String,
	profileImage:	String,
	locations: {
		type:	String,
		coordinates: [Number, Number]
	},
	logType: 		{ type: String, enum: ["reg_auto", "login"] },
	logDate: 		{ type: Date, default: Date.now }
});

var _LogCrackdownSchema = new Schema({
	crackdownId:	String,
	userId:			String,
	registrant:		String,
	sigugun:		String,
	sido:			String,
	category:		String,
	dislikeCount:	Number,
	likeCount:		Number,
	address:		String,
	from:			String,
	picture:		String,
	reportType:		String,
	locations: {
		type : { type: String },
		coordinates: [Number, Number]
	},
	createTime:		Number,
	updatedTime:	Number,
	logType: 		{ type: String, enum: ["register"] },
	logDate: 		{ type: Date, default: Date.now }
});

var _LogCrackdownCommentSchema = new Schema({
	crackdownCommentId:	String,
	crackdownId:		String,
	depth:				Number,
	userId:				String,
	registrant:			String,
	contents:			String,
	createTime:			Number,
	logType: 			{ type: String, enum: ["register"] },
	logDate: 			{ type: Date, default: Date.now }
});

var _LogCrackdownActionSchema = new Schema({
	crackdownActionId:	String,
	crackdownId:		String,
	userId:				String,
	actionType:			{ type: String, enum: ["like", "dislike", "share"] },
	logDate: 			{ type: Date, default: Date.now }
});

var _LogBookmarkChauffeurCompanySchema = new Schema({
	chauffeurCompanyId:	String,
	userId:				String,
	logType: 		{ type: String, enum: ["register", "unregister"] },
	logDate: 		{ type: Date, default: Date.now }
});

mongoose.model('Driver', DriverSchema);
mongoose.model('Company', CompanySchema);
mongoose.model('Alliance', AllianceSchema);
mongoose.model('Admin', AdminSchema);
mongoose.model('Call', CallSchema);
mongoose.model('TaxiCarpool', TaxiCarpoolSchema);
mongoose.model('TaxiFind', TaxiFindSchema);
mongoose.model('Location', LocationSchema);
mongoose.model('Bus', BusSchema);
mongoose.model('BusNew', BusNewSchema);
mongoose.model('Push', PushSchema);
mongoose.model('Alba', AlbaSchema);
mongoose.model('Nosun', NosunSchema);
mongoose.model('Chat', ChatSchema);
mongoose.model('ShuttlePoint', ShuttlePointSchema);
mongoose.model('ShuttleInfo', ShuttleInfoSchema);
mongoose.model('CallHistory', CallHistorySchema);
mongoose.model('FrequentlyUsedWord', FrequentlyUsedWordSchema);
mongoose.model('Board', BoardSchema);
mongoose.model('_LogDriver', _LogDriverSchema);
mongoose.model('_LogTaxi', _LogTaxiSchema);
mongoose.model('_LogTaxiFind', _LogTaxiFindSchema);
mongoose.model('_LogChat', _LogChatSchema);
mongoose.model('_LogCall', _LogCallSchema);
mongoose.model('_LogBoard', _LogBoardSchema);
mongoose.model('_LogBoardComment', _LogBoardCommentSchema);
mongoose.model('Version', VersionSchema);
mongoose.model('Notice', NoticeSchema);
mongoose.model('_LogNotice', _LogNoticeSchema);
mongoose.model('InsuranceCompany', InsuranceCompanySchema);
mongoose.model('_LogInsuranceCompany', _LogInsuranceCompanySchema);
mongoose.model('Question', QuestionSchema);
mongoose.model('_LogQuestion', _LogQuestionSchema);
mongoose.model('Counter', CounterSchema);
mongoose.model('QuestionAnswer', QuestionAnswerSchema);
mongoose.model('_LogQuestionAnswer', _LogQuestionAnswerSchema);
mongoose.model('BusStation', BusStationSchema);
mongoose.model('BusRoute', BusRouteSchema);
mongoose.model('BusRouteStation', BusRouteStationSchema);
mongoose.model('SubwayStation', SubwayStationSchema);
mongoose.model('SubwayTime', SubwayTimeSchema);
mongoose.model('Account', AccountSchema);
mongoose.model('Crackdown', CrackdownSchema);
mongoose.model('_LogCrackdown', _LogCrackdownSchema);
mongoose.model('_LogCrackdownComment', _LogCrackdownCommentSchema);
mongoose.model('User', UserSchema);
mongoose.model('UserVersion', UserVersionSchema);
mongoose.model('_LogUser', _LogUserSchema);
mongoose.model('CrackdownAction', CrackdownActionSchema);
mongoose.model('_LogCrackdownAction', _LogCrackdownActionSchema);
mongoose.model('ChauffeurCompany', ChauffeurCompanySchema);
mongoose.model('BookmarkChauffeurCompany', BookmarkChauffeurCompanySchema);
mongoose.model('_LogBookmarkChauffeurCompany', _LogBookmarkChauffeurCompanySchema);
mongoose.model('ShuttleCategory', ShuttleCategorySchema);
mongoose.model('ShuttleRealTimePosition', ShuttleRealTimePositionSchema);

module.exports = mongoose.connect(mongoURI);
