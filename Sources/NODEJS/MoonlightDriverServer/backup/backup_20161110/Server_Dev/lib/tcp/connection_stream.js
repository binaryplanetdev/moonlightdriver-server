var SVRUtils	= require(rootPath + '/lib/SVRUtils');
var svr_utils	= new SVRUtils();

function ConnStream() {
	logger.info('===== connection_stream ===== constructor!');
	
	this.socket = null;
	this.checkConnIntervalId = null;
	this.clearGarbageIntervalId = null;
	this.virtualNumbers = [];
	this.virtualNumbersInUse = {};
	
	this.buffer = new Buffer(102400);
	this.buffer_copy = null;
	this.buffer_p = 0;
	this.fmt_slice = [];	
 	this.fmt_column = [];
	
	this.init();
};

ConnStream.prototype.init = function() {
	logger.info('===== connection_stream ===== init()!');
	
	this.fmt_slice = [1, 4, 3, 3, 20, 2, 1, 15, 15, 15, 15, 15, 15, 1, 1];
   	this.fmt_column =['start', 'packet_id', 'company_id', 'system_id', 'sequence', 'result', 'method', 'number_1', 'phone_number_1', 'phone_number_2', 'number_2', 'phone_number_3', 'phone_number_4', 
   	                  'use_flag', 'finish'];
   	
	for(var i = 0; i < 10000; i++) {
		var virtual_number = new Array(5).join("0");
		virtual_number = virtual_number.substr(0, virtual_number.length - (i + "").length) + i;
		
		this.virtualNumbers.push(config.VIRTUAL_NUMBER.NUM_PREFIX + virtual_number);
	}
};

ConnStream.prototype.add_buff = function(_buff) {
	logger.info('===== connection_stream ===== add_buff()! - ' + _buff);
	
	_buff.copy(this.buffer, this.buffer_p);
	this.buffer_p += _buff.length;
};

ConnStream.prototype.parse_buffer = function(_buff) {
	logger.info('===== connection_stream ===== parse_buffer()! - ' + _buff);
	
	var ret = [];
	var offset = 0;
	var count = this.fmt_slice.length;
	
	for (var i = 0; i < count; i++) {
		var str = _buff.toString('utf8', offset, offset + this.fmt_slice[i]);

		ret.push(str);

		offset += this.fmt_slice[i];
	}

	return ret;
};

ConnStream.prototype.parse = function() {
	logger.info('===== connection_stream ===== parse()!');
	
	var token_buff;
	var p = 0;

	for (var i = 0, i_l = this.buffer_p; i < i_l; i++) {
		if (this.buffer[i] == '0x24') {
			token_buff = this.buffer.slice(p, i + 1);
			var pdata = this.parse_buffer(token_buff);
			var recv_packet = {};
			
			for(var column_index = 0; column_index < this.fmt_column.length; column_index++) {
				recv_packet[this.fmt_column[column_index]] = pdata[column_index].replace(/\s/g, '');
			}
			
			logger.info("recv packet : " + JSON.stringify(recv_packet));
			this.resposePacket(recv_packet);
			
			i = i + 1;
			p = i;	
		}
	}

	if (p > 0) {
		if (this.buffer_p > p) {
			this.buffer_copy = this.buffer.slice(p, this.buffer_p);
			this.buffer_copy.copy(this.buffer);

			this.buffer_p = this.buffer_p-p;

			if (this.buffer_p <= 0) {
				this.buffer_p = 0;
			}
		} else {
			this.buffer_p = 0;
		}
	}
};

ConnStream.prototype.createPacket = function(_send_msg) {
	logger.info('===== connection_stream ===== createPacket()! - ' + JSON.stringify(_send_msg));
	
	if(!_send_msg.start) _send_msg.start = "#";
	if(!_send_msg.finish) _send_msg.finish = "$";
	if(!_send_msg.use_flag) _send_msg.use_flag = "1";
	if(!_send_msg.company_id) _send_msg.company_id = config.VIRTUAL_NUMBER.COMPANY_ID;
	if(!_send_msg.system_id) _send_msg.system_id = config.VIRTUAL_NUMBER.SYSTEM_ID;
	
	logger.info("send packet : " + JSON.stringify(_send_msg));
	var send_packet = "";
	
	for(var column_index = 0; column_index < this.fmt_column.length; column_index++) {
		var packet_data = new Array(this.fmt_slice[column_index] + 1).join(" ");
		
		if(_send_msg.hasOwnProperty(this.fmt_column[column_index])) {
			packet_data = packet_data.substr(0, 0) + _send_msg[this.fmt_column[column_index]] + packet_data.substr(_send_msg[this.fmt_column[column_index]].length);
		}
		
		send_packet += packet_data;
	}
	
	return send_packet;
}

ConnStream.prototype.onData = function(_dataBuff) {
	logger.info('===== connection_stream ===== onData()! - ' + _dataBuff);
	
	this.add_buff(_dataBuff);
	this.parse();
};

ConnStream.prototype.onConnect = function(_socket) {
	logger.info('===== connection_stream ===== onConnect()!');
	
	if(_socket) {
		logger.info('===== connection_stream ===== onConnect()! - Connected to stream server');
		
		this.socket = _socket;
		
		// 상태 체크 요구 패킷
		this.checkConnection();
		
		// 안쓰고 있는 번호 사용 등록 해지 요청
		this.clearGarbage();
	}
};

ConnStream.prototype.onReset = function() {
	logger.info('===== connection_stream ===== onReset()!');
};

ConnStream.prototype.onReconnect = function() {
	logger.info('===== connection_stream ===== onReconnect()!');
	this.onConnect();
};

ConnStream.prototype.onError = function(_err) {
	logger.info('===== connection_stream ===== onError()!' + _err);
};

ConnStream.prototype.write = function(_sendBuff) {
	logger.info('===== connection_stream ===== write()!' + _sendBuff);
	this.socket.write(_sendBuff);
};

ConnStream.prototype.checkConnection = function() {
	logger.info('===== connection_stream ===== checkConnection()!');
	var that = this;
	
	that.sendConnectionStatus();
	
	if(that.checkConnIntervalId != null) {
		clearInterval(that.checkConnIntervalId);
	}
	
	that.checkConnIntervalId = setInterval(function() {
		that.sendConnectionStatus();
	}, config.VIRTUAL_NUMBER.RETRY_CHECK_TIME);
};

ConnStream.prototype.clearGarbage = function() {
	logger.info('===== connection_stream ===== clearGarbage()!');
	
	var that = this;
		
	if(that.clearGarbageIntervalId != null) {
		clearInterval(that.clearGarbageIntervalId);
	}
	
	that.clearGarbageIntervalId = setInterval(function() {
		that.sendGarbageNumberUnregisterPacket();
	}, config.VIRTUAL_NUMBER.GARBAGE_CLEAR_TIME);
};

// 안쓰고 있는 번호 사용 해지 요구 패킷
ConnStream.prototype.sendGarbageNumberUnregisterPacket = function() {
	logger.info('===== connection_stream ===== sendGarbageNumberUnregisterPacket()!');
	var that = this;
	var nowDate = new Date().getTime();
	
	Object.keys(that.virtualNumbersInUse).forEach(function(key) {
		var ret_data = that.virtualNumbersInUse[key];
		var diff_time = config.VIRTUAL_NUMBER.GARBAGE_CLEAR_TIME / 2;
		
		if((nowDate - ret_data.reg_date) >= diff_time) {	
			var send_packet = that.createPacket({
				packet_id: "2502",
				method: "1",
				number_1 : ret_data.virtual_number,
				phone_number_1: ret_data.phone_number
			});
			
			that.write(send_packet);
		}
	});
};

ConnStream.prototype.closeConnection = function() {
	logger.info('===== connection_stream ===== closeConnection()!');
	
	this.socket.destroy();
	
	if(this.checkConnIntervalId) {
		clearInterval(this.checkConnIntervalId);
	}
	
	if(this.clearGarbageIntervalId) {
		clearInterval(this.clearGarbageIntervalId);
	}
};

// 상태 체크 요구 패킷 전송
ConnStream.prototype.sendConnectionStatus = function() {
	logger.info('===== connection_stream ===== sendConnectionStatus()!');
	
	var send_packet = this.createPacket({
		packet_id: "2600"
	});
	
	this.write(send_packet);
};

//상태 체크 응답 패킷 전송
ConnStream.prototype.sendConnectionStatusRes = function() {
	logger.info('===== connection_stream ===== sendConnectionStatusRes()!');
	
	var send_packet = this.createPacket({
		packet_id: "3600",
		result: "00"
	});
	
	this.write(send_packet);
};

// 번호 사용 등록 요구 패킷
ConnStream.prototype.sendRegisterPacket = function(_unique_id, _org_number) {
	logger.info('===== connection_stream ===== sendRegisterPacket()! - ' + _unique_id + ',' + _org_number);
	
	var ret_data = this.getVirtualNumber(_unique_id, _org_number);
	
	var send_packet = this.createPacket({
		packet_id: "2501",
		method: "1",
		number_1 : ret_data.virtual_number,
		phone_number_1: ret_data.phone_number
	});
	
	this.write(send_packet);
};

// 번호 사용 해지 요구 패킷
ConnStream.prototype.sendUnregisterPacket = function(_virtual_number) {
	logger.info('===== connection_stream ===== sendUnregisterPacket()! - ' + _virtual_number);
	
	var ret_data = this.virtualNumbersInUse[_virtual_number];
	
	var send_packet = this.createPacket({
		packet_id: "2502",
		method: "1",
		number_1 : ret_data.virtual_number,
		phone_number_1: ret_data.phone_number
	});
	
	this.write(send_packet);
};

// 응답 패킷 처리
ConnStream.prototype.resposePacket = function(_res_data) {
	logger.info('===== connection_stream ===== resposePacket()! - ' + JSON.stringify(_res_data));
	
	switch(_res_data.packet_id) {
		case "2600" :	// 상태 체크 요구 패킷
			this.sendConnectionStatusRes();
			break;
		case "3600" :	// 상태 체크 요구 응답 패킷
			if(_res_data.result != "00") {
				this.closeConnection();
			}
			break;
		case "3501" :	// 번호 사용 등록 요구 응답패킷
			var message = this.virtualNumbersInUse[_res_data.number_1];
			message.result = _res_data.result;
			
			redis_pub.publish("virtualNumberRes", JSON.stringify(message));
//			process.send(message);
			
//			logger.info(JSON.stringify(this.virtualNumbersInUse));
			break;
		case "3502" :	// 번호 사용 해지 요구 응답패킷
			this.updateVirtualNumberInUse(_res_data);
			break;
	}
};

// 사용 가능한 가상 번호 조회
ConnStream.prototype.getVirtualNumber = function(_unique_id, _org_number) {
	logger.info('===== connection_stream ===== getVirtualNumber()! - ' + _unique_id + ',' + _org_number);
	
	var converted_number = svr_utils.convertToLocalPhoneNumber(_org_number);
	var virtual_number = this.virtualNumbers.shift();
	
	if(!this.virtualNumbersInUse[virtual_number]) {
		this.virtualNumbersInUse[virtual_number] = {
			phone_number: converted_number,
			org_number : _org_number,
			unique_id: _unique_id,
			virtual_number : virtual_number,
			reg_date : new Date().getTime()
		};
	}
	
	return this.virtualNumbersInUse[virtual_number];
};

// 사용중인 번호 갱신
ConnStream.prototype.updateVirtualNumberInUse = function(_res_data) {
	logger.info('===== connection_stream ===== getVirtualNumber()! - ' + JSON.stringify(_res_data));
	
	if(_res_data.packet_id == "3502") {
		if(this.virtualNumbersInUse[_res_data.number_1]) {
			delete(this.virtualNumbersInUse[_res_data.number_1]);
			
			if(this.virtualNumbers.indexOf(_res_data.number_1) == -1) {
				this.virtualNumbers.push(_res_data.number_1);
			}
		}
	}
};

module.exports = new ConnStream();

process.on("uncaughtException", function (err) {
	logger.error("===== connection_stream ===== uncaughtException: " + err.stack);
});
