/***
 * MoonlightDriver Server
 * 달빛기사 앱을 위한 node.js 서버입니다.
 */
var pkg     = require('./package.json');
var restify = require('restify');
var winston = require('winston');
var server  = restify.createServer();

// 서버에 당도한 것을 환영하오. 낯선 Request여...
// 모든 API 호출은 이 곳을 지나간다.
var preFunc	= function (req, res, next) {
//	logger.info("url : " + req.url + ", params : " + req.body);
	var oneof = false;

	// Response는 꼭 UTF-8로.
	res.charSet('utf-8');

	if (req.headers.origin) {
		res.header('Access-Control-Allow-Origin', req.headers.origin);
		oneof = true;
	}

	if (req.headers['access-control-request-method']) {
		res.header('Access-Control-Allow-Methods', req.headers['access-control-request-method']);
		oneof = true;
	}

	if (req.headers['access-control-request-headers']) {
		res.header('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
		oneof = true;
	}

	if (oneof) res.header('Access-Control-Max-Age', 60 * 60 * 24 * 365);

	// Token을 Param으로 보내도 Header로 보내주자.
	if (req.params.hasOwnProperty('token')) req.headers['token'] = req.params.token;

	next();
};

// 포트는 8888번.
server.listen(8888, function() {
	logger.info(" :: MoonlightDriver " + pkg.version + " | 시작합니다. :: ");
});
server.use(restify.fullResponse());
server.use(restify.bodyParser());
server.use(restify.queryParser());
server.use(restify.gzipResponse());
server.use(preFunc);
server.on('uncaughtException', function (req, res, route, err) {
	// 에러는 걸러주자.
//    winston.info(err);
	errFunction(err, req, res);
});

// Database도 Global Scope에 둬버리고 어디서든 호출 가능하게 하자.
global.DB	= require('./database.js');

// Enum들이나 이런 것들은 여기에 지정해두고 가져온다.
global.main = require('./conf/main.json');

// 만약 Error가 발생하면 여기로 온다.
global.errFunction = function (err, req, res) {
    // 에러는 로깅하고 보내주자.
	try {
		if(err) {
			logger.error("errFunction : [" + req._url.path + "]");
			logger.error(err);
			logger.error(err.stack);
		} else {
			logger.error("errFunction : [" + req._url.path + "]");
		}
		
		res.send({ result: main.result.INTERNAL_ERROR });
	} catch(e) {
		
	}
};

// 꼭 필요한 것들이 있는지 체크하자.
global.checkParams = function (origin, params) {
    var retData = false;
    for (var o in origin)
        if (!params.hasOwnProperty(origin[o])) retData = true;

    return retData;
};

global.paramFunction = function (req, res) {
	res.send({ result: main.result.NOT_ENOUGH_PARAMS });
};

// Logger 설정. 우리는 console.log를 멀리하고 winston의 로깅을 가까이 하는 것이 옳습니다.
global.logger = new (winston.Logger) ({
	transports: [
		new (winston.transports.Console)({
			level : "debug",
			colorize : true,
			timestamp : true
		}),
		new (winston.transports.File)({
			filename: './logs/moonlightdriver.log',
			level : "error",
			colorize : true,
			timestamp : true
		})
//        new (winston.transports.Console)	({ colorize: true }),
//        new (winston.transports.File)		({ filename: './logs/moonlightdriver.log' })
    ]
});

logger.setLevels(winston.config.syslog.levels);

// 위치와 관련된 Function들은 미리 정의해두면 펀하다.
global.getDistance = function (lat1, lng1, lat2, lng2) {
    var delta_lon = degree2Radius(lng2) - degree2Radius(lng1);
    var distance = Math.acos(Math.sin(degree2Radius(lat1)) * Math.sin(degree2Radius(lat2))
        + Math.cos(degree2Radius(lat1)) * Math.cos(degree2Radius(lat2)) * Math.cos(delta_lon)) * 3963.189;

    return parseInt(distance * 1609.344);
};

global.degree2Radius = function (val) {
    return ((eval(val))*(Math.PI/180));
};

process.on("uncaughtException", function (err) {
	logger.error("uncaughtException: " + err.stack);
});

String.prototype.startsWith = function( str ) {
	return this.substring( 0, str.length ) === str;
};

String.prototype.endsWith = function( str ) {
	return this.substring( this.length - str.length, this.length ) === str;
};


// 어디서든 routes를 통해 모듈로 접근 가능.
var routes = {
    call: require('./routes/call.js'),
    taxi: require('./routes/taxi.js'),
    bus: require('./routes/bus.js'),
    busnew: require('./routes/bus_new.js'),
    driver: require('./routes/driver.js'),
	file: require('./routes/file.js'),
	employee: require('./routes/employee.js'),
	board: require('./routes/board.js'),
	notice: require('./routes/notice.js'),
	question: require('./routes/question.js'),
	crackdown: require('./routes/crackdown.js'),
	user: require('./routes/user.js'),
	chauffeur_company: require('./routes/chauffeur_company.js')
};

for (var r in routes) if (routes.hasOwnProperty(r))
	routes[r].initialize(server);

//logger.info(" :: MoonlightDriver " + pkg.version + " | 시작합니다. :: ");
module.exports = global;
