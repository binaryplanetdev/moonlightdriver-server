var winston = require('winston');

Date.prototype.toLogDateFormat = function() {
	var nowDate = new Date();
	var year = nowDate.getFullYear();
	var months = nowDate.getMonth() + 1;
	if(months < 10) months = '0' + months; 
	var day = nowDate.getDate();
	if(day < 10) day = '0' + day;
	var hours = nowDate.getHours();
	if(hours < 10) hours = '0' + hours;
	var minutes = nowDate.getMinutes();
	if(minutes < 10) minutes = '0' + minutes;
	var seconds = nowDate.getSeconds();
	if(seconds < 10) seconds = '0' + seconds;
	var miliSec = nowDate.getMilliseconds();
	
	return year + "-"+ months + "-" + day + " " + hours + ":" + minutes + ":" + seconds + "." + miliSec; 
}

module.exports = function(_info_log_file, _error_log_fie) {
	var logger = new winston.Logger({
		transports: [
			new winston.transports.Console({
				name: 'consoleLog',
				level: 'debug',
				colorize: false,
				timestamp: function(){ return new Date().toLogDateFormat() },
				json: false
			}),
			new winston.transports.File({
				name: 'debugLog',
				level: 'debug',
				filename: rootPath + config.LOG.LOG_DIR + _info_log_file,
				maxsize: config.LOG.MAX_FILE_SIZE,
				maxFiles: config.LOG.MAX_FILES,
				timestamp: function(){ return new Date().toLogDateFormat() },
				json: false
		    }),
		    new winston.transports.File({
				name: 'errorLog',
				level: 'error',
				filename: rootPath + config.LOG.LOG_DIR + _error_log_fie,
				maxsize: config.LOG.MAX_FILE_SIZE,
				maxFiles: config.LOG.MAX_FILES,
				timestamp: function(){ return new Date().toLogDateFormat() },
				json: false
		    })
		]
	});
	
	logger.setLevels(winston.config.syslog.levels);
	logger.exitOnError = false;
	
	return logger;
};
