var shortid = require("shortid");

var SVRUtils = function() {
	
};

SVRUtils.prototype.getUniqueId = function() {
	return shortid.generate();
};

SVRUtils.prototype.degree2Radius = function (val) {
    return ((eval(val)) * (Math.PI / 180));
};

SVRUtils.prototype.getDistance = function(lat1, lng1, lat2, lng2) {
	var delta_lon = this.degree2Radius(lng2) - this.degree2Radius(lng1);
    var distance = Math.acos(Math.sin(this.degree2Radius(lat1)) * Math.sin(this.degree2Radius(lat2)) + Math.cos(this.degree2Radius(lat1)) * Math.cos(this.degree2Radius(lat2)) * Math.cos(delta_lon)) * 3963.189;

    return parseInt(distance * 1609.344);
};

SVRUtils.prototype.checkParams = function (origin, params) {
    var retData = false;
    for (var o in origin) {
    	if (!params.hasOwnProperty(origin[o])) {
    		retData = true;
    		break;
    	}
    }

    return retData;
};

// 전화번호 포맷 변경(+821012345678 -> 01012345678)
SVRUtils.prototype.convertToLocalPhoneNumber = function (_phone_number) {
	if(_phone_number.startsWith("+82")) {
		_phone_number = "0" + _phone_number.substring(3);
	}
	
	_phone_number = _phone_number.replace(/-/g, "");

    return _phone_number;
};

//전화번호 포맷 변경(01012345678 -> +821012345678)
SVRUtils.prototype.convertToGlobalPhoneNumber = function (_phone_number) {
	if(_phone_number.startsWith("010")) {
		_phone_number = "+82" + _phone_number.substring(1);
	}

    return _phone_number;
};

SVRUtils.prototype.getRandPhoneNumber = function() {
	var retNumber = "";
	
	for(var i = 0; i < 8; i++) {
		if(i == 1 || i == 2) {
			retNumber += "X";
		} else {
			retNumber += this.getRandomInt(0, 9);
		}
		
		if(i == 3) {
			retNumber += "-";
		}
	}
	
	return retNumber;
};

SVRUtils.prototype.getRandomInt = function(_low, _high) {
	return Math.floor(Math.random() * (_high - _low + 1) + _low);
};




module.exports = SVRUtils;
