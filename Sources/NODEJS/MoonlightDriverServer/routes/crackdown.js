var Async					= require('async');
var Crackdown				= DB.model('Crackdown');
var _LogCrackdown			= DB.model('_LogCrackdown');
var _LogCrackdownComment	= DB.model('_LogCrackdownComment');
var CrackdownAction			= DB.model('CrackdownAction');
var _LogCrackdownAction		= DB.model('_LogCrackdownAction');
var CrackdownPushNotification = DB.model('CrackdownPushNotification');
var User      				= DB.model('User');
var UserAdditional			= DB.model('UserAdditional');
var CrackdownRankWeekly		= DB.model('CrackdownRankWeekly');
var CrackdownRankMonthly	= DB.model('CrackdownRankMonthly');
var CrackdownBlackList		= DB.model('CrackdownBlackList');
var ObjectId				= DB.Types.ObjectId;
var fs 						= require('fs');
var mv 						= require('mv');
var PushMessage		= require(rootPath + '/lib/PushMessage');

function initialize (server) {
	server.post('/crackdown/list', getCrackdownList);
	server.post('/crackdown/detail', getCrackdownDetail);
	server.post('/crackdown/register', registerCrackdown);
	server.post('/crackdown/action', updateCrackdownAction);
	server.post('/crackdown/delete', deleteCrackdown);
	server.post('/crackdown/comment/register', registerCrackdownComment);
	server.post('/crackdown/upload/file', uploadCrackdownFile);
	server.post('/crackdown/share/file', uploadShareFile);

	server.get('/crackdown/list/rank', getCrackdownRankList);
}

function getCrackdownList(req, res) {
	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	var range = parseFloat(req.params.range);
	
	Async.waterfall([
 		function (next) {
		    Crackdown.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
				}
			], function(err, crackdowns) {
				next(err, crackdowns);
			});
 		},
 		function(crackdowns, next) {
 			var retCrackdown = [];
 			if(crackdowns) {
 				var listLength = crackdowns.length;
 				for(var i = 0; i < listLength; i++) {
 					retCrackdown.push({
 						crackdownId:	crackdowns[i]._id,
 						sigugun:		crackdowns[i].sigugun,
 						sido:			crackdowns[i].sido,
 						category:		crackdowns[i].category,
 						dislikeCount:	crackdowns[i].dislikeCount,
 						likeCount:		crackdowns[i].likeCount,
 						address:		crackdowns[i].address,
 						from:			crackdowns[i].from,
 						reportType:		crackdowns[i].reportType ? crackdowns[i].reportType : "normal",
 						picture:		crackdowns[i].picture,
 						locations:		crackdowns[i].locations,
 						commentCount:	crackdowns[i].comments ? crackdowns[i].comments.length : 0,
 						createTime:		crackdowns[i].createTime,
 						updatedTime:	crackdowns[i].updatedTime
 					});
 				}
 			}
 			
 			res.send({ result: error_code.SUCCESS, crackdown_list: retCrackdown });
			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function getCrackdownDetail(req, res) {
	if(svr_utils.checkParams(['crackdownId', 'userId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
 		function (next) {
 			Crackdown.findById(new ObjectId(req.params.crackdownId)).exec(next);
 		},
 		function(crackdown_info, next) {
 			if(!crackdown_info) {
 				res.send({ result: error_code.NO_SUCH_CRACKDOWN });
				return;
 			}
 			
 			CrackdownAction.findOne({crackdownId: req.params.crackdownId, userId: req.params.userId}).exec(function(err, crackdown_action) {
  				if(err) {
  					next(err);
  				} else {
  					var retCrackdownInfo = crackdown_info.getData();
  					retCrackdownInfo.isUserLike = false;
  					retCrackdownInfo.isUserDislike = false;
  					if(crackdown_action) {
  						if(crackdown_action.actionType == "like") {
  							retCrackdownInfo.isUserLike = true;
  						} else if(crackdown_action.actionType == "dislike") {
  							retCrackdownInfo.isUserDislike = true;
  						}
  					}
  		 			
  		 			res.send({ result: error_code.SUCCESS, crackdown_detail: retCrackdownInfo});
  					next();
  				}
  			});
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function getCrackdownRankList(req, res) {
	var now = new Date();

	Async.waterfall([
 		function (next) {
 			CrackdownRankWeekly.find({weekOfYear : now.getWeekOfYear()}).sort({point : -1}).limit(5).exec(next);
 		},
 		function(weeklyRankList, next) {
 			var search = now.getFullYear() + "-" + (now.getMonth() < 10 ? '0' + now.getMonth() : now.getMonth());
 			console.log("search : " + search);
 			CrackdownRankMonthly.find({month : search}).sort({point : -1}).limit(5).exec(function(err, monthlyRankList) {
 				next(err, weeklyRankList, monthlyRankList);
 			});
 		},
 		function(weeklyRankList, monthlyRankList, next) {
 			var crackdownWeeklyRankList = [];
 			var crackdownMonthlyRankList = [];
 			
 			if(weeklyRankList) {
 				var weeklyRankListSize = weeklyRankList.length;
 				for(var i = 0; i < weeklyRankListSize; i++) {
 					crackdownWeeklyRankList.push({
 						rank : i + 1,
 						registrant : weeklyRankList[i].registrant,
 		 				reportCount : weeklyRankList[i].totalCount,
 		 				reportPoint : weeklyRankList[i].point
 					});
 				}
 			}
 			
 			if(monthlyRankList) {
 				var monthlyRankListSize = monthlyRankList.length;
 				for(var i = 0; i < monthlyRankListSize; i++) {
 					crackdownMonthlyRankList.push({
 						rank : i + 1,
 						registrant : monthlyRankList[i].registrant,
 		 				reportCount : monthlyRankList[i].totalCount,
 		 				reportPoint : monthlyRankList[i].point
 					});
 				}
 			}
 			
 			res.send({ result: error_code.SUCCESS, crackdownWeeklyRankList: crackdownWeeklyRankList, crackdownMonthlyRankList: crackdownMonthlyRankList});
			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function registerCrackdown(req, res) {
	if(svr_utils.checkParams(['userId', 'lat', 'lng', 'sido', 'sigugun', 'address', 'registrant', 'reportType'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var nowTime = new Date();
	if(nowTime.getHours() > 5 && nowTime.getHours() < 19) {
		res.send({ result: error_code.NOT_AVAILABLE_REPORT_TIME });
		return;
	}
	
	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	
	Async.waterfall([
 		function (next) {
 			User.findById(new ObjectId(req.params.userId)).exec(next);
 		},
 		function (user_info, next) {
 			if(!user_info) {
 				res.send({ result: error_code.NO_SUCH_USER });
				return;
 			}
 			
 			user_info = user_info.getData();
 			
 			CrackdownBlackList.findOne({'userId' : req.params.userId}).exec(function(_err, blacklistData) {
 				if(_err) {
 					next(_err);
 				} else {
 					if(blacklistData && blacklistData.blockState == "block") {
 						res.send({ result: error_code.NOT_ALLOW_REPORT });
 						return;
 					}
 					
 					next(null, user_info);
 				}
 			});
 		},
 		function(user_info, next) {
 			var crackdownData = new Crackdown({
 				userId:			req.params.userId,
 				registrant:		req.params.registrant,
				phone:			user_info.phone,
 				sigugun:		req.params.sigugun,
 				sido:			req.params.sido,
 				picture:		req.params.picture,
 				dislikeCount:	0,
 				likeCount:		0,
 				commentCount:	0,
 				address:		req.params.address,
 				from:			"카대리",
 				reportType:		req.params.reportType,
 				locations: {
 					type : "Point",
 					coordinates: [lng, lat]
 				},
 				comments: [],
 				createTime:		new Date().getTime(),
 				updatedTime:	new Date().getTime()
 			});
 			
 			crackdownData.save(function(err) {
				if(err) {
					next(err);
				} else {
					next(null, crackdownData);
				}
			});
 		},
 		function(crackdownData, next) {
 			crackdownData = crackdownData.getData();
 			var log_crackdown = new _LogCrackdown({
 				crackdownId:	crackdownData.crackdownId,
 				userId:			crackdownData.userId,
 				registrant:		crackdownData.registrant,
 				phone:			crackdownData.phone,
 				sigugun:		crackdownData.sigugun,
 				sido:			crackdownData.sido,
 				category:		crackdownData.category,
 				dislikeCount:	crackdownData.dislikeCount,
 				likeCount:		crackdownData.likeCount,
 				address:		crackdownData.address,
 				from:			crackdownData.from,
 				picture:		crackdownData.picture,
 				locations:		crackdownData.locations,
 				reportType:		crackdownData.reportType,
 				createTime:		crackdownData.createTime,
 				updatedTime:	crackdownData.updatedTime,
 				log_type:		"register",
 				logDate: 		new Date()
 			});
 			
 			log_crackdown.save();
 			next(null, crackdownData);
 		},
 		function(crackdownData, next) {
 			User.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"query" : { "_id" : { "$ne" : new ObjectId(req.params.userId) } },
						"distanceField": "distance",
						"maxDistance": 1000,
						"spherical": true,
						"limit": 100000
					}
				}, {
					"$project" : { "_id" : 1, "gcmId" : 1, "distance": 1 }
				}, {
					"$sort" : {
						"distance" : 1
					}
				}
			], function(err, users) {
				next(err, users, crackdownData);
			});
 		},
 		function(users, crackdownData, next) {
 			if(users && users.length > 0) {
 				sendCrackdownPush(users, crackdownData.crackdownId);
 			}
 			
 			res.send({ result: error_code.SUCCESS });
 			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function sendCrackdownPush(_users, _crackdownID) {
	Async.each(_users, function(_user_info, _callback) {
		UserAdditional.findOne({"userId" : _user_info._id}).exec(function(_err, _userAdditional) {
    		if(_err) {
    			logger.error(_err);
				logger.error(_err.stack);
    		} else {
    			if(!_userAdditional) {
    				_userAdditional = new UserAdditional({
						userId : _user_info._id,
						pushYN : "Y",
						createdDate : new Date(),
						lastUpdateDate : new Date()
					});
    				
    				_userAdditional.save();
    	    	}
    			
    			var userAdditional = _userAdditional.getData();
    			
    			if(userAdditional && userAdditional.pushYN == "Y") {
    				var pushMsg = new PushMessage();
     				var message = {
    					type: 'crackdown_warning',
    					crackdownCount : 1
    				};
     				
    				var crackdownMessage = "현재 ";
    				var distanceType = 0;
    				if(_user_info.distance >= 0 && _user_info.distance < 300) {
    					distanceType = 300;
    					crackdownMessage += distanceType + "m";
    				} else if(_user_info.distance >= 300 && _user_info.distance < 500) {
    					distanceType = 500;
    					crackdownMessage += distanceType + "m";
    				} else if(_user_info.distance >= 500 && _user_info.distance < 1000) {
    					distanceType = 1000;
    					crackdownMessage += "1km";
    				}
    				
    				message.distance = distanceType;
    				message.message = crackdownMessage + " 이내에 1곳에서 음주단속 중입니다";
    				
    				var newData = new CrackdownPushNotification({
    					crackdownId : _crackdownID,
    					userId : _user_info._id,
    					distance: distanceType,
    					createTime : new Date().getTime()
    				});
    				
    				newData.save();
    				
    				pushMsg.sendMessage(message, [_user_info.gcmId], function(_err) {
    					if(_err) {
    						logger.error(_err);
    						logger.error(_err.stack);
    					}
    				});
    			}
    			
    			_callback();
    		}
    	});
	}, function(err) {
		if(err) {
			logger.error(_err);
			logger.error(_err.stack);
		}
	});
}

function registerCrackdownComment(req, res) {
	if(svr_utils.checkParams(['parentIndex', 'crackdownId', 'depth', 'userId', 'registrant', 'comment'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var parentIndex = parseInt(req.params.parentIndex);
	var depth = parseInt(req.params.depth);
	
	Async.waterfall([
 		function (next) {
 			Crackdown.findById(new ObjectId(req.params.crackdownId)).exec(next);
 		},
 		function(crackdown_info, next) {
 			if(!crackdown_info) {
 				res.send({ result: error_code.NO_SUCH_CRACKDOWN });
				return;
 			}
 			
 			User.findById(new ObjectId(req.params.userId)).exec(function(err, user_info) {
 				if(err) {
 					next(err);
 				} else {
 					next(null, crackdown_info, user_info);
 				}
 			});
 		},
 		function(crackdown_info, user_info, next) {
 			if(!user_info) {
 				res.send({ result: error_code.NO_SUCH_USER });
				return;
 			}
 			
 			var newCommentData = {
 				userId:			req.params.userId,
 				registrant:		req.params.registrant,
 				contents:		req.params.comment,
 				createTime:		new Date().getTime()
 			};
 			
 			if(depth == 1) {
 				newCommentData.sub_comments = [];
 				crackdown_info.comments.push(newCommentData);
 			} else if(depth == 2) {
 				if(crackdown_info.comments.length > 0 && crackdown_info.comments[parentIndex]) {
					crackdown_info.comments[req.params.parentIndex].sub_comments.push(newCommentData);
 				} else {
 					res.send({ result: error_code.NOT_ENOUGH_PARAMS });
 					return;
 				}
 			} else {
 				res.send({ result: error_code.NOT_ENOUGH_PARAMS });
				return;
 			}
 			
 			crackdown_info.commentCount = crackdown_info.comments.length;
 			
 			crackdown_info.save(function(err) {
 				next(err, crackdown_info, user_info);
 			});
 		},
 		function(crackdown_info, user_info, next) {
 			res.send({ result: error_code.SUCCESS, comment_list: crackdown_info.comments });
 			
 			crackdown_info = crackdown_info.getData();
 			var newCommentData;
 			if(depth == 1) {
 				newCommentData = crackdown_info.comments[crackdown_info.comments.length - 1];
 			} else if(depth == 2) {
 				newCommentData = crackdown_info.comments[parentIndex].sub_comments[crackdown_info.comments[parentIndex].sub_comments.length - 1];
 			}
 			
 			if(newCommentData) {
 				var log_crackdown_comment = new _LogCrackdownComment({
 					crackdownCommentId:	newCommentData._id,
 					crackdownId:	crackdown_info.crackdownId,
 					depth:			depth,
 					userId:			user_info.userId,
 					registrant:		newCommentData.registrant,
 					contents:		newCommentData.contents,
 					createTime:		newCommentData.createTime,
 					logType: 		"register",
 					logDate: 		new Date()
 				});
 				
 				log_crackdown_comment.save();
 			}
 			
 			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function updateCrackdownAction(req, res) {
	if(svr_utils.checkParams(['crackdownId', 'userId', 'actionType'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
	 	function (next) {
	 		Crackdown.findById(new ObjectId(req.params.crackdownId)).exec(next);
	 	},
  		function(crackdown_info, next) {
  			if(!crackdown_info) {
  				res.send({ result: error_code.NO_SUCH_CRACKDOWN });
 				return;
  			}
  			
  			User.findById(new ObjectId(req.params.userId)).exec(function(err, user_info) {
  				if(err) {
  					next(err);
  				} else {
  					next(null, crackdown_info, user_info);
  				}
  			});
  		},
  		function(crackdown_info, user_info, next) {
  			if(!user_info) {
  				res.send({ result: error_code.NO_SUCH_USER });
 				return;
  			}
  			
  			CrackdownAction.findOne({crackdownId: req.params.crackdownId, userId: req.params.userId}).exec(function(err, crackdown_action) {
  				if(err) {
  					next(err);
  				} else {
  					next(null, crackdown_info, user_info, crackdown_action);
  				}
  			});
  		},
  		function(crackdown_info, user_info, crackdown_action, next) {
  			if(crackdown_action) {
  				if(crackdown_action.actionType == "like") {
  					res.send({ result: error_code.DUPLICATE_LIKE });
  				} else if(crackdown_action.actionType == "dislike") {
  					res.send({ result: error_code.DUPLICATE_DISLIKE });
  				} else {
  					res.send({ result: error_code.DUPLICATE_LIKE });
  				}
  				
 				return;
  			}
  			
  			if(req.params.actionType == "like") {
  				crackdown_info.likeCount = parseInt(crackdown_info.likeCount) + 1;
  			} else if(req.params.actionType == "dislike") {
  				crackdown_info.dislikeCount = parseInt(crackdown_info.dislikeCount) + 1;
  			} else if(req.params.actionType == "share") {
  				
  			} else {
  				res.send({ result: error_code.NOT_ENOUGH_PARAMS });
 				return;
  			}
  			
  			crackdown_info.save(function(err) {
  				if(err) {
 					next(err);
 				} else {
 					next(null, crackdown_info, user_info, crackdown_action);
 				}
  			});
  		},
  		function(crackdown_info, user_info, crackdown_action, next) {
  			var retCrackdownInfo = crackdown_info.getData();
  			user_info = user_info.getData();
  			var crackdown_action = new CrackdownAction({
  				crackdownId:	retCrackdownInfo.crackdownId,
  				userId:			user_info.userId,
  				actionType:		req.params.actionType,
  				createTime:		new Date().getTime()
  			});
  			
  			crackdown_action.save(function(err) {
  				if(err) {
 					next(err);
 				} else {
 					if(crackdown_action) {
						if(crackdown_action.actionType == "like") {
							retCrackdownInfo.isUserLike = true;
						} else if(crackdown_action.actionType == "dislike") {
							retCrackdownInfo.isUserDislike = true;
						}
					}
 						
 					res.send({ result: error_code.SUCCESS, crackdown_detail: retCrackdownInfo });
 					next(null, crackdown_action);
 				}
  			});
  		},
  		function(crackdown_action, next) {
  			crackdown_action = crackdown_action.getData();
  			var log_crackdown_action = new _LogCrackdownAction({
  				crackdownActionId:	crackdown_action.crackdownActionId,
  				crackdownId:	crackdown_action.crackdownId,
  				userId:			crackdown_action.userId,
  				actionType:		crackdown_action.actionType,
  				logDate: 		new Date()
  			});
  			
  			log_crackdown_action.save();
  			next();
  		}
  	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function uploadCrackdownFile(req, res) {
	var extList = [ "png", "jpg", "jpeg", "gif" ];
    var result = false;
    
    for (var i in extList) {
    	if (extList.hasOwnProperty(i)) {
            if (String(req.files.file).indexOf(extList[i]) != -1) {
            	result = true;
            }
    	}
    }
    
    var time = new Date().getTime();
	var fileorignnameArr = req.files.uploadFile.name.split(".");
    var fileName = decodeURI( svr_utils.getUniqueId() + "_" + time + "." + fileorignnameArr[1] );
    var uploadPath = config.CRACKDOWN.CRACKDOWN_PATH + fileName;
    
    mv(req.files.uploadFile.path, rootPath + config.UPLOAD_ROOT_PATH + uploadPath, { mkdirp: true }, function (err) {
        if (err) {
        	errFunction(err, req, res);
        } else {
        	res.send({ result: error_code.SUCCESS, filePath: uploadPath });
        }
    });
}

function uploadShareFile(req, res) {
	var extList = [ "png", "jpg", "jpeg", "gif" ];
    var result = false;
    
    for (var i in extList) {
    	if (extList.hasOwnProperty(i)) {
            if (String(req.files.file).indexOf(extList[i]) != -1) {
            	result = true;
            }
    	}
    }
    
    var time = new Date().getTime();
	var fileorignnameArr = req.files.uploadFile.name.split(".");
    var fileName = decodeURI( svr_utils.getUniqueId() + "_" + time + "." + fileorignnameArr[1] );
    var uploadPath = config.CRACKDOWN.SHARED_PATH + fileName;
    
    mv(req.files.uploadFile.path, rootPath + config.UPLOAD_ROOT_PATH + uploadPath, { mkdirp: true }, function (err) {
        if (err) {
        	errFunction(err, req, res);
        } else {
        	res.send({ result: error_code.SUCCESS, filePath: uploadPath });
        }
    });
}

function deleteCrackdown(req, res) {
	if(svr_utils.checkParams(['crackdownId', 'userId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
 		function (next) {
 			User.findById(new ObjectId(req.params.userId)).exec(next);
 		},
 		function (user_info, next) {
 			if(!user_info) {
 				res.send({ result: error_code.NO_SUCH_USER });
				return;
 			}
 			
 			Crackdown.findById(new ObjectId(req.params.crackdownId)).exec(function(err, crackdown_info) {
 				next(err, user_info, crackdown_info);
 			});
 		},
 		function(user_info, crackdown_info, next) {
 			if(!crackdown_info) {
 				res.send({ result: error_code.NO_SUCH_CRACKDOWN });
				return;
 			}
 			
 			user_info = user_info.getData();
 			crackdown_info = crackdown_info.getData();
 			
 			if(crackdown_info.userId != user_info.userId) {
 				res.send({ result: error_code.NOT_MATCH_USER });
 				return;
 			}
 			
 			Crackdown.remove({"_id" : new ObjectId(crackdown_info.crackdownId)}, function(err, data) {
				if(err) {
					next(err);
				} else {
					res.send({ result: error_code.SUCCESS });
					next(err);
				}
			});
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

exports.initialize = initialize;
exports.getCrackdownList = getCrackdownList;
exports.getCrackdownDetail = getCrackdownDetail;
exports.registerCrackdown = registerCrackdown;
exports.updateCrackdownAction = updateCrackdownAction;
exports.registerCrackdownComment = registerCrackdownComment;
exports.uploadCrackdownFile = uploadCrackdownFile;
exports.uploadShareFile = uploadShareFile;
exports.getCrackdownRankList = getCrackdownRankList;
exports.deleteCrackdown = deleteCrackdown;
