var Async				= require('async');
var BusNew				= DB.model('BusNew');
var Alba				= DB.model('Alba');
var Nosun				= DB.model('Nosun');
var Chat				= DB.model('Chat');
var Driver				= DB.model('Driver');
var ShuttlePoint		= DB.model('ShuttlePoint');
var ShuttleInfo 		= DB.model('ShuttleInfo');
var _LogChat			= DB.model('_LogChat');
var BusStation			= DB.model('BusStation');
var BusRoute			= DB.model('BusRoute');
var BusRouteStation		= DB.model('BusRouteStation');
var SubwayStation		= DB.model('SubwayStation');
var SubwayTime			= DB.model('SubwayTime');
var FrequentlyUsedWord 	= DB.model('FrequentlyUsedWord');
var ShuttleCategory		= DB.model('ShuttleCategory');
var ShuttleRealTimePosition = DB.model('ShuttleRealTimePosition');
var gcm					= require("node-gcm");
var ObjectId			= DB.Types.ObjectId;
var PushMessage			= require(rootPath + '/lib/PushMessage');
var sender;

function initialize (server) {
	sender = new gcm.Sender(config.GCM.SENDER_ID);
	server.get('/busnew', busList);
	server.get('/busnew2', busList2);
	server.get('/busnew/:busId', busOne);
	server.post('/busnew', createBus);
	server.get("/busnew/:busId/comment", getComment);
	server.post("/busnew/:busId/comment", addComment);
	server.post('/busnew/:busId/edit', edit);
	server.post('/busnew/:busId/desc', recordDesc);
	server.post('/busnew/:busId/phone', recordPhone);
	server.post('/busnew/:busId/record', recordBus);
	server.post('/busnew/:busId/edittext', editText);
	server.get('/busnew2/:busId/points', getShuttlePoints);
	server.get('/busnew2/station', getShuttleStation);
	server.get('/busnew2/station/updated', getUpdatedShuttleStation);
	server.post('/busnew2/:busId/:driverId/start', startChatting);
	server.post('/busnew2/:busId/:driverId/end', endChatting);
	server.get('/shuttle/list', getShuttleList);
	server.get('/shuttle/category', getShuttleCategoryList);
	server.post('/shuttle/:busId/req_location', requestLocation);
	server.post('/shuttle/:busId/res_location', responseLocation);
	server.get('/shuttle/frequently/used', getFrequentlyUsedList);
	server.get('/shuttle/:arsId/list', getShuttleListByStationId);
	server.get("/public/transit/near/station/list", getNearPublicTransitStationList);
	server.get("/public/transit/route/list", getPublicTransitRouteListByStationId);
	server.get("/public/transit/route/detail", getPublicTransitRouteDetail);
	server.get("/public/transit/station/list", getPublicTransitStationListByRouteId);
	server.get("/public/transit/nightbus/list", getPublicTransitNightBusList);
	server.get('/shuttle/listall', getShuttleListAll);
	server.get('/shuttle/realtime/position', getShuttleRealTimePosition);
	server.get("/busnew/:busId/wokitokilist", getWokitoki);
	server.post("/busnew/:busId/wokitokilist", addWokitoki);
	server.post('/shuttle/transfer', getShuttleTransfer);
}

/***
 * HTTP GET /bus
 * REQUEST PARAMETERS : len(한번에 가져올 길이), pageNo(페이지 수)
 * RESPONSE PARAMETERS : result(결과값), buses(버스 목록), length(총 버스 수)
 */
function busList (req, res) {
	if (svr_utils.checkParams(['len', 'pageNo'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	var len = parseInt(req.params.len);
	var pageNo = parseInt(req.params.pageNo);

	Async.waterfall([
		function (next) {
			BusNew.find({}).sort({ _id: -1 }).skip((pageNo - 1) * len).limit(len).exec(next);
		}, function (buses, next) {
			for (var i in buses) if (buses.hasOwnProperty(i)) {
				buses[i] = buses[i].getData();
			}

			BusNew.find({}).sort({ _id: -1 }).skip((pageNo - 1) * len).limit(len).exec(function (err, list) {
				next(err, buses, list);
			});
		}, function (buses, list, next) {
			res.send({ result: error_code.SUCCESS, buses: buses, length: list.length });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function busList2(req, res){
	if (svr_utils.checkParams(['lat', 'lng', 'range'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	var range = parseFloat(req.params.range);

	var lngBase = range / 88800;
	var latBase = range / 1100000;

	var query	= {
		'lists.lat': { $gte: lat - latBase, $lte: lat + latBase },
		'lists.lng': { $gte: lng - lngBase, $lte: lng + lngBase }
	};

	Async.waterfall([
		function (next) {
			BusNew.find(query, {name: 1, category: 1, desc: 1}).sort({ _id: -1 }).limit(30).exec(next);
		}, function (buses, next) {
			for (var i in buses){
				if (buses.hasOwnProperty(i)) {
					buses[i] = buses[i].getData();
				}
			}

			BusNew.find(query, {name: 1, category: 1, desc: 1, comment: 1}).sort({ _id: -1 }).limit(30).exec(function (err, list) {
				next(err, buses, list);
			});
		}, function (buses, list, next) {
			res.send({ result: error_code.SUCCESS, buses: buses, length: list.length });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function getShuttleList(req, res) {
	if (svr_utils.checkParams(['lat', 'lng', 'range'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	var range = parseFloat(req.params.range);

	Async.waterfall([
		function (next) {
			ShuttleCategory.find({}, {categoryName : 1}).sort({categoryIndex: 1}).exec(next);
		},
		function (shuttle_catgory, next) {
			var retShuttleCategory = [];
			if(shuttle_catgory) {
				var listSize = shuttle_catgory.length;
				for(var i = 0; i < listSize; i++) {
					retShuttleCategory.push(shuttle_catgory[i].categoryName);
				}
			}
			
			next(null, retShuttleCategory);
		},
		function (retShuttleCategory, next) {
			ShuttlePoint.aggregate([
					{
						"$geoNear": {
							"near": {
								"type": "Point",
								"coordinates": [lng, lat]
							},
							"distanceField": "dist.distance",
							"maxDistance": range,
							"spherical": true,
							"limit": 100000
						}
					},
					{
						"$group": { "_id" : "$busId", "minDistance": {"$min": "$dist.distance"}}
					}
				], function(err, shuttlePoints) {
					next(err, shuttlePoints, retShuttleCategory);
				}
			);
		},
		function(_shuttlePoints, _retShuttleCategory, next) {
			var busIds = [];
			var shuttlePoints = {};
			var shuttlePointsLength = _shuttlePoints.length;
			for(var i = 0; i < shuttlePointsLength; i++) {
				busIds.push(new ObjectId(_shuttlePoints[i]._id));
				shuttlePoints[_shuttlePoints[i]._id] = _shuttlePoints[i].minDistance;
			}

			ShuttleInfo.find({"_id" : {"$in": busIds}, "ynEnabled": "Y"}).exec(function(err, shuttles) {
				if(err) {
					next(err);
				} else {
					var retShuttles = [];
					var shuttlesLength = shuttles.length;
					for(var i = 0; i < shuttlesLength; i++) {
						var shuttleInfo = shuttles[i].getData();
						shuttleInfo.minDistance = shuttlePoints[shuttleInfo.busId];
						delete(shuttleInfo.comment);
						
						retShuttles.push(shuttleInfo);
					}

					res.send({ result: error_code.SUCCESS, shuttle_category: _retShuttleCategory, shuttles: retShuttles, length: retShuttles.length });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function getShuttleCategoryList(req, res) {
	Async.waterfall([
		function (next) {
			ShuttleCategory.find({}, {categoryName : 1}).sort({categoryIndex: 1}).exec(next);
		},
		function (shuttle_catgory, next) {
			var retShuttleCategory = [];
			if(shuttle_catgory) {
				var listSize = shuttle_catgory.length;
				for(var i = 0; i < listSize; i++) {
					retShuttleCategory.push(shuttle_catgory[i].categoryName);
				}
			}
			
			res.send({ result: error_code.SUCCESS, shuttle_category: retShuttleCategory});
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function getShuttleListByStationId(req, res) {
	if (svr_utils.checkParams(['arsId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function (next) {
			ShuttlePoint.find({"arsId" : req.params.arsId}, {"busId": 1}).exec(next);
		},
		function(_shuttlePoint, next) {
			if(_shuttlePoint) {
				var busIds = [];
				for(var i = 0; i < _shuttlePoint.length; i++) {
					busIds.push(new ObjectId(_shuttlePoint[i].busId));					
				}
				
				ShuttleInfo.find({"_id" : {"$in": busIds}, "ynEnabled": "Y"}).exec(function(err, shuttles) {
					if(err){
						next(err);
					} else {
						var retShuttles = [];
						var shuttlesLength = shuttles.length;
						for(var i = 0; i < shuttlesLength; i++) {
							var shuttleInfo = shuttles[i].getData();
							delete(shuttleInfo.comment);
							
							retShuttles.push(shuttleInfo);
						}

						res.send({ result: error_code.SUCCESS, shuttles: retShuttles, length: retShuttles.length });
						next();
					}
				});
			} else {
				res.send({ result: error_code.SUCCESS, shuttles: [], length: 0 });
				next();
			}
		}
	], function (err) {
		if (err) {
			errFunction(err, req, res);
		}
	});
}

/***
 * HTTP GET /bus/:busId
 * REQUEST PARAMETERS : busId (bus의 id값)
 * RESPONSE PARAMETERS : result(결과값), bus(버스)
 */
function busOne (req, res) {
	if (svr_utils.checkParams(['busId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function (next) {
			Alba.authorize(req.params.token).exec(next);
		}, function (alba, next) {
			if (!alba) {
				res.send({ result: error_code.NOT_AUTHORIZED });
				return;
			}

			Bus.findById(new ObjectId(req.params.busId)).exec(next);
		}, function (bus, next) {
			if (!bus) {
				// TODO 예외처리를 해주자.
				res.send({ result: error_code.NO_SUCH_BUS });
				return;
			}

			res.send({ result: error_code.SUCCESS, bus: bus.getData() });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

/***
 * HTTP POST /bus
 * REQUEST PARAMETERS : name (이름), desc (구간 등 설명)
 * RESPONSE PARAMETERS : result(결과값), bus(버스)
 */
function createBus (req, res) {
	if (svr_utils.checkParams(['name', 'desc'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function (next) {
			var bus = new BusNew({
				name: req.params.name,
				desc: req.params.desc,
				lists: []
			});

			bus.save(function (err) {
				next(err, bus);
			});
		}, function (bus, next) {
			res.send({ result: error_code.SUCCESS, bus: bus.getData() });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}


/***
 * HTTP POST /bus/:busId/record
 * REQUEST PARAMETERS : lat, lng (위/경도), name (정류장 이름), busId (버스 ID)
 * RESPONSE PARAMETERS : result(결과값), bus(버스)
 */
function recordBus (req, res) {
	if (svr_utils.checkParams(['lat', 'lng', 'name', 'busId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function (next) {
			BusNew.findById(new ObjectId(req.params.busId)).exec(next);
		}, function (bus, next) {
			if (!bus) {
				// TODO 예외처리를 해주자.
				res.send({ result: error_code.NO_SUCH_BUS });
				return;
			}

			var newObject = {
				index: 	bus.lists.length + 1,
				text: 	req.params.name,
				lat: 	parseFloat(req.params.lat),
				lng: 	parseFloat(req.params.lng)
			};

			bus.lists.push(newObject);
			bus.save(function (err) {
				if (err) next(err);
				else {
					res.send({ result: error_code.SUCCESS, bus: bus.getData() });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}
function getComment (req, res) {
	if (svr_utils.checkParams(['busId','len','pageNo'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function (next) {
			ShuttleInfo.findById(new ObjectId(req.params.busId)).exec(next);
		}, function (_shuttleInfo, next) {
			if (!_shuttleInfo) {
				// TODO 예외처리를 해주자.
				res.send({ result: error_code.NO_SUCH_BUS });
				return;
			}

			var length = parseInt(req.params.len);
			var pageNo = parseInt(req.params.pageNo);

			var limitDate = new Date().getTime() - (24 * 60 * 60 * 1000);
			var commentlist = [];
			var tmpCommentList = [];
			
			for(var i = 0; i < _shuttleInfo.comment.length; i++) {
				if(_shuttleInfo.comment[i] && _shuttleInfo.comment[i].createdAt >= limitDate) {
					tmpCommentList.push(_shuttleInfo.comment[i]);
				}
			}

			for(var i = length * (pageNo - 1) + 1; i <= length * (pageNo - 1) + 1 + (length - 1); i++) {
				if(tmpCommentList[i - 1]) {
					commentlist.push(tmpCommentList[i - 1]);
				}
			}

			res.send({ result: error_code.SUCCESS, comments: commentlist });

			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function addComment (req, res) {
	if(svr_utils.checkParams(['busId', "driverId", "messageType", "message"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function(next) {
			ShuttleInfo.findOne({ _id: new ObjectId(req.params.busId) }, {name: 1, category: 1, desc: 1, comment: 1}).exec(next);
		},
		function(_shuttleInfo, next) {
			if(!_shuttleInfo) {
				res.send({ result: error_code.NO_SUCH_SHUTTLE });
			}
			
			Chat.findOne({ busId: req.params.busId }).exec(function(err, _chat_info) {
				next(err, _shuttleInfo, _chat_info);
			});
		},
		function(_shuttleInfo, _chatInfo, next) {
			if(!_chatInfo) {
				res.send({ result: error_code.NO_SUCH_CHAT });
				return;
			}
			
			var driverInfo = null;
			var driverCount = _chatInfo.drivers.length;
			
			if(req.params.nickName) {
				var isDuplication = false;
				for(var i = 0; i < driverCount; i++) {
					if(req.params.driverId != _chatInfo.drivers[i].driverId && req.params.nickName == _chatInfo.drivers[i].nickName) {
						isDuplication = true;
						break;
					} else if(req.params.driverId == _chatInfo.drivers[i].driverId) {
						_chatInfo.drivers[i].nickName = req.params.nickName;
						driverInfo = _chatInfo.drivers[i];
					}
				}

				if(isDuplication) {
					res.send({ result: error_code.DUPLICATE_NICKNAME });
					return;
				} else {
					_chatInfo.save(function(err){
						next(err, driverInfo, _shuttleInfo, _chatInfo);
					});
				}
			} else {
				for(var i = 0; i < driverCount; i++) {
					if(req.params.driverId == _chatInfo.drivers[i].driverId) {
						driverInfo = _chatInfo.drivers[i];
						break;
					}
				}
				
				next(null, driverInfo, _shuttleInfo, _chatInfo);
			}
		},
		function (_driverInfo, _shuttleInfo, _chatInfo, next) {
			if(!_driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
			
			var newObject = {
				messageType: req.params.messageType,
				message: req.params.message,
				driverId: _driverInfo.driverId,
				nickName: _driverInfo.nickName,
				lat: req.params.lat,
				lng: req.params.lng,
				createdAt: new Date().getTime()
			};
			
			_shuttleInfo.comment.push(newObject);
			_shuttleInfo.save(function (err) {
				if (err) {
					next(err);
				} else {
					res.send({ result: error_code.SUCCESS });
					next(null, _driverInfo, _chatInfo);
				}
			});
		},
		function(_driverInfo, _chatInfo, next) {
			if(req.params.messageType == "fuw" || req.params.messageType == "fuw_location") {
				FrequentlyUsedWord.findById(new ObjectId(req.params.wordId)).exec(function(err, word) {
					if(err) {
						next(err);
					} else {
						if(word) {
							word.usedCount = word.usedCount + 1;
							word.save(function(err) {
								next(err, _driverInfo, _chatInfo);
							});
						} else {
							next(null, _driverInfo, _chatInfo);
						}
					}
				});
			} else {
				next(null, _driverInfo, _chatInfo);
			}
		},
		function(_driverInfo, _chatInfo, next) {
			if(_chatInfo) {
				var chatInfo = _chatInfo.getData();
				var driverCount = chatInfo.drivers.length;
				var gcmIds = [];
				for(var i = 0; i < driverCount; i++) {
					gcmIds.push(chatInfo.drivers[i].gcmId);
				}
				
				if(gcmIds.length > 0) {
					var message = new gcm.Message({
						delayWhileIdle: true,
						timeToLive: 3,
						data: {
							type: 'msg_shuttle_chat',
							busId: chatInfo.busId,
							driverId: _driverInfo.driverId,
							nickName: _driverInfo.nickName,
							messageType: req.params.messageType,
							message: req.params.message,
							lat: req.params.lat,
							lng: req.params.lng,
							sendDate: new Date().getTime()
						}
					});
					
					sender.send(message, gcmIds, function (err, result) {
						if(err) {
							logger.error(err.stack);
						}
					});
				}
				
				var log_chat = new _LogChat({
					busId: chatInfo.busId,
					drivers: chatInfo.drivers,
					sendDriverId: _driverInfo.driverId,
					recvDriverId: "",
					message: req.params.message,
					lat: req.params.lat,
					lng: req.params.lng,
					messageType: "msg", // msg(일반메시지), notice(입장/퇴장), req_location(위치 확인 요청), res_location(위치 확인 응답)
					logDate: new Date()
				});
				
				log_chat.save();
			}
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

/***
 * HTTP POST /bus/:busId/edit
 */
function edit(req, res) {
	if (svr_utils.checkParams(['busId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function (next) {
			Alba.authorize(req.params.token).exec(next);
		}, function (alba, next) {
			if (!alba) {
				res.send({ result: error_code.NOT_AUTHORIZED });
				return;
			}

			Bus.findById(new ObjectId(req.params.busId)).exec(next);
		}, function (bus, next) {
			if (!bus) {
				// TODO 예외처리를 해주자.
				res.send({ result: error_code.NO_SUCH_BUS });
				return;
			}

			if (req.params.name) bus.name = req.params.name;
			if (req.params.desc) bus.desc = req.params.desc;
			if (req.params.phone) bus.phone = req.params.phone;
			bus.save(function (err) {
				if (err) next(err);
				else {
					res.send({ result: error_code.SUCCESS, bus: bus.getData() });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

/***
 * HTTP POST /bus/:busId/phone
 */
function recordPhone (req, res) {
	if (svr_utils.checkParams(['phone', 'busId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function (next) {
			BusNew.findById(new ObjectId(req.params.busId)).exec(next);
		}, function (bus, next) {
			if (!bus) {
				// TODO 예외처리를 해주자.
				res.send({ result: error_code.NO_SUCH_BUS });
				return;
			}

			bus.phone = req.params.phone;
			bus.save(function (err) {
				if (err) next(err);
				else {
					res.send({ result: error_code.SUCCESS, bus: bus.getData() });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

/***
 * HTTP POST /bus/:busId/desc
 */
function recordDesc (req, res) {
	if (svr_utils.checkParams(['desc', 'busId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function (next) {
			Alba.authorize(req.params.token).exec(next);
		}, function (alba, next) {
			if (!alba) {
				res.send({ result: error_code.NOT_AUTHORIZED });
				return;
			}

			BusNew.findById(new ObjectId(req.params.busId)).exec(next);
		}, function (bus, next) {
			if (!bus) {
				// TODO 예외처리를 해주자.
				res.send({ result: error_code.NO_SUCH_BUS });
				return;
			}

			bus.desc = req.params.desc;
			bus.save(function (err) {
				if (err) next(err);
				else {
					res.send({ result: error_code.SUCCESS, bus: bus.getData() });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

/***
 * HTTP POST /bus/:busId/edit
 */
function editText(req, res) {
	if (svr_utils.checkParams(['busId','index','text'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function (next) {
			BusNew.findById(new ObjectId(req.params.busId)).exec(next);
		}, function (bus, next) {
			if (!bus) {
				// TODO 예외처리를 해주자.
				res.send({ result: error_code.NO_SUCH_BUS });
				return;
			}
			var index = parseInt(req.params.index);
			if(req.params.text != "" ) bus.lists[index].text = req.params.text;

			bus.save(function (err) {
				if (err) next(err);
				else {
					res.send({ result: error_code.SUCCESS, bus: bus.getData() });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function getShuttlePoints(req, res) {
	if(svr_utils.checkParams(['busId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function (next) {
			ShuttleInfo.findById(new ObjectId(req.params.busId)).exec(next);
		},
		function(_shuttleInfo, next) {
			if(!_shuttleInfo) {
				res.send({ result: error_code.NO_SUCH_SHUTTLE });
				return;
			}
			
			var shuttleInfo = _shuttleInfo.getData();
			ShuttlePoint.find({"busId": shuttleInfo.busId}).sort({"index" : 1}).exec(function(err, _shuttlePoints) {
				if(err) {
					next(err);
				} else {
					shuttleInfo.lists = [];
					for(var i in _shuttlePoints) {
						if(_shuttlePoints.hasOwnProperty(i)) {
							var shuttlePoint = _shuttlePoints[i].getData();
							shuttleInfo.lists.push(shuttlePoint);
						}
					}
					res.send({ result: error_code.SUCCESS, shuttle: shuttleInfo });
					next();
				}
			});
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function getUpdatedShuttleStation(req, res) {
	if(svr_utils.checkParams(['lat', 'lng', 'range'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	var range = parseFloat(req.params.range);
	
	Async.waterfall([
		function (next) {
			ShuttlePoint.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "dist.distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
				}, {
					"$match" : {
						"text": {"$ne" : "__point"},
						"ynEnabled" : "Y"
					}
				},{
					"$group" : {
						"_id" : {
							"arsId" : "$arsId", 
							"text" : "$text"
						},
						"minDistance": {
							"$min": "$dist.distance"
						}
					}
				}, {
					"$sort" : {
						"minDistance" : 1
					}
				}
			], function(err, shuttleMarkerPoints) {
				next(err, shuttleMarkerPoints);
			});
		}, function (_shuttleMarkerPoints, next) {
			var shuttleMarkerLength = _shuttleMarkerPoints.length;
			var retShuttleMarkerPoints = [];
			for(var i = 0; i < shuttleMarkerLength; i++) {
				var shuttleMarkerPointItem = {
					arsId: _shuttleMarkerPoints[i]._id.arsId,
					text: _shuttleMarkerPoints[i]._id.text,
					distance: _shuttleMarkerPoints[i].minDistance,
					stationType: "shuttle"
				};
				
				retShuttleMarkerPoints.push(shuttleMarkerPointItem);
			}
			
			next(null, retShuttleMarkerPoints);
		}, function(_shuttleMarkerPoints, next) {
			ShuttlePoint.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
				}, {
					"$match" : {
						"ynEnabled" : "Y"
					}
				}, {
					"$group": {
						"_id": {
							"busId": "$busId"
						}
					}
				}
			], function(err, shuttlePoints) {
				next(err, _shuttleMarkerPoints, shuttlePoints);
			});
		}, function(_shuttleMarkerPoints, _shuttleLinePoints, next) {
			var shuttleLinePointsLength = _shuttleLinePoints.length;
			var busIds = [];
			for(var i = 0; i < shuttleLinePointsLength; i++) {
				var busId = _shuttleLinePoints[i]._id.busId;
				
				if(busIds.indexOf(busId) == -1) {
					busIds.push(new ObjectId(busId));
				}
			}

			ShuttleInfo.find({"_id" : {"$in": busIds}, "ynEnabled": "Y"}).exec(function(err, shuttles) {
				if(err) {
					next(err);
				} else {
					var retShuttles = [];
					var shuttlesLength = shuttles.length;
					for(var i = 0; i < shuttlesLength; i++) {
						retShuttles.push({
							name: 					shuttles[i].name, 
							busId: 					shuttles[i]._id, 
							lineColor: 				shuttles[i].lineColor,
							firstTime:				shuttles[i].firstTime,
							lastTime:				shuttles[i].lastTime,
							saturdayFirstTime:		shuttles[i].saturdayFirstTime,
							saturdayLastTime:		shuttles[i].saturdayLastTime,
							sundayFirstTime:		shuttles[i].sundayFirstTime,
							sundayLastTime:			shuttles[i].sundayLastTime,
							runFirstTime:			shuttles[i].runFirstTime,
							runLastTime:			shuttles[i].runLastTime,
							saturdayRunFirstTime:	shuttles[i].saturdayRunFirstTime,
							saturdayRunLastTime:	shuttles[i].saturdayRunLastTime,
							sundayRunFirstTime:		shuttles[i].sundayRunFirstTime,
							sundayRunLastTime:		shuttles[i].sundayRunLastTime,
							holidayDate:			shuttles[i].holidayDate
						});
					}

					res.send({ result: error_code.SUCCESS, shuttleMarkerPoints: _shuttleMarkerPoints, shuttles: retShuttles });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function getShuttleStation(req, res) {	
	if(svr_utils.checkParams(['lat', 'lng'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	var range = parseFloat(req.params.range);
	
	Async.waterfall([
		function (next) {
			ShuttlePoint.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "dist.distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
				}, {
					"$match" : {
						"text": {"$ne" : "__point"},
						"ynEnabled" : "Y"
					}
				},{
					"$group" : {
						"_id" : {
							"arsId" : "$arsId", 
							"text" : "$text",
							"busId" : "$busId",
							"locations" : "$locations",
							"exceptOutback" : "$exceptOutback"
						},
						"minDistance": {
							"$min": "$dist.distance"
						},
						"firstTime" : {
							"$min" : "$firstTime"
						},
						"lastTime" : {
							"$max" : "$lastTime"
						},
						"saturdayFirstTime" : {
							"$min" : "$saturdayFirstTime"
						},
						"saturdayLastTime" : {
							"$max" : "$saturdayLastTime"
						},
						"sundayFirstTime" : {
							"$min" : "$sundayFirstTime"
						},
						"sundayLastTime" : {
							"$max" : "$sundayLastTime"
						}
					}
				}, {
					"$sort" : {
						"minDistance" : 1
					}
				}
			], function(err, shuttleMarkerPoints) {
				next(err, shuttleMarkerPoints);
			});
		}, function (_shuttleMarkerPoints, next) {
			var shuttleMarkerLength = _shuttleMarkerPoints.length;
			var retShuttleMarkerPoints = [];
			for(var i = 0; i < shuttleMarkerLength; i++) {
				var shuttleMarkerPointItem = {
					arsId: _shuttleMarkerPoints[i]._id.arsId,
					text: _shuttleMarkerPoints[i]._id.text,
					busId: _shuttleMarkerPoints[i]._id.busId,
					locations: _shuttleMarkerPoints[i]._id.locations,
					exceptOutback: _shuttleMarkerPoints[i]._id.exceptOutback,
					distance: _shuttleMarkerPoints[i].minDistance,
					firstTime: _shuttleMarkerPoints[i].firstTime,
					lastTime: _shuttleMarkerPoints[i].lastTime,
					saturdayFirstTime: _shuttleMarkerPoints[i].saturdayFirstTime,
					saturdayLastTime: _shuttleMarkerPoints[i].saturdayLastTime,
					sundayFirstTime: _shuttleMarkerPoints[i].sundayFirstTime,
					sundayLastTime: _shuttleMarkerPoints[i].sundayLastTime,
					stationType: "shuttle"
				};
				
				retShuttleMarkerPoints.push(shuttleMarkerPointItem);
			}
			
			next(null, retShuttleMarkerPoints);
		}, function(_shuttleMarkerPoints, next) {
			ShuttlePoint.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
				}, {
					"$match" : {
						"ynEnabled" : "Y"
					}
				}, {
					"$group": {
						"_id": {
							"busId": "$busId"
						},
						"shuttle_point" : {
							"$push" : { "busId": "$busId", "index": "$index", "locations" : "$locations", "text" : "$text" }
						}
					}
				}
			], function(err, shuttlePoints) {
				next(err, _shuttleMarkerPoints, shuttlePoints);
			});
		}, function(_shuttleMarkerPoints, _shuttleLinePoints, next) {
			var shuttleLinePointsLength = _shuttleLinePoints.length;
			var busIds = [];
			for(var i = 0; i < shuttleLinePointsLength; i++) {
				var busId = _shuttleLinePoints[i]._id.busId;
				
				if(busIds.indexOf(busId) == -1) {
					busIds.push(new ObjectId(busId));
				}
			}

			ShuttleInfo.find({"_id" : {"$in": busIds}, "ynEnabled" : "Y"}).exec(function(err, shuttles) {
				if(err) {
					next(err);
				} else {
					var retShuttles = [];
					var shuttlesLength = shuttles.length;
					for(var i = 0; i < shuttlesLength; i++) {
						retShuttles.push({
							name: 					shuttles[i].name, 
							busId: 					shuttles[i]._id, 
							lineColor: 				shuttles[i].lineColor,
							firstTime:				shuttles[i].firstTime,
							lastTime:				shuttles[i].lastTime,
							saturdayFirstTime:		shuttles[i].saturdayFirstTime,
							saturdayLastTime:		shuttles[i].saturdayLastTime,
							sundayFirstTime:		shuttles[i].sundayFirstTime,
							sundayLastTime:			shuttles[i].sundayLastTime,
							runFirstTime:			shuttles[i].runFirstTime,
							runLastTime:			shuttles[i].runLastTime,
							saturdayRunFirstTime:	shuttles[i].saturdayRunFirstTime,
							saturdayRunLastTime:	shuttles[i].saturdayRunLastTime,
							sundayRunFirstTime:		shuttles[i].sundayRunFirstTime,
							sundayRunLastTime:		shuttles[i].sundayRunLastTime,
							holidayDate:			shuttles[i].holidayDate,
							realTimeCategoryName:	shuttles[i].realTimeCategoryName
						});
					}

					res.send({ result: error_code.SUCCESS, shuttleMarkerPoints: _shuttleMarkerPoints, shuttleLinePoints: _shuttleLinePoints, shuttles: retShuttles });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function startChatting(req, res) {
	if(svr_utils.checkParams(['busId', 'driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function (next) {
			Driver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function(_driverInfo, next) {
			if(!_driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
			
			_driverInfo = _driverInfo.getData();
			
			ShuttleInfo.findById(new ObjectId(req.params.busId)).exec(function(err, _shuttleInfo) {
				next(err, _driverInfo, _shuttleInfo);
			});
		}, function (_driverInfo, _shuttleInfo, next) {
			if (!_shuttleInfo) {
				res.send({ result: error_code.NO_SUCH_BUS });
				return;
			}
			
			_shuttleInfo = _shuttleInfo.getData();
			
			Chat.findOne({ busId: _shuttleInfo.busId }).exec(function(err, _chatInfo) {
				next(err,_driverInfo, _shuttleInfo, _chatInfo);
			});
		}, function(_driverInfo, _shuttleInfo, _chatInfo, next) {
			if(_chatInfo) {
				var driverIndex = -1;
				var driverCount = _chatInfo.drivers.length;
				for(var i = 0; i < driverCount; i++) {
					if(_chatInfo.drivers[i].driverId == req.params.driverId) {
						driverIndex = i;
						break;
					}
				}

				if(driverIndex >= 0) {
					_chatInfo.drivers = _chatInfo.drivers.slice(0, driverIndex).concat(_chatInfo.drivers.slice(driverIndex + 1, _chatInfo.drivers.length));
					_chatInfo.save(function(err) {
						if(err) {
							next(err);
						} else {
							next(null, _driverInfo, _shuttleInfo, _chatInfo);
						}
					});
				} else {
					next(null, _driverInfo, _shuttleInfo, _chatInfo);
				}
			} else {
				next(null, _driverInfo, _shuttleInfo, _chatInfo);
			}
		}, function(_driverInfo, _shuttleInfo, _chatInfo, next) {
			if(_chatInfo) {
				var driverCount = _chatInfo.drivers.length;
				if(req.params.nickName) {
					var isDuplication = false;
					for(var i = 0; i < driverCount; i++) {
						if(req.params.driverId != _chatInfo.drivers[i].driverId && req.params.nickName == _chatInfo.drivers[i].nickName) {
							samePhoneCount++;
							isDuplication = true;
							break;
						}
					}
					
					if(isDuplication) {
						res.send({ result: error_code.DUPLICATE_NICKNAME });
						return;
					} else {
						_driverInfo.nickName = req.params.nickName;
						next(null, _driverInfo, _shuttleInfo, _chatInfo);
					}
				} else {
					_driverInfo.nickName = _driverInfo.phone.substring(_driverInfo.phone.length - 4);
					var samePhoneCount = 0;
					for(var i = 0; i < driverCount; i++) {
						var findSplited = _chatInfo.drivers[i].nickName.split('(');
						if(findSplited.lengh > 1 && _driverInfo.nickName == findSplited[0]) {
							samePhoneCount++;
						}
					}
					
					if(samePhoneCount > 0) {
						_driverInfo.nickName += "(" + samePhoneCount + ")";
					}
					
					next(null, _driverInfo, _shuttleInfo, _chatInfo);
				}
			} else {
				if(req.params.nickName) {
					_driverInfo.nickName = req.params.nickName;
				} else {
					_driverInfo.nickName = _driverInfo.phone.substring(_driverInfo.phone.length - 4);
				}
				
				next(null, _driverInfo, _shuttleInfo, _chatInfo);
			}
		}, function (_driverInfo, _shuttleInfo, _chatInfo, next) {
			var driver = {
				driverId: _driverInfo.driverId,
				gcmId: _driverInfo.gcmId,
				nickName: _driverInfo.nickName,
				joinDate: new Date()
			};

			if(!_chatInfo) {
				_chatInfo = new Chat({
					busId: _shuttleInfo.busId,
					drivers: [driver],
					createDate: new Date()
				});
			} else {
				var isExist = false;
				var driverCount = _chatInfo.drivers.length;
				for(var i = 0; i < driverCount; i++) {
					if(_chatInfo.drivers[i].driverId == driver.driverId) {
						_chatInfo.drivers[i].gcmId = driver.gcmId;
						_chatInfo.drivers[i].nickName = driver.nickName;
						_chatInfo.drivers[i].joinDate = driver.joinDate;
						isExist = true;
						break;
					}
				}

				if(!isExist) {
					_chatInfo.drivers.push(driver);
				}
			}

			_chatInfo.save(function(err) {
				next(err, _driverInfo, _shuttleInfo, _chatInfo);
			});
		}, function (_driverInfo, _shuttleInfo, _chatInfo, next) {
			var limitDate = new Date().getTime() - (24 * 60 * 60 * 1000);
			var commentlist = [];

			for(var i = 0; i < _shuttleInfo.comment.length; i++) {
				if(_shuttleInfo.comment[i] && _shuttleInfo.comment[i].createdAt >= limitDate) {
					commentlist.push(_shuttleInfo.comment[i]);
				}
			}
			
			commentlist = commentlist.sort(function(_a, _b) { return _a.createdAt - _b.createdAt});
			
			var drivers = [];
			var gcm_list = [];
			for(var i = 0; i < _chatInfo.drivers.length; i++) {
				drivers.push({
					"driverId" : _chatInfo.drivers[i].driverId,
					"nickName" : _chatInfo.drivers[i].nickName,
					"joinDate" : _chatInfo.drivers[i].joinDate
				});
				
				gcm_list.push(_chatInfo.drivers[i].gcmId);
			}
			
			res.send({ result: error_code.SUCCESS, comments: commentlist, joinedDrivers: drivers, nickName: _driverInfo.nickName });
			
			var sendMsg = '[' + _driverInfo.nickName + ']님이 입장하셨습니다.';
			if(gcm_list.length > 0) {
				var pushMsg = new PushMessage();
				var message = {
					type: 'msg_shuttle_chat_notice',
					busId: _shuttleInfo.busId,
					drivers: drivers,
					message: sendMsg,
					sendDate: new Date().getTime()
				};
				
				pushMsg.sendMessage(message, gcm_list, function(_err) {
					if(_err) {
						logger.error(_err);
						logger.error(_err.stack);
					}
				});
			}
			
			var log_chat = new _LogChat({
				busId: _shuttleInfo.busId,
				drivers: _chatInfo.drivers,
				sendDriverId: _driverInfo.driverId,
				recvDriverId: "",
				message: sendMsg,
				lat: null,
				lng: null,
				messageType: "notice", // msg(일반메시지), notice(입장/퇴장), req_location(위치 확인 요청), res_location(위치 확인 응답)
				logDate: new Date()
			});

			log_chat.save();

			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function endChatting(req, res) {
	if(svr_utils.checkParams(['busId', 'driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function(next) {
			Chat.findOne({ busId: req.params.busId }).exec(next);
		},
		function(_chatInfo, next) {
			if(_chatInfo) {
				var driverIndex = -1;
				var driverCount = _chatInfo.drivers.length;
				for(var i = 0; i < driverCount; i++) {
					if(_chatInfo.drivers[i].driverId == req.params.driverId) {
						driverIndex = i;
						break;
					}
				}
				
				if(driverIndex >= 0) {
					var driverInfo = _chatInfo.drivers[driverIndex];
					var sendMsg = '[' + driverInfo.nickName + ']님이 퇴장하셨습니다.';

					_chatInfo.drivers = _chatInfo.drivers.slice(0, driverIndex).concat(_chatInfo.drivers.slice(driverIndex + 1, _chatInfo.drivers.length));
					
					if(_chatInfo.drivers.length > 0) {
						var drivers = [];
						var gcm_list = [];
						for(var i = 0; i < _chatInfo.drivers.length; i++) {
							drivers.push({
								"driverId" : _chatInfo.drivers[i].driverId,
								"nickName" : _chatInfo.drivers[i].nickName,
								"joinDate" : _chatInfo.drivers[i].joinDate
							});
							
							gcm_list.push(_chatInfo.drivers[i].gcmId);
						}
						
						var pushMsg = new PushMessage();
						var message = {
							type: 'msg_shuttle_chat_notice',
							busId: req.params.busId,
							drivers: drivers,
							message: sendMsg,
							sendDate: new Date().getTime()
						};
						
						pushMsg.sendMessage(message, gcm_list, function(_err) {
							if(_err) {
								errFunction(_err, req, res);
							}
						});
					}
					
					_chatInfo.save(function(err) {
						if(err) {
							next(err);
						} else {
							var log_chat = new _LogChat({
								busId: req.params.busId,
								drivers: _chatInfo.drivers,
								sendDriverId: driverInfo.driverId,
								recvDriverId: "",
								message: sendMsg,
								lat: null,
								lng: null,
								messageType: "notice", // msg(일반메시지), notice(입장/퇴장), req_location(위치 확인 요청), res_location(위치 확인 응답)
								logDate: new Date()
							});

							log_chat.save();
							
							next();
						}
					});
				} else {
					next();
				}
			} else {
				next();
			}
		}, function(next) {
			res.send({ result: error_code.SUCCESS });
			next();
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function requestLocation(req, res) {
	if(svr_utils.checkParams(['busId', 'sendDriverId', 'recvDriverId', 'message'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
			function(next) {
				Chat.findOne({ busId: req.params.busId }).exec(next);
			},
			function(_chatInfo, next) {
				if(!_chatInfo) {
					res.send({ result: error_code.NO_SUCH_CHAT });
					return;
				}

				var chatInfo = _chatInfo.getData();
				var driverCount = chatInfo.drivers.length;
				var sendDriverInfo = null;
				var recvDriverInfo = null;
				for(var i = 0; i < driverCount; i++) {
					if(req.params.recvDriverId == chatInfo.drivers[i].driverId) {
						recvDriverInfo = chatInfo.drivers[i];
					}

					if(req.params.sendDriverId == chatInfo.drivers[i].driverId) {
						sendDriverInfo = chatInfo.drivers[i];
					}

					if(sendDriverInfo && recvDriverInfo) {
						break;
					}
				}

				if(!sendDriverInfo || !recvDriverInfo) {
					res.send({ result: error_code.NO_SUCH_DRIVER });
					return;
				} else {
					var message = new gcm.Message({
						delayWhileIdle: true,
						timeToLive: 3,
						data: {
							type: 'msg_req_location',
							sendDriverId: sendDriverInfo.driverId,
							sendNickName: sendDriverInfo.nickName,
							message: req.params.message,
							sendDate: new Date().getTime()
						}
					});

					sender.send(message, [recvDriverInfo.gcmId], function(err, result){
						if(err) {
							logger.error(err.stack);
						}
					});

					res.send({ result: error_code.SUCCESS });
					
					var log_chat = new _LogChat({
						busId: chatInfo.busId,
						drivers: chatInfo.drivers,
						sendDriverId: sendDriverInfo.driverId,
						recvDriverId: recvDriverInfo.driverId,
						message: req.params.message,
						lat: null,
						lng: null,
						messageType: "req_location", // msg(일반메시지), notice(입장/퇴장), req_location(위치 확인 요청), res_location(위치 확인 응답)
						logDate: new Date()
					});

					log_chat.save();
					
					next();
				}
			}
		], function(err) {
			if(err) {
				errFunction(err, req, res);
			}
		}
	);
}

function responseLocation(req, res) {
	if(svr_utils.checkParams(['busId', 'sendDriverId', 'recvDriverId', 'result'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
			function(next) {
				Chat.findOne({ busId: req.params.busId }).exec(next);
			},
			function(_chatInfo, next) {
				if(!_chatInfo) {
					res.send({ result: error_code.NO_SUCH_CHAT });
					return;
				}

				var chatInfo = _chatInfo.getData();
				var driverCount = chatInfo.drivers.length;
				var sendDriverInfo = null;
				var recvDriverInfo = null;
				for(var i = 0; i < driverCount; i++) {
					if(req.params.sendDriverId == chatInfo.drivers[i].driverId) {
						sendDriverInfo = chatInfo.drivers[i];
					}

					if(req.params.recvDriverId == chatInfo.drivers[i].driverId) {
						recvDriverInfo = chatInfo.drivers[i];
					}

					if(sendDriverInfo && recvDriverInfo) {
						break;
					}
				}

				if(!sendDriverInfo || !recvDriverInfo) {
					res.send({ result: error_code.NO_SUCH_DRIVER });
					return;
				} else {
					var message = new gcm.Message({
						delayWhileIdle: true,
						timeToLive: 3,
						data: {
							type: 'msg_res_location',
							sendNickName: sendDriverInfo.nickName,
							result: req.params.result,
							lat: req.params.lat,
							lng: req.params.lng
						}
					});

					sender.send(message, [recvDriverInfo.gcmId], function(err, result){
						if(err) {
							logger.error(err.stack);
						}
					});

					res.send({ result: error_code.SUCCESS });
					
					var log_chat = new _LogChat({
						busId: chatInfo.busId,
						drivers: chatInfo.drivers,
						sendDriverId: sendDriverInfo.driverId,
						recvDriverId: recvDriverInfo.driverId,
						message: req.params.result,
						lat: req.params.lat,
						lng: req.params.lng,
						messageType: "res_location", // msg(일반메시지), notice(입장/퇴장), req_location(위치 확인 요청), res_location(위치 확인 응답)
						logDate: new Date()
					});

					log_chat.save();
					
					next();
				}
			}
		], function(err) {
			if(err) {
				errFunction(err, req, res);
			}
		}
	);
}

function getFrequentlyUsedList(req, res) {
	Async.waterfall([
			function(next) {
				FrequentlyUsedWord.find().sort({ priority: 1 }).exec(next);
			},
			function(words, next) {
				for (var i in words) if (words.hasOwnProperty(i)) {
					words[i] = words[i].getData();
				}
				
				res.send({result: error_code.SUCCESS, lists: words });
				next();
			}
		], function(err) {
			if(err) {
				errFunction(err, req, res);
			}
		}
	);
}

/**
 * 주변 버스/지하철 정류장 조회
 * @param req
 * @param res
 */
function getNearPublicTransitStationList(req, res) {
	if (svr_utils.checkParams(['lat', 'lng', 'range'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	var range = parseFloat(req.params.range);
	
	if(range < 100 || range > 5000) {
		res.send({ result: error_code.INTERNAL_ERROR });
		return;
	}
	
	var retBusStationList = [];
	var retSubwayStationList = [];
	
	Async.waterfall([
		function (next) {			
			BusStation.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "dist.distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
				},
				{ 
					"$match": { 
						"arsId" : { "$ne" : '' }
					}
				},
				{
					"$group": {
						"_id": {
							"stationId" : "$stationId",
							"stationName" : "$stationName",
							"arsId" : "$arsId",
							"gpsX" : "$gpsX",
							"gpsY" : "$gpsY"
						},
						"minDistance": {
							"$min" : "$dist.distance"
						}
					}
				},
				{
					"$sort": {
						"minDistance": 1
					}
				}
			], function(err, busStationList) {
				next(err, busStationList);
			});
		}, function (busStationList, next) {
			var busStationIdList = [];
			var stationListLength = busStationList.length;
			
			if(stationListLength > 0) {
				for(var i = 0; i < stationListLength; i++) {
					var busStation = {
						stationId : busStationList[i]._id.stationId,
						stationName : busStationList[i]._id.stationName,
						arsId : busStationList[i]._id.arsId,
						gpsX : busStationList[i]._id.gpsX,
						gpsY : busStationList[i]._id.gpsY,
						distance : busStationList[i].minDistance,
						startTime : busStationList[i].startTime,
						endTime : busStationList[i].endTime
					};
					
					retBusStationList.push(busStation);
					busStationIdList.push(busStation.stationId);
				}

				BusRouteStation.aggregate([
					{ 
						"$match": { 
							"stationId" : { "$in" : busStationIdList }
						}
					},
					{
						"$group": {
							"_id": {
								"busRouteId" : "$busRouteId",
								"stationId" : "$stationId"
							},
							"startTime": {
								"$min" : "$startTime"
							},
							"endTime": {
								"$max" : "$endTime"
							}
						}
					}
				], function(err, busRoutesStationList) {
					if(err) {
						next(err);
					} else {
						var retBusRoutesStation = {};
						var busRouteIdList = [];
						var listLength = busRoutesStationList.length;
						for(var i = 0; i < listLength; i++) {
							busRouteIdList.push(busRoutesStationList[i]._id.busRouteId);
							retBusRoutesStation[busRoutesStationList[i]._id.stationId] = {
								stationId : busRoutesStationList[i]._id.stationId,
								busRouteId : busRoutesStationList[i]._id.busRouteId,
								startTime : busRoutesStationList[i].startTime,
								endTime : busRoutesStationList[i].endTime
							}
						}
						
						BusRoute.find({"busRouteId" : {"$in": busRouteIdList}}).exec(function(err, busRouteList) {
							if(err) {
								next(err);
							} else {
								var retBusRoute = {};
								var listLength = busRouteList.length;
								for(var i = 0; i < listLength; i++) {
									retBusRoute[busRouteList[i].busRouteId] = busRouteList[i].busRouteArea;
								}
								
								var stationLength = retBusStationList.length;
								for(var i = 0; i < stationLength; i++) {
									var stationId = retBusStationList[i].stationId;
									var busRouteId = retBusRoutesStation[stationId].busRouteId;
									
									retBusStationList[i].busRouteArea = retBusRoute[busRouteId];
									retBusStationList[i].startTime = retBusRoutesStation[stationId].startTime;
									retBusStationList[i].endTime = retBusRoutesStation[stationId].endTime;
								}
								
								next();
							}
						});
					}
				});
			} else {
				next();
			}
		}, function(next) {
			SubwayStation.aggregate([
  				{
  					"$geoNear": {
  						"near": {
  							"type": "Point",
  							"coordinates": [lng, lat]
  						},
  						"distanceField": "distance",
  						"maxDistance": range,
  						"spherical": true,
  						"limit": 100000
  					}
  				},
  				{
  					"$sort": {
  						"minDistance": 1
  					}
  				}
  			], function(err, subwayStationList) {
  				next(err, subwayStationList);
  			});
		}, function(subwayStationList, next) {
			var subwayStationCodeList = [];
			var stationListLength = subwayStationList.length;
			
			if(stationListLength > 0) {
				for(var i = 0; i < stationListLength; i++) {
					var subwayStation = {
						stationCode : subwayStationList[i].stationCode,
						stationName : subwayStationList[i].stationName,
						location : subwayStationList[i].location,
						stationCodeDaum : subwayStationList[i].stationCodeDaum,
						distance : subwayStationList[i].distance
					};
					
					retSubwayStationList.push(subwayStation);
					subwayStationCodeList.push(subwayStation.stationCode);
				}
				
				var today = new Date();
				var weektag = "" + (today.getDay() < 6 ? 1 : (today.getDay() == 6 ? 2 : 3));
				
				SubwayTime.aggregate([
       				{
       					"$match": {
       						"stationCode": { "$in": subwayStationCodeList },
       						"weekTag": weektag
       					}
       				},
       				{
       					"$group": {
       						"_id": {
       							"stationCode": "$stationCode"
       						},
       						"firstTime": {
       							"$min" : "$firstTime"
       						},
       						"lastTime": {
       							"$max" : "$lastTime"
       						}
       					}
       				}
       			], function(err, subwayTimeList) {
					if(err) {
						next(err);
					} else {
						var listLength = subwayTimeList.length;
						var retSubwayTime = {};
						for(var i = 0; i < listLength; i++) {
							retSubwayTime[subwayTimeList[i]._id.stationCode] = {
								stationCode: subwayTimeList[i]._id.stationCode,
								firstTime: subwayTimeList[i].firstTime,
								lastTime: subwayTimeList[i].lastTime
							};
						}
						
						var subwayStationLength = retSubwayStationList.length;
						for(var i = 0; i < subwayStationLength; i++) {
							var stationCode = retSubwayStationList[i].stationCode;
							
							if(retSubwayTime[stationCode]) {
								retSubwayStationList[i].firstTime = retSubwayTime[stationCode].firstTime.substring(0, 5);
								retSubwayStationList[i].lastTime = retSubwayTime[stationCode].lastTime.substring(0, 5);
							} else {
								retSubwayStationList[i].firstTime = "05:00";
								retSubwayStationList[i].lastTime = "23:00";
							}
						}
						
						res.send({result: error_code.SUCCESS, busStationList: retBusStationList, subwayStationList: retSubwayStationList });
						next();
					}
       			});
			} else {
				res.send({result: error_code.SUCCESS, busStationList: retBusStationList, subwayStationList: retSubwayStationList });
				next();
			}
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

/**
 * 버스 정류장 노선 조회
 * @param req
 * @param res
 */
function getPublicTransitRouteListByStationId(req, res) {
	if (svr_utils.checkParams(['stationId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var stationId = req.params.stationId;
	
	var stationInfo = {};
	var retBusRouteList = [];
	
	Async.waterfall([
	    function (next) {
	    	BusStation.findOne({"stationId" : stationId}).exec(function(err, _retStationInfo) {
				if(err) {
					next(err);
				} else {
					stationInfo = _retStationInfo.getData();
					next();
				}
			});
	    }, function (next) {
			BusRouteStation.find({"stationId" : stationId}, {"busRouteId" : true, "stationSequence" : true, "direction" : true}).exec(function(err, busRouteStationList) {
				if(err) {
					next(err);
				} else {
					var busRouteIdList = [];
					var listLength = busRouteStationList.length;
					for(var i = 0; i < listLength; i++) {
						busRouteIdList.push(busRouteStationList[i].busRouteId);
						
						retBusRouteList.push({
							busRouteId: busRouteStationList[i].busRouteId,
							stationSequence: busRouteStationList[i].stationSequence,
							direction: busRouteStationList[i].direction
						});
					}
					
					BusRoute.find({"busRouteId" : { "$in" : busRouteIdList }}, {"busRouteId" : true, "busRouteName" : true, "busRouteType" : true, "busRouteTypeName" : true, "localBusRouteId": true, "busRouteArea" : true}).exec(function(err, busRouteList) {
						if(err) {
							next(err);
						} else {
							var tempBusRouteList = {};
							var listLength = busRouteList.length;
							for(var i = 0; i < listLength; i++) {
								tempBusRouteList[busRouteList[i].busRouteId] = {
									busRouteId: busRouteList[i].busRouteId,
									busRouteName: busRouteList[i].busRouteName,
									busRouteType: busRouteList[i].busRouteType,
									busRouteTypeName: busRouteList[i].busRouteTypeName,
									localBusRouteId : busRouteList[i].localBusRouteId,
									busRouteArea : busRouteList[i].busRouteArea
								};
							}
							
							Async.each(retBusRouteList, function(_item, _callback) {
								if(tempBusRouteList[_item.busRouteId]) {
									_item.busRouteName = tempBusRouteList[_item.busRouteId].busRouteName;
									_item.busRouteType = tempBusRouteList[_item.busRouteId].busRouteType;
									_item.busRouteTypeName = tempBusRouteList[_item.busRouteId].busRouteTypeName;
									_item.localBusRouteId = tempBusRouteList[_item.busRouteId].localBusRouteId;
									_item.busRouteArea = tempBusRouteList[_item.busRouteId].busRouteArea;
								}
								
								var nextSeq = _item.stationSequence + 1;
								BusRouteStation.findOne({"busRouteId" : _item.busRouteId, "stationSequence" : nextSeq}, {"stationId" : true}).exec(function(err, _retStationId) {
									if(err) {
										_callback(err);
									} else {
										if(_retStationId && _retStationId.stationId) {
											BusStation.findOne({"stationId" : _retStationId.stationId}, { "stationName" : true }).exec(function(err, _retStationInfo) {
												if(err) {
													_callback(err);
												} else {
													if(_retStationInfo && _retStationInfo.stationName) {
														_item.nextStation = _retStationInfo.stationName;
													}
													
													_callback();
												}
											});
										} else {
											_callback();
										}
									}
								});
							}, function(_err, result) {
								if(_err) {
									next(_err);
								} else {
									next();
								}
							});
						}
					});
				}
			});
		}, function (next) {
			res.send({result: error_code.SUCCESS, stationInfo : stationInfo, busRouteList: retBusRouteList });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

/**
 * 버스 노선 상세 정보 조회
 * @param req
 * @param res
 */
function getPublicTransitRouteDetail(req, res) {
	if (svr_utils.checkParams(['busRouteId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var retBusRouteInfo = {};
	var busRouteId = req.params.busRouteId;
	
	BusRoute.findOne({"busRouteId" : busRouteId}).exec(function(err, busRouteInfo) {
		if(err) {
			errFunction(err, req, res);
		} else {
			if(busRouteInfo) {
				retBusRouteInfo = {
						busRouteId : busRouteInfo.busRouteId,
						busRouteName : busRouteInfo.busRouteName,
						busRouteArea : busRouteInfo.busRouteArea,
						startStationName : busRouteInfo.startStationName,
						endStationName : busRouteInfo.endStationName,
						firstTime : busRouteInfo.firstTime,
						lastTime : busRouteInfo.lastTime,
						term : busRouteInfo.term,
						localBusRouteId : busRouteInfo.localBusRouteId
				};
			}

			res.send({result: error_code.SUCCESS, busRouteInfo: retBusRouteInfo });
		}
	});
}

/**
 * 버스 노선 정류장 리스트 조회
 * @param req
 * @param res
 */
function getPublicTransitStationListByRouteId(req, res) {
	if (svr_utils.checkParams(['busRouteId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var busRouteId = req.params.busRouteId;
	
	var retBusRouteInfo = {};
	var retBusStationList = [];
	
	BusRoute.findOne({"busRouteId" : busRouteId}).exec(function(err, busRouteInfo) {
		if(err) {
			errFunction(err, req, res);
		} else {
			retBusRouteInfo = {
				busRouteId : busRouteInfo.busRouteId,
				busRouteName : busRouteInfo.busRouteName,
				busRouteArea : busRouteInfo.busRouteArea,
				startStationName : busRouteInfo.startStationName,
				endStationName : busRouteInfo.endStationName,
				firstTime : busRouteInfo.firstTime,
				lastTime : busRouteInfo.lastTime,
				term : busRouteInfo.term,
				localBusRouteId : busRouteInfo.localBusRouteId
			};
			
			BusRouteStation.find({"busRouteId" : busRouteId}).sort({ "stationSequence" : 1 }).exec(function(err, busRouteStationList) {
				if(err) {
					next(err);
				} else {
					var retBusRouteStation = [];
					var listLength = busRouteStationList.length;
					var stationIdList = [];
					for(var i = 0; i < listLength; i++) {
						retBusRouteStation.push({
							stationId : busRouteStationList[i].stationId,
							stationSequence : busRouteStationList[i].stationSequence,
							startTime : busRouteStationList[i].startTime,
							endTime : busRouteStationList[i].endTime,
							direction : busRouteStationList[i].direction
						});
						
						stationIdList.push(busRouteStationList[i].stationId);
					}
					
					BusStation.find({"stationId" : { "$in" : stationIdList }}, function(err, busStationList) {
						if(err) {
							next(err);
						} else {
							var tempBusStationList = {};
							var listLength = busStationList.length;
							for(var i = 0; i < listLength; i++) {
								tempBusStationList[busStationList[i].stationId] = busStationList[i].getData();
							}
							
							var busRouteStationListLength = retBusRouteStation.length;
							for(var i = 0; i < busRouteStationListLength; i++) {
								var stationId = retBusRouteStation[i].stationId;
								
								retBusStationList.push({
									stationId: stationId,
									arsId : tempBusStationList[stationId].arsId,
									stationName: tempBusStationList[stationId].stationName,
									gpsX : tempBusStationList[stationId].gpsX,
									gpsY : tempBusStationList[stationId].gpsY,
									stationSequence : retBusRouteStation[i].stationSequence,
									startTime : retBusRouteStation[i].startTime,
									endTime : retBusRouteStation[i].endTime,
									direction : retBusRouteStation[i].direction
								});
							}
							
							res.send({result: error_code.SUCCESS, busRouteInfo: retBusRouteInfo, busStationList: retBusStationList });
						}
					});
				}
			});
		}
	});
}

/**
 * 주변 심야버스 조회
 * @param req
 * @param res
 */
function getPublicTransitNightBusList(req, res) {
	if (svr_utils.checkParams(['lat', 'lng', 'range'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	var range = parseFloat(req.params.range);
	
	var retNightBusRouteList = [];
	
	Async.waterfall([
		function (next) {			
			BusStation.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "dist.distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
				},
				{ 
					"$match": { 
						"arsId" : { "$ne" : '' }
					}
				},
				{
					"$group": {
						"_id": {
							"stationId" : "$stationId",
							"stationName" : "$stationName",
							"arsId" : "$arsId",
							"gpsX" : "$gpsX",
							"gpsY" : "$gpsY"
						},
						"minDistance": {
							"$min" : "$dist.distance"
						}
					}
				},
				{
					"$sort": {
						"minDistance": 1
					}
				}
			], function(err, busStationList) {
				next(err, busStationList);
			});
		}, function (busStationList, next) {
			var busStationIdList = [];
			var stationListLength = busStationList.length;
			
			if(stationListLength > 0) {
				for(var i = 0; i < stationListLength; i++) {
					busStationIdList.push(busStationList[i]._id.stationId);
				}

				BusRouteStation.aggregate([
					{ 
						"$match": { 
							"stationId" : { "$in" : busStationIdList }
						}
					},
					{
						"$group": {
							"_id": {
								"busRouteId" : "$busRouteId",
								"stationId" : "$stationId"
							},
							"startTime": {
								"$min" : "$startTime"
							},
							"endTime": {
								"$max" : "$endTime"
							}
						}
					}
				], function(err, busRoutesStationList) {
					if(err) {
						next(err);
					} else {
						var busRouteIdList = [];
						var listLength = busRoutesStationList.length;
						for(var i = 0; i < listLength; i++) {
							busRouteIdList.push(busRoutesStationList[i]._id.busRouteId);
						}
						
						BusRoute.find({"busRouteId" : {"$in": busRouteIdList}, "firstTime" : { "$gt" : "22:30" }, "lastTime" : { "$gt" : "$firstTime" }}).exec(function(err, busRouteList) {
							if(err) {
								next(err);
							} else {
								var retBusRoute = {};
								var listLength = busRouteList.length;
								for(var i = 0; i < listLength; i++) {
									retNightBusRouteList.push({
										busRouteId : busRouteList[i].busRouteId,
										busRouteName : busRouteList[i].busRouteName,
										startStationName : busRouteList[i].startStationName,
										endStationName : busRouteList[i].endStationName,
										firstTime : busRouteList[i].firstTime,
										lastTime : busRouteList[i].lastTime,
										localBusRouteId : busRouteList[i].localBusRouteId
									});
								}
																
								next(null, retNightBusRouteList);
							}
						});
					}
				});
			} else {
				next(null, retNightBusRouteList);
			}
		}, function(retNightBusRouteList, next) {
			res.send({result: error_code.SUCCESS, nightBusRouteList: retNightBusRouteList });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function getShuttleListAll(req, res) {
	Async.waterfall([
		function (next) {
			ShuttleInfo.find({},{"comment":0,"desc":0}).exec(function(err, shuttles) {
				if(err) {
					next(err);
				} else {
					res.send({ result: error_code.SUCCESS, buses: shuttles, length: shuttles.length });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function getShuttleRealTimePosition(req, res) {
	ShuttleRealTimePosition.find().exec(function(err, realTimePositionList) {
		if(err) {
			errFunction(err, req, res);
		} else {
			var retList = [];
			if(realTimePositionList) {
				var listSize = realTimePositionList.length;
				for(var i = 0; i < listSize; i++) {
					retList.push({
						categoryName : realTimePositionList[i].categoryName,
						locations : realTimePositionList[i].locations
					});
				}
			}
			
			res.send({ result: error_code.SUCCESS, realTimePosition: retList });
		}
	});
}

function getWokitoki (req, res) {
	if (svr_utils.checkParams(['busId', 'pageNo'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	var pageNo = parseInt(req.params.pageNo);


	Async.waterfall([
		function (next) {
			ShuttleInfo.findOne({ "_id": new ObjectId(req.params.busId) }).exec(next);
		}, function (_shuttleInfo, next) {
			if (!_shuttleInfo) {
				res.send({ result: error_code.NO_SUCH_BUS });
				return;
			}
			
			var wokitokilistTmp = [];
			var wokitokilist = [];
			
			for(var i = 0; i < _shuttleInfo.wokitoki.length; i++) {
				if(_shuttleInfo.wokitoki[i]) {
					wokitokilistTmp.push(_shuttleInfo.wokitoki[i]);
				}
			}

			wokitokilistTmp.reverse();

			for(var i = 10 * (pageNo - 1); i <= 10 * (pageNo - 1) + 9; i++) {
				if(wokitokilistTmp[i]) {
					wokitokilist.push(wokitokilistTmp[i]);
				}
			}

			res.send({ result: error_code.SUCCESS, wokitokis: wokitokilist });

			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function addWokitoki (req, res) {
	if(svr_utils.checkParams(["busId", "date", "phone", "fileSrc", "userId", "userNick", "lat", "lng"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function(next) {
			ShuttleInfo.findOne({ _id: new ObjectId(req.params.busId) }, {name: 1, category: 1, desc: 1, wokitoki: 1}).exec(next);
		},
		function(_shuttleInfo, next) {
			if(!_shuttleInfo) {
				res.send({ result: error_code.NO_SUCH_SHUTTLE });
				return;
			}
			
			var newObject = {
				date: req.params.date,
				messageSrc: req.params.fileSrc,
				driverId: req.params.userId,
				nickName: req.params.userNick,
				phone: req.params.phone,
				lat: req.params.lat,
				lng: req.params.lng
			};

			_shuttleInfo.wokitoki.push(newObject);
			_shuttleInfo.save(function (err) {
				if (err) {
					next(err);
				} else {
					res.send({ result: error_code.SUCCESS });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function getShuttleTransfer(req, res) {
	if(svr_utils.checkParams(["depart_from_lat", "depart_from_lng", "arrive_at_lat", "arrive_at_lng"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var depart_from_lat = parseFloat(req.params.depart_from_lat);
	var depart_from_lng = parseFloat(req.params.depart_from_lng);
	var arrive_at_lat = parseFloat(req.params.arrive_at_lat);
	var arrive_at_lng = parseFloat(req.params.arrive_at_lng);

	Async.waterfall([
		function(next) {
			// 하나의 노선으로 길찾기
			getOneLineTransfer(depart_from_lat, depart_from_lng, arrive_at_lat, arrive_at_lng, function(_err, _retOneLineTransfer) {
				if(_err) {
					if(_err == error_code.NOT_FOUND_SHUTTLE_TRANSFER) {
						res.send({ result: error_code.NOT_FOUND_SHUTTLE_TRANSFER });
						return;
					} else {
						next(_err);
					}
				} else {
					next(null, _retOneLineTransfer);
				}
			});
		},
		function(_retOneLineTransfer, next) {
			// 두개 노선으로 환승 길찾기
			getTwoLineTransfer(depart_from_lat, depart_from_lng, arrive_at_lat, arrive_at_lng, function(_err, _retTwoLineTransfer) {
				if(_err) {
					if(_err == error_code.NOT_FOUND_SHUTTLE_TRANSFER) {
						res.send({ result: error_code.NOT_FOUND_SHUTTLE_TRANSFER });
						return;
					} else {
						next(_err);
					}
				} else {
					next(null, _retOneLineTransfer, _retTwoLineTransfer);
				}
			});
		}, function(_retOneLineTransfer, _retTwoLineTransfer, next) {
			if(_retOneLineTransfer.length == 0 && _retTwoLineTransfer.length == 0) {
				res.send({ result: error_code.NOT_FOUND_SHUTTLE_TRANSFER });
			} else {
				var retTransfer = [];
				for(var i = 0; i < _retOneLineTransfer.length; i++) {
					retTransfer.push(_retOneLineTransfer[i]);
				}
				
				for(var i = 0; i < _retTwoLineTransfer.length; i++) {
					retTransfer.push(_retTwoLineTransfer[i]);
				}
				
				res.send({ result: error_code.SUCCESS, transferData: retTransfer });
			}
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

// 하나의 노선으로 길찾기
function getOneLineTransfer(_depart_from_lat, _depart_from_lng, _arrive_at_lat, _arrive_at_lng, _callback) {
	// 출발지 주변 셔틀 정거장 검색
	getShuttleInfoByCoordinates(_depart_from_lat, _depart_from_lng, function(_err, _depart_shuttle_lines) {
		if(_err) {
			_callback(_err);
		} else {
			if(!_depart_shuttle_lines) {
				_callback(error_code.NOT_FOUND_SHUTTLE_TRANSFER);
				return;
			}
			
			// 도착지 주변 셔틀 정거장 검색
			getShuttleInfoByCoordinates(_arrive_at_lat, _arrive_at_lng, function(_err, _arrive_shuttle_lines) {
				if(_err) {
					_callback(_err);
				} else {
					if(!_arrive_shuttle_lines) {
						_callback(error_code.NOT_FOUND_SHUTTLE_TRANSFER);
						return;
					}
					
					var matched_list = [];
					var matched_busIds = [];
					
					for(var i = 0; i < _depart_shuttle_lines.length; i++) {
						var depart_shuttle_item = _depart_shuttle_lines[i];
						for(var j = 0; j < _arrive_shuttle_lines.length; j++) {
							var arrive_shuttle_item = _arrive_shuttle_lines[j];
							if(depart_shuttle_item._id.busId == arrive_shuttle_item._id.busId 
									&& depart_shuttle_item._id.index < arrive_shuttle_item._id.index
									&& matched_busIds.indexOf(depart_shuttle_item._id.busId) == -1) {
								matched_list.push({
									depart : {
										lat : _depart_from_lat,
										lng : _depart_from_lng
									},
									pathList : [{
										seq : 0,
										geton : {
											busId : depart_shuttle_item._id.busId,
											index : depart_shuttle_item._id.index,
											text : depart_shuttle_item._id.text,
											transferName : depart_shuttle_item._id.transferName,
											lat : depart_shuttle_item._id.locations.coordinates[1],
											lng : depart_shuttle_item._id.locations.coordinates[0]
										},
										getoff : {
											busId : arrive_shuttle_item._id.busId,
											index : arrive_shuttle_item._id.index,
											text : arrive_shuttle_item._id.text,
											transferName : arrive_shuttle_item._id.transferName,
											lat : arrive_shuttle_item._id.locations.coordinates[1],
											lng : arrive_shuttle_item._id.locations.coordinates[0]
										}
									}],
									arrive : {
										lat : _arrive_at_lat,
										lng : _arrive_at_lng
									}
								});
								
								matched_busIds.push(depart_shuttle_item._id.busId);
								
								if(matched_list.length >= 2) {
									break;
								}
							}
						}
						
						if(matched_list.length >= 2) {
							break;
						}
					}

					if(matched_list.length > 0) {
						// 하나의 노선으로 길찾기 완료
						getTransferRouteInfo(matched_list, function(_err, _retTransfer) {
							if(_err) {
								_callback(_err);
							} else {
//								console.log("getOneLineTransferRoute retTransfer", JSON.stringify(_retTransfer));
								if(_retTransfer.length == 0) {
									_callback(error_code.NOT_FOUND_SHUTTLE_TRANSFER);
								} else {
									_callback(null, _retTransfer);
								}
							}
						});
					} else {
						_callback(null, []);
					}
				}
			});
		}
	});
}

function getTransferRouteInfo(_matched_list, _ret_callback) {
	var retTransfer = [];
	
	Async.each(_matched_list, function(_matched_item, _callback) {
		getTransferRoutePath(_matched_item, function(_err, _transfer_list) {
			if(_err) {
				_callback(_err);
			} else {
				if(_transfer_list && _transfer_list.length > 0) {
					var item = {};
					item.depart = {
						lat : _matched_item.depart.lat,
						lng : _matched_item.depart.lng
					};
					
					item.arrive = {
						lat : _matched_item.arrive.lat,
						lng : _matched_item.arrive.lng
					};
					
					item.transferName = _matched_item.transferName;
					
					item.pathList = _transfer_list;
					
					retTransfer.push(item);
				}
				
				_callback();
			}
		});
	}, function(err) {
		if(err) {
			_ret_callback(err);
		} else {
			_ret_callback(null, retTransfer);
		}
	});
}

function getTransferRoutePath(_item, _ret_callback) {
	var retRoutePath = [];
	
	Async.each(_item.pathList, function(_transfer_item, _callback) {
		ShuttleInfo.findById(new ObjectId(_transfer_item.geton.busId)).exec(function(err, _shuttleInfo) {
			if(err) {
				_callback(err);
			} else {
				if(!_shuttleInfo) {
					_callback("error");
				} else {
					ShuttlePoint.find({"busId" : _transfer_item.geton.busId, "index" : {"$gte" : _transfer_item.geton.index, "$lte" : _transfer_item.getoff.index}}).sort({"index" : 1}).exec(function(err, _path_list) {
						if(err) {
							_callback(err);
						} else {
							if(_path_list && _path_list.length > 0) {
								var transferShuttleInfo = {
									busId : _transfer_item.geton.busId,
									shuttleName : _shuttleInfo.name,
									routeType : _shuttleInfo.routeType,
									firstTime : _shuttleInfo.firstTime,
									lastTime : _shuttleInfo.lastTime,
									term : _shuttleInfo.term,
									totalTime : _shuttleInfo.totalTime,
									phone : _shuttleInfo.phone,
									saturdayFirstTime : _shuttleInfo.saturdayFirstTime,
								    saturdayLastTime : _shuttleInfo.saturdayLastTime,
								    sundayFirstTime : _shuttleInfo.sundayFirstTime,
								    sundayLastTime : _shuttleInfo.sundayLastTime,
								    path : []
								}
								
								_path_list.forEach(function(_path_item) {
									transferShuttleInfo.path.push({
										lat: parseFloat(_path_item.locations.coordinates[1]),
										lng: parseFloat(_path_item.locations.coordinates[0])
									});
								});
								
								var ret_transfer = {
									seq : _transfer_item.seq,
									geton : {
										text : _transfer_item.geton.text,
										transferName : _transfer_item.geton.transferName,
										lat: _transfer_item.geton.lat,
										lng: _transfer_item.geton.lng,
									},
									getoff : {
										text : _transfer_item.getoff.text,
										transferName : _transfer_item.getoff.transferName,
										lat: _transfer_item.getoff.lat,
										lng: _transfer_item.getoff.lng,
									},
									shuttleInfo : transferShuttleInfo
								};
								
								retRoutePath.push(ret_transfer);
							}
							
							_callback();
						}
					});
				}
			}
		});
	}, function(err) {
		if(err) {
			_ret_callback(err);
		} else {
			_ret_callback(null, retRoutePath);
		}
	});
}

// 두개의 노선으로 길찾기
function getTwoLineTransfer(_depart_from_lat, _depart_from_lng, _arrive_at_lat, _arrive_at_lng, _callback) {
	// 출발지 주변 셔틀 정거장 검색
	getTwoLineNearStations(_depart_from_lat, _depart_from_lng, "depart", function(_err, _depart_shuttle_lines) {
		if(_err) {
			_callback(_err);
		} else {
			if(!_depart_shuttle_lines || _depart_shuttle_lines.length <= 0) {
				_callback(error_code.NOT_FOUND_SHUTTLE_TRANSFER);
				return;
			}
			
			// 도착지 주변 셔틀 정거장 검색
			getTwoLineNearStations(_arrive_at_lat, _arrive_at_lng, "arrive", function(_err, _arrive_shuttle_lines) {
				if(_err) {
					_callback(_err);
				} else {
					if(!_arrive_shuttle_lines || _arrive_shuttle_lines.length <= 0) {
						_callback(error_code.NOT_FOUND_SHUTTLE_TRANSFER);
						return;
					}
					
					var matched_list = [];
					var matched_busIds = [];
					
					for(var i = 0; i < _depart_shuttle_lines.length; i++) {
						var depart_shuttle_item = _depart_shuttle_lines[i].station;
						var depart_shuttle_transfer_list = _depart_shuttle_lines[i].transfers;
						for(var k = 0; k < depart_shuttle_transfer_list.length; k++) {
							var depart_shuttle_transfer_item = depart_shuttle_transfer_list[k];
							for(var j = 0; j < _arrive_shuttle_lines.length; j++) {
								var arrive_shuttle_item = _arrive_shuttle_lines[j].station;
								var arrive_shuttle_transfer_list = _arrive_shuttle_lines[j].transfers;
								for(var l = 0; l < arrive_shuttle_transfer_list.length; l++) {
									var arrive_shuttle_transfer_item = arrive_shuttle_transfer_list[l];
									if(depart_shuttle_transfer_item.transferName === arrive_shuttle_transfer_item.transferName
											&& matched_busIds.indexOf(depart_shuttle_item.busId + arrive_shuttle_item.busId) == -1) {
										
										matched_busIds.push(depart_shuttle_item.busId + arrive_shuttle_item.busId);
										
										var matchedItem = {
											depart : {
												lat : _depart_from_lat,
												lng : _depart_from_lng
											},
											transferName : arrive_shuttle_transfer_item.text,
											pathList: [],
											arrive : {
												lat : _arrive_at_lat,
												lng : _arrive_at_lng
											}
										};
										
										matchedItem.pathList.push({
											seq : 0,
											geton : {
												busId : depart_shuttle_item.busId,
												index : depart_shuttle_item.index,
												text : depart_shuttle_item.text,
												transferName : depart_shuttle_item.transferName,
												lat : depart_shuttle_item.locations.coordinates[1],
												lng : depart_shuttle_item.locations.coordinates[0]
											},
											getoff : {
												busId : depart_shuttle_transfer_item.busId,
												index : depart_shuttle_transfer_item.index,
												text : depart_shuttle_transfer_item.text,
												transferName : depart_shuttle_transfer_item.transferName,
												lat : depart_shuttle_transfer_item.locations.coordinates[1],
												lng : depart_shuttle_transfer_item.locations.coordinates[0]
											}
										});
										
										if(depart_shuttle_transfer_item.text == arrive_shuttle_transfer_item.text) {
											matchedItem.pathList.push({
												seq : 1,
												geton : {
													busId : arrive_shuttle_transfer_item.busId,
													index : arrive_shuttle_transfer_item.index,
													text : arrive_shuttle_transfer_item.text,
													transferName : arrive_shuttle_transfer_item.transferName,
													lat : arrive_shuttle_transfer_item.locations.coordinates[1],
													lng : arrive_shuttle_transfer_item.locations.coordinates[0]
												},
												getoff : {
													busId : arrive_shuttle_item.busId,
													index : arrive_shuttle_item.index,
													text : arrive_shuttle_item.text,
													transferName : arrive_shuttle_item.transferName,
													lat : arrive_shuttle_item.locations.coordinates[1],
													lng : arrive_shuttle_item.locations.coordinates[0]
												}
											});
										} else {
											matchedItem.pathList.push({
												seq : 1,
												geton : {
													busId : arrive_shuttle_transfer_item.busId,
													index : arrive_shuttle_transfer_item.index,
													text : arrive_shuttle_transfer_item.text,
													transferName : arrive_shuttle_transfer_item.transferName,
													lat : arrive_shuttle_transfer_item.locations.coordinates[1],
													lng : arrive_shuttle_transfer_item.locations.coordinates[0]
												},
												getoff : {
													busId : arrive_shuttle_item.busId,
													index : arrive_shuttle_item.index,
													text : arrive_shuttle_item.text,
													transferName : arrive_shuttle_item.transferName,
													lat : arrive_shuttle_item.locations.coordinates[1],
													lng : arrive_shuttle_item.locations.coordinates[0]
												}
											});
										}
										
										matched_list.push(matchedItem);
									}
									
									if(matched_list.length >= 2) {
										break;
									}
								}
								
								if(matched_list.length >= 2) {
									break;
								}
							}
							
							if(matched_list.length >= 2) {
								break;
							}
						}
						
						if(matched_list.length >= 2) {
							break;
						}
					}

					if(matched_list.length > 0) {
						// 두개의 노선으로 길찾기 완료
						getTransferRouteInfo(matched_list, function(_err, _retTransfer) {
							if(_err) {
								_callback(_err);
							} else {
//								console.log("getTwoLineTransferRoute retTransfer", JSON.stringify(_retTransfer));
								if(_retTransfer.length == 0) {
									_callback(error_code.NOT_FOUND_SHUTTLE_TRANSFER);
								} else {
									_callback(null, _retTransfer);
								}
							}
						});
					} else {
						_callback(null, []);
					}
				}
			});
		}
	});
}

// 두개의 노선으로 길찾기
function getTwoLineNearStations(_lat, _lng, _type, _callback) {
	// 주변 셔틀 정거장 검색
	getShuttleInfoByCoordinates(_lat, _lng, function(_err, _shuttle_stations) {
		if(_err) {
			_callback(_err);
		} else {
			if(!_shuttle_stations || _shuttle_stations.length <= 0) {
				_callback(error_code.NOT_FOUND_SHUTTLE_TRANSFER);
			} else {
				var ret_shuttle_lines = [];
				Async.each(_shuttle_stations, function(_shuttle_stations_item, _sub_callback) {
					// 출발지 주변 환승 정류장 조회
					var cond = { 
						"busId" : _shuttle_stations_item._id.busId, 
						"index" : { "$lt" : _shuttle_stations_item._id.index }, 
						"transferName" : { "$ne" : "" }, 
						"ynEnabled" : "Y", 
						"isTransfer" : 1
					};
					
					if(_type == "depart") {
						cond.index = { "$gt" : _shuttle_stations_item._id.index };
					}
					
					var stationInfo = {
						station : _shuttle_stations_item._id
					};
					
					ShuttlePoint.find(cond).exec(function(_err, _stations) {
						if(_err) {
							_sub_callback(_err);
						} else {
							if(_stations && _stations.length > 0) {
								stationInfo.transfers = _stations;
								ret_shuttle_lines.push(stationInfo);
							}
							
							_sub_callback();
						}
					});
				}, function(err) {
					if(err) {
						_callback(err);
					} else {
						_callback(null, ret_shuttle_lines);
					}
				});
			}
		}
	});
}

function getShuttleInfoByCoordinates(_lat, _lng, _callback) {
	ShuttlePoint.aggregate([
		{
			"$geoNear": {
				"near": {
					"type": "Point",
					"coordinates": [_lng, _lat]
				},
				"distanceField": "dist.distance",
				"maxDistance": 1000,
				"spherical": true,
				"limit": 100000
			}
		},
		{
            "$match" : {
                "text": {"$ne" : "__point"},
                "ynEnabled" : "Y"
            }
        },
		{
			"$group": {
				"_id": {
					"busId" : "$busId",
                    "index" : "$index",
                    "locations" : "$locations",
                    "text" : "$text",
                    "transferName" : "$transferName"
				},
				"minDistance": {
					"$min" : "$dist.distance"
				}
			}
		},
		{
			"$sort": {
				"minDistance": 1
			}
        }
	], function(err, _shuttle_lines) {
		_callback(err, _shuttle_lines);
	});
}

exports.initialize = initialize;
exports.busList = busList;
exports.busList2 = busList2;
exports.busOne = busOne;
exports.createBus = createBus;
exports.recordBus = recordBus;
exports.editText = editText;
exports.addComment = addComment;
exports.getComment = getComment;
exports.getShuttlePoints = getShuttlePoints;
exports.getShuttleStation = getShuttleStation;
exports.startChatting = startChatting;
exports.endChatting = endChatting;
exports.requestLocation = requestLocation;
exports.responseLocation = responseLocation;
exports.getFrequentlyUsedList = getFrequentlyUsedList;
exports.getShuttleListByStationId = getShuttleListByStationId;
exports.getUpdatedShuttleStation = getUpdatedShuttleStation;
exports.getNearPublicTransitStationList = getNearPublicTransitStationList;
exports.getPublicTransitRouteListByStationId = getPublicTransitRouteListByStationId;
exports.getPublicTransitRouteDetail = getPublicTransitRouteDetail;
exports.getPublicTransitStationListByRouteId = getPublicTransitStationListByRouteId;
exports.getPublicTransitNightBusList = getPublicTransitNightBusList;
exports.getShuttleCategoryList = getShuttleCategoryList;
exports.getShuttleListAll = getShuttleListAll;
exports.getShuttleRealTimePosition = getShuttleRealTimePosition;
exports.getWokitoki = getWokitoki;
exports.addWokitoki = addWokitoki;
exports.getShuttleTransfer = getShuttleTransfer;
