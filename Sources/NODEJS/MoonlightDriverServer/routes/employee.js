var Async		= require('async');
var Company		= DB.model('Company');
var Alliance	= DB.model('Alliance');
var Admin		= DB.model('Admin');
var sender;
var ObjectId	= DB.Types.ObjectId;

function initialize (server) {
    server.post('/company/create', createCompany);
    server.post('/company/delete', deleteCompany);
    server.post('/alliance/:companyId/create', createAlliance);
    server.post('/alliance/:companyId/delete', deleteAlliance);
    server.patch('/admin/create', createAdmin);
    server.post('/admin/delete', deleteAdmin);
}

function createCompany (req, res) {
	if (svr_utils.checkParams(["name","representative","registration","phone","fax"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
    }
	Async.waterfall([
        function (next) {
			var company = new Company({
				name: req.params.name,
				representative : req.params.representative,
				registration: req.params.registration,
				phone: req.params.phone,
				fax: req.params.fax
			});
		}, function (company, next) {
	            res.send({ result: error_code.SUCCESS });
	            next();
	        }
	    ], function (err) {
	        if (err) errFunction(err, req, res);
    });
}
function deleteCompany (req, res) {
	if (svr_utils.checkParams(["companyId","password"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
    }

}
function createAlliance (req, res) {
	if (svr_utils.checkParams(["companyId","name","phone"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
    }
	Async.waterfall([
        function (next) {
			var alliance = new Alliance({
				companyId : req.params.companyId,
				name : req.params.name,
				phone: req.params.phone
			});
		}, function (alliance, next) {
	            res.send({ result: error_code.SUCCESS });
	            next();
	        }
	    ], function (err) {
	        if (err) errFunction(err, req, res);
    });
}
function deleteAlliance (req, res) {
	if (svr_utils.checkParams(["allianceId","password"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
    }

}
function createAdmin (req, res) {
	if (svr_utils.checkParams(["companyId","allianceId","name","phone","level"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
    }
	Async.waterfall([
        function (next) {
			var admin = new Admin({
				companyId: req.params.companyId,
				allianceId : req.params.allianceId,
				name: req.params.phone,
				level: parseInt(req.params.level)
			});
		}, function (admin, next) {
	            res.send({ result: error_code.SUCCESS });
	            next();
	        }
	    ], function (err) {
	        if (err) errFunction(err, req, res);
    });
}
function deleteAdmin (req, res) {
	if (svr_utils.checkParams(["adminId","password"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
    }

}

exports.initialize = initialize;
exports.createCompany = createCompany;
exports.deleteCompany = deleteCompany;
exports.createAlliance = createAlliance;
exports.deleteAlliance = deleteAlliance;
exports.createAdmin = createAdmin;
exports.deleteAdmin = deleteAdmin;
