var Async			= require('async');
var Driver			= DB.model('Driver');
var Question		= DB.model('Question');
var QuestionAnswer	= DB.model('QuestionAnswer');
var _LogQuestion	= DB.model('_LogQuestion');
var Counter			= DB.model('Counter');
var Account			= DB.model('Account');
var ObjectId		= DB.Types.ObjectId;
var gcm				= require("node-gcm");
var sender;

function initialize (server) {
	sender = new gcm.Sender(config.GCM.SENDER_ID);
	server.post('/question/send', sendUserQuestion);
	server.get('/question/list', getQuestionList);
	server.get('/question/info/:questionId', getQuestionInfo);
	server.get('/question/info/answer/:questionId', getQuestionInfoWithAnswer);
	server.get('/question/push', sendQuestionAnswerPush);
}

function sendUserQuestion(req, res) {
	if (svr_utils.checkParams(['driverId', 'phone', 'title', 'contents', 'lat', 'lng'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
	}

	var lat = parseFloat(req.params.lat);
    var lng = parseFloat(req.params.lng);
	
	Async.waterfall([
  		function (next) {
			Driver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function (driver, next) {
			if (!driver) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}

			Counter.getNextSequence('question_seq', function(err, _counter) {
				if(err) {
					next(err);
				} else {
					var question = new Question({
						seq: _counter.seq,
						driverId: req.params.driverId,
						phone: req.params.phone,
						title: req.params.title,
						contents: req.params.contents,
						answerCount: 0,
						locations: {
							type: "Point",
							coordinates: [lng, lat]
						},
						createdDate: new Date().getTime(),
						updatedDate: new Date().getTime()
					});

					question.save(function (err) {
						if(err) {
							next(err);
						} else {
							res.send({ result: error_code.SUCCESS });
							next(null, question);
						}
					});
				}
			});
  		}, function(question, next) {
  			var log_question = new _LogQuestion({
				seq: question.seq,
  				driverId: question.driverId,
  				phone: question.phone,
  				title: question.title,
  				contents: question.contents,
				locations: {
					type: "Point",
					coordinates: question.locations.coordinates
				},
  				createdDate: question.createdDate,
  				logType: "regist",
  				logDate: new Date()
  			});
  			
  			log_question.save();
  			
  			sendQuestionPush(question._id, req, res);

  			next();
  		}
  	], function (err) {
  		if (err) errFunction(err, req, res);
  	});
}

function sendQuestionPush(question_id, req, res) {
	Async.waterfall([
		function (next) {
        	Account.find({ynPush: "Y"}).exec(next);
        }, function (accountList, next) {
            if (!accountList) {
                return;
            }
            
            var listSize = accountList.length;
            if(listSize > 0) {
            	var phoneList = [];
                for(var i = 0; i < listSize; i++) {
                	var phoneNumber = accountList[i].phone.replace(/-/gi, "");
                	phoneList.push(phoneNumber);
                	
                	if(!phoneNumber.startsWith("+82")) {
                		phoneList.push("+82" + phoneNumber.substring(1, phoneNumber.length));
                	}
                }
                
                Driver.find({phone: {'$in' : phoneList}}).exec(next);
            }
        }, function(driverList, next) {
        	if(!driverList) {
        		return;
        	}
        	
        	var listSize = driverList.length;
        	if(listSize > 0) {
        		var gcmIds = [];
        		for(var i = 0; i < listSize; i++) {
        			gcmIds.push(driverList[i].gcmId);
        		}
        		
        		var message = new gcm.Message({
    				delayWhileIdle: true,
    				timeToLive: 3,
    				data: {
    					questionId : question_id,
    					type: 'question_register',
    					message: "새로운 문의사항이 등록되었습니다."
    				}
    			});
        		
        		sender.send(message, gcmIds, function (err, result) {
    				if(err) {
    					next(err);
    				} else {
    					next();
    				}
    			});
        	}
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function getQuestionList(req, res) {
	Async.waterfall([
 		function (next) {
 			var pageNum = req.params.pageNum ? req.params.pageNum : 0;
 			var limit = req.params.limit ? req.params.limit : 20;
 			
 			pageNum = pageNum * limit;
 			
 			Question.find().sort({ seq: -1 }).skip(pageNum).limit(limit).exec(next);
 		}, function (questions, next) {
 			var questionList = [];
 			for (var i = 0; i < questions.length; i++) {
 				var questionListData = questions[i].getListData();
 				var questionInfo = {
 					questionId : questionListData.questionId,
 					driverId : questionListData.driverId,
 					title : questionListData.phone.substr(questionListData.phone.length - 4, questionListData.phone.length) + "님의 문의사항 입니다.",
 					answerCount : questionListData.answerCount,
 					createdDate : questionListData.createdDate
 				};
 				
 				questionList.push(questionInfo);
 			}
 			
 			res.send({ result: error_code.SUCCESS, list: questionList });
 			next();
 		}
 	], function (err) {
 		if (err) errFunction(err, req, res);
 	});
}

function getQuestionInfo(req, res) {
	if (svr_utils.checkParams(['questionId'], req.params)) {
        res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
    }

    Async.waterfall([
        function (next) {
			Question.findById(new ObjectId(req.params.questionId)).exec(next);
        }, function (questionInfo, next) {
            if (!questionInfo) {
                res.send({ result: error_code.NO_SUCH_QUESTION });
                return;
            }

			res.send({ result: error_code.SUCCESS, question: questionInfo.getData() });
			next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

function getQuestionInfoWithAnswer(req, res) {
	if (svr_utils.checkParams(['questionId'], req.params)) {
        res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
    }

    Async.waterfall([
        function (next) {
			Question.findById(new ObjectId(req.params.questionId)).exec(next);
        }, function (questionInfo, next) {
            if (!questionInfo) {
                res.send({ result: error_code.NO_SUCH_QUESTION });
                return;
            }
            
            QuestionAnswer.find({questionId: req.params.questionId}).exec(function(err, questionAnswer) {
            	if(err) {
            		next(err);
            	} else {
            		next(null, questionInfo, questionAnswer);
            	}
            });
        }, function(questionInfo, questionAnswer, next) {
        	var questionAnswerList = [];
        	var listLength = questionAnswer.length;
        	for(var i = 0; i < listLength; i++) {
        		questionAnswerList.push(questionAnswer[i].getData());
        	}
        	
        	res.send({ result: error_code.SUCCESS, question: questionInfo.getData(), answer: questionAnswerList });
			next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

function sendQuestionAnswerPush(req, res) {
	if (svr_utils.checkParams(['questionId', 'message'], req.params)) {
        res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
    }

    Async.waterfall([
        function (next) {
        	Question.findById(new ObjectId(req.params.questionId)).exec(next);
        }, function (questionInfo, next) {
            if (!questionInfo) {
                res.send({ result: error_code.NO_SUCH_QUESTION });
                return;
            }
            
            Driver.findById(new ObjectId(questionInfo.driverId)).exec(function(err, driverInfo) {
            	if(err) {
            		next(err);
            	} else {
            		if(!driverInfo) {
            			res.send({ result: error_code.NO_SUCH_DRIVER });
        				return;
            		} else {
            			next(null, questionInfo, driverInfo);
            		}
            	}
            });
        }, function(questionInfo, driverInfo, next) {
        	questionInfo = questionInfo.getData();
        	var message = new gcm.Message({
				delayWhileIdle: true,
				timeToLive: 3,
				data: {
					type: 'question_answer',
					questionId: questionInfo.questionId,
					message: req.params.message
				}
			});
			
			sender.send(message, [driverInfo.gcmId], function (err, result) {
				if(err) {
					next(err);
				} else {
					res.send({ result: error_code.SUCCESS });
					next();
				}
			});
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

exports.initialize = initialize;
exports.sendUserQuestion = sendUserQuestion;
exports.getQuestionList = getQuestionList;
exports.getQuestionInfo = getQuestionInfo;
exports.getQuestionInfoWithAnswer = getQuestionInfoWithAnswer;
exports.sendQuestionAnswerPush = sendQuestionAnswerPush;