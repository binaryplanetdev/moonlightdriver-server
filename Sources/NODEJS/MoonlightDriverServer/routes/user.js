var Async			= require('async');
var User      		= DB.model('User');
var Crackdown		= DB.model('Crackdown');
var CrackdownPushNotification = DB.model('CrackdownPushNotification');
var UserVersion 	= DB.model('UserVersion');
var UserAdditional	= DB.model('UserAdditional');
var _LogUser		= DB.model('_LogUser');
var ObjectId		= DB.Types.ObjectId;
var PushMessage		= require(rootPath + '/lib/PushMessage');

function initialize (server) {
	server.get('/user/version', checkUserVersion);
	server.post('/user/register/auto', autoRegisterUser);
	server.post('/user/update/location', updateUserLocation);
	server.post('/user/update/push_yn', updatePush);
}

function checkUserVersion(req, res) {
	UserVersion.findOne({version: req.params.version}).exec(function (err, version) {
		if(err) {
			errFunction(err, req, res);
		} else {
			if(version) {
				var versionInfo = version.getData();
				
				if(versionInfo.isValid) {
					res.send({ result: error_code.SUCCESS });
				} else {
					res.send({ result: error_code.NEED_UPDATE_VERSION });
				}
			} else {
				res.send({ result: error_code.NOT_ALLOW_VERSION });
			}
		}
	});
}

function autoRegisterUser(req, res) {
	if(svr_utils.checkParams(['phone', 'macAddr', 'gcmId', "device_uuid", "lat", "lng", "email", "name"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var isRegister = true;
	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	
	Async.waterfall([
	    function(next) {
	    	User.findOne({ device_uuid: req.params.device_uuid }).exec(next);
	    }, function(_userInfo, next) {
	    	var user = null;
	    	if(!_userInfo) {
	    		isRegister = true;
	    		_userInfo = new User({
					gcmId: req.params.gcmId,
					email: req.params.email,
					phone: req.params.phone,
					device_uuid: req.params.device_uuid,
					name: req.params.name,
					macAddr: req.params.macAddr,
					profileImage: "",
					locations: {
						type: "Point",
						coordinates: [ lng, lat ]
					},
					createdDate : new Date(),
					lastLoginDate : new Date()
				});
	    	} else {
	    		isRegister = false;
	    		
				if(req.params.gcmId) {
	    			_userInfo.gcmId = req.params.gcmId;
	    			_userInfo.macAddr = req.params.macAddr;
	    			_userInfo.phone = req.params.phone;
					_userInfo.locations = {
						type: "Point",
						coordinates: [ lng, lat ]
					};
					_userInfo.lastLoginDate = new Date();
	    		}
	    	}
	    	
	    	_userInfo.save(function(err) {
	    		next(err, _userInfo);
			});
		}, function(_user, next) {
			var userInfo = _user.getData();
			
			UserAdditional.findOne({"userId" : userInfo.userId}).exec(function(_err, _userAdditional) {
				if(_err) {
					next(_err);
				} else {
					if(!_userAdditional) {
						var _userAdditional = new UserAdditional({
							userId : userInfo.userId,
							pushYN : "Y",
							createdDate : new Date(),
							lastUpdateDate : new Date()
						});
						
						_userAdditional.save(function(err) {
							next(err, userInfo, _userAdditional.getData());
						});
					} else {
						next(null, userInfo, _userAdditional.getData());
					}
				}
			});
		}, function(_userInfo, _userAdditional, next) {
			var log_user = new _LogUser({
				userId: _userInfo.userId,
				gcmId: _userInfo.gcmId,
				device_uuid: _userInfo.device_uuid,
				email:  _userInfo.email,
				phone: _userInfo.phone,
				name: _userInfo.name,
				macAddr: _userInfo.macAddr,
				profileImage: _userInfo.profileImage,
				locations: _userInfo.locations,
				logType: "",
				logDate: new Date()
			});
			
			if(isRegister) {
				log_user.logType = "reg_auto";
			} else {
				log_user.logType = "login";
			}
			
			log_user.save();
			
			res.send({ result: error_code.SUCCESS, userDetail: _userInfo, userAdditional: _userAdditional });
			next();
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function updateUserLocation(req, res) {
	if(svr_utils.checkParams(['userId', 'lat', 'lng'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var userInfo = null;
	var userAdditional = null;
	var range = 1000;
	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	
	Async.waterfall([
	    function(next) {
	    	User.findById(new ObjectId(req.params.userId)).exec(next);
	    }, function(_userInfo, next) {
	    	if(!_userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
	    	
	    	_userInfo.locations = {
	    		type: "Point",
				coordinates: [ lng, lat ]
	    	};
	    	
	    	_userInfo.save(function(err) {
	    		userInfo = _userInfo.getData();
	    		next(err);
			});
	    }, function(next) {
	    	UserAdditional.findOne({"userId" : req.params.userId}).exec(function(_err, _userAdditional) {
	    		if(_err) {
	    			next(_err);
	    		} else {
	    			if(!_userAdditional) {
	    				_userAdditional = new UserAdditional({
							userId : req.params.userId,
							pushYN : "Y",
							createdDate : new Date(),
							lastUpdateDate : new Date()
						});
	    				
	    				_userAdditional.save();
	    	    	}
	    			
	    			userAdditional = _userAdditional.getData();
	    			next();
	    		}
	    	});
	    }, function(next) {
	    	// 300m 반경 내 단속 조회
	    	getNearCrackdownList(req.params.userId, lat, lng, 0, 300, function(_err1, _crackdowns1) {
	    		if(_err1) {
	    			next(_err1);
	    		} else {
	    			if(_crackdowns1 && _crackdowns1.length > 0) {
	    				next(null, 300, _crackdowns1);	    				
	    			} else {
	    				// 500m 반경 내 단속 조회
	    		    	getNearCrackdownList(req.params.userId, lat, lng, 300, 500, function(_err2, _crackdowns2) {
	    		    		if(_err2) {
	    		    			next(_err2);
	    		    		} else {
	    		    			if(_crackdowns2 && _crackdowns2.length > 0) {
	    		    				next(null, 500, _crackdowns2);	    				
	    		    			} else {
	    		    				// 1000m 반경 내 단속 조회
	    		    		    	getNearCrackdownList(req.params.userId, lat, lng, 500, 1000, function(_err3, _crackdowns3) {
	    		    		    		next(_err3, 1000, _crackdowns3);
	    		    		    	});
	    		    			}
	    		    		}
	    		    	});
	    			}
	    		}
	    	});
		}, function(_distanceType, _crackdowns, next) {
			if(_crackdowns && _crackdowns.length > 0) {
				if(userAdditional && userAdditional.pushYN == "Y") {
					var pushMsg = new PushMessage();
					
					var crackdownMessage = "현재 ";
					if(_distanceType == 300 || _distanceType == 500) {
						crackdownMessage += _distanceType + "m";
					} else {
						crackdownMessage += "1km";
					}
					
					crackdownMessage += " 이내에 " + _crackdowns.length + "곳에서 음주단속 중입니다";
					
					var message = {
						type: 'crackdown_warning',
						distance: _distanceType,
						crackdownCount : _crackdowns.length,
						message : crackdownMessage
					};
					
					pushMsg.sendMessage(message, [userInfo.gcmId], function(_err) {
						if(_err) {
							logger.error(_err);
							logger.error(_err.stack);
						}
					});
					
					for(var i = 0; i < _crackdowns.length; i++) {
						console.log("_crackdowns[" + i + "]._id", _crackdowns[i]._id);
						
						(function(_id, _distanceType) {
							var newData = new CrackdownPushNotification({
								crackdownId : _id,
								userId : req.params.userId,
								distance: _distanceType,
								createTime : new Date().getTime()
							});
							
							newData.save(function(_err) {
								if(_err) {
									logger.error(_err);
								}
							});
							
							console.log("inserted_id", _id);
						})(_crackdowns[i]._id, _distanceType);
					}
				}
			}
			
			res.send({ result: error_code.SUCCESS });
			next();
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function getNearCrackdownList(_userId, _lat, _lng, _min_distance, _max_distance, _callback) {
	var limitTime = new Date().getTime() - (60 * 60 * 1000);
	CrackdownPushNotification.find({userId : _userId, createTime : { '$gte' : limitTime }, distance: _max_distance}, {crackdownId : 1}).exec(function(_err, _crackdownPushNoti) {
		if(_err) {
			_callback(_err);
		} else {
			var crackdownPushNotiExcept = [];
			if(_crackdownPushNoti) {
				for(var i = 0; i < _crackdownPushNoti.length; i++) {
					crackdownPushNotiExcept.push(new ObjectId(_crackdownPushNoti[i].crackdownId));
				}
			}
			
			Crackdown.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [_lng, _lat]
						},
						"query" : { "_id" : { "$nin" : crackdownPushNotiExcept }, "userId" : { "$ne" : _userId }, "createTime" : { "$gte" : limitTime } },
						"distanceField": "distance",
						"maxDistance": _max_distance,
						"spherical": true,
						"limit": 10000
					}
				}, {
					"$project" : { "_id" : 1, "distance": 1 }
				}, {
					"$match" : { "distance" : {'$gte' : _min_distance} }
				}, {
					"$sort" : {
						"distance" : 1
					}
				}
			], function(err, crackdowns) {
				_callback(err, crackdowns);
			});
		}
	});
}

function updatePush(req, res) {
	if(svr_utils.checkParams(['userId', 'pushYN'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
	    function(next) {
	    	User.findById(new ObjectId(req.params.userId)).exec(next);
	    }, function(_userInfo, next) {
	    	if(!_userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
	    	
	    	UserAdditional.findOne({"userId" : req.params.userId}).exec(function(_err, _userAdditional) {
	    		next(_err, _userInfo, _userAdditional);
	    	});
	    }, function(_userInfo, _userAdditional, next) {
	    	if(!_userAdditional) {
	    		_userAdditional = new UserAdditional({
					userId : req.params.userId,
					pushYN : req.params.pushYN,
					createdDate : new Date(),
					lastUpdateDate : new Date()
				});
	    	} else {
	    		_userAdditional.pushYN = req.params.pushYN;
	    		_userAdditional.lastUpdateDate = new Date();
	    	}
	    	
	    	_userAdditional.save(function(err) {
	    		next(err);
	    	});
		}, function(next) {
			res.send({ result: error_code.SUCCESS });
			next();
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

exports.initialize = initialize;
exports.autoRegisterUser = autoRegisterUser;
exports.checkUserVersion = checkUserVersion;
exports.updateUserLocation = updateUserLocation;
exports.updatePush = updatePush;
