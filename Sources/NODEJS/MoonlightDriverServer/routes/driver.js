var Async		= require('async');
var Driver      = DB.model('Driver');
var Version     = DB.model('Version');
var Location    = DB.model('Location');
var Chat		= DB.model('Chat');
var _LogDriver	= DB.model('_LogDriver');
var _LogChat	= DB.model('_LogChat');
var InsuranceCompany = DB.model('InsuranceCompany');
var _LogInsuranceCompany = DB.model('_LogInsuranceCompany');
var ObjectId	= DB.Types.ObjectId;
var PushMessage	= require(rootPath + '/lib/PushMessage');

function initialize (server) {
	server.post('/driver/register/auto', tempRegisterDriver);
	server.post('/driver/register', registerDriver);
	server.post('/driver/:driverId/edit', EditDriver);
	server.post('/driver/version', checkVersion);
	server.post('/driver/update/confirm', updateDriverConfirmData);
	server.post('/driver/check', checkDriverConfirmData);
	server.post('/driver/logout', logOut);
	server.post('/insurance/company/insert', insertInsuranceCompany);
}

function checkVersion(req, res) {
	Version.findOne({version: req.params.version}).exec(function (err, version) {
		if(err) {
			errFunction(err, req, res);
		} else {
			if(version) {
				var versionInfo = version.getData();
				
				if(versionInfo.isValid) {
					res.send({ result: error_code.SUCCESS });
				} else {
					res.send({ result: error_code.NEED_UPDATE_VERSION });
				}
			} else {
				res.send({ result: error_code.NOT_ALLOW_VERSION });
			}
		}
	});
}

function checkDriverConfirmData(req, res) {
	if(svr_utils.checkParams(['driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
	    function(next) {
	    	Driver.findById(new ObjectId(req.params.driverId)).exec(next);
	    }, function(_driverInfo, next) {
	    	if(!_driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
	    	} else {
	    		var driver = _driverInfo.getData();
	    		
	    		res.send({ result: error_code.SUCCESS });
    			next();
    			
    			// 오픈 일주일 후에 주석 풀어서 기사 등록한 사용자만 셔틀 노선 확인 가능하도록 활성화
//	    		if((driver.name == null || driver.name.length <= 0) || (driver.driverLicense == null || driver.driverLicense.length <= 0) ||
//	    				(driver.insuranceCompanyId == null || driver.insuranceCompanyId.length <= 0) || (driver.insuranceNumber == null || driver.insuranceNumber.length <= 0)) {
//	    			InsuranceCompany.find().exec(function(err, _companyList) {
//	    	    		if(err) {
//	    	    			next(err);
//	    	    		} else {
//	    	    			var companyList = [];
//	    	    			var listSize = _companyList.length;
//	    	    			for(var i = 0; i < listSize; i++) {
//	    	    				companyList.push(_companyList[i].getData());
//	    	    			}
//	    	    			
//	    	    			res.send({ result : error_code.NOT_CONFIRM_DRIVER, company_list : companyList });
//	    	    			return;
//	    	    		}
//	    	    	});
//	    		} else {
//	    			res.send({ result: error_code.SUCCESS });
//	    			next();
//	    		}
	    	}
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function tempRegisterDriver(req, res) {
	if(svr_utils.checkParams(['phone', 'macAddr', 'gcmId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var isRegister = true;
	var version = req.params.version ? req.params.version : "";
	
	Async.waterfall([
	    function(next) {
	    	Driver.findOne({ phone: req.params.phone }).exec(next);
	    }, function(_driverInfo, next) {
	    	var driver = null;
	    	if(!_driverInfo) {
	    		isRegister = true;
	    		driver = new Driver({
					gcmId: req.params.gcmId,
					phone: req.params.phone,
					macAddr: req.params.macAddr,
					version: version,
					name: "",
					driverLicense: "",
					insuranceCompanyId: "",
					insuranceNumber: "",
					star: 0,
					ynLogin: "Y",
					starnum: 0,
					createdDate : new Date(),
					updatedDate : new Date(),
					lastLoginDate : new Date()
				});
	    	} else {
	    		isRegister = false;
	    		driver = _driverInfo;
	    		driver.ynLogin = "Y";
	    		driver.version = req.params.version;
	    		driver.macAddr = req.params.macAddr;
	    		driver.lastLoginDate = new Date();
	    		
	    		if(req.params.gcmId) {
	    			driver.gcmId = req.params.gcmId;
	    		}
	    	}
	    	
	    	driver.save(function(err) {
				if(err) {
					next(err);
				} else {
					res.send({ result: error_code.SUCCESS, driver: driver.getData() });
					next(null, driver);
				}
			});
		}, function(_driver, next) {
			var driverInfo = _driver.getData();
			
			var log_driver = new _LogDriver({
				driverId: driverInfo.driverId,
				gcmId: driverInfo.gcmId,
				phone: driverInfo.phone,
				macAddr: driverInfo.macAddr,
				ynLogin: driverInfo.ynLogin,
				logType: "reg_auto",
				logDate: new Date()
			});
			
			if(!isRegister) {
				log_driver.logType = "login";
			}
			
			log_driver.save();
			next();
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function logOut(req, res) {
	if(svr_utils.checkParams(['driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
		
	Async.waterfall([
	    function(next) {
	    	Driver.findById(new ObjectId(req.params.driverId)).exec(next);
	    }, function(_driverInfo, next) {
	    	if (!_driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
	    	
	    	if(_driverInfo.ynLogin == "N") {
	    		res.send({ result: error_code.SUCCESS });
	    		return;
	    	}
	    	
	    	_driverInfo.ynLogin = "N";
	    	
	    	_driverInfo.save(function(err) {
	    		var log_driver = new _LogDriver({
					driverId: _driverInfo.driverId,
					gcmId: _driverInfo.gcmId,
					phone: _driverInfo.phone,
					macAddr: _driverInfo.macAddr,
					ynLogin: _driverInfo.ynLogin,
					logType: "log_out",
					logDate: new Date()
				});
				
				log_driver.save();
				
				next(err, _driverInfo);
			});
	    }, function(_driverInfo, next) {
	    	Chat.find({drivers : { '$elemMatch' : { driverId : req.params.driverId}}}).exec(function (err, chat_list) {
	    		next(err, _driverInfo, chat_list);
	    	});
		}, function(_driverInfo, _chat_list, next) {
			var driverInfo = _driverInfo.getData();
			if(_chat_list) {
				var listSize = _chat_list.length;
				if(listSize > 0) {
					for(var i = 0; i < listSize; i++) {
						var chatInfo = _chat_list[i];
						var driverIndex = -1;
						var driverCount = chatInfo.drivers.length;
						for(var i = 0; i < driverCount; i++) {
							if(chatInfo.drivers[i].driverId == req.params.driverId) {
								driverIndex = i;
								break;
							}
						}

						if(driverIndex >= 0) {
							var driverInfo = chatInfo.drivers[driverIndex];
							var sendMsg = '[' + driverInfo.nickName + ']님이 퇴장하셨습니다.';

							chatInfo.drivers = chatInfo.drivers.slice(0, driverIndex).concat(chatInfo.drivers.slice(driverIndex + 1, chatInfo.drivers.length));
							
							if(chatInfo.drivers.length > 0) {
								var drivers = [];
								var gcm_list = [];
								for(var i = 0; i < chatInfo.drivers.length; i++) {
									drivers.push({
										"driverId" : chatInfo.drivers[i].driverId,
										"nickName" : chatInfo.drivers[i].nickName,
										"joinDate" : chatInfo.drivers[i].joinDate
									});
									
									gcm_list.push(chatInfo.drivers[i].gcmId);
								}
								
								var pushMsg = new PushMessage();
								var message = {
									type: 'msg_shuttle_chat_notice',
									busId: chatInfo.busId,
									drivers: drivers,
									message: sendMsg,
									sendDate: new Date().getTime()
								};
								
								pushMsg.sendMessage(message, gcm_list, function(_err) {
									if(_err) {
										errFunction(err, req, res);
									}
								});
							}
							
							chatInfo.save(function(err) {
								if(err) {
									next(err);
								} else {
									var log_chat = new _LogChat({
										busId: chatInfo.busId,
										drivers: chatInfo.drivers,
										sendDriverId: driverInfo.driverId,
										recvDriverId: "",
										message: sendMsg,
										lat: null,
										lng: null,
										messageType: "notice", // msg(일반메시지), notice(입장/퇴장), req_location(위치 확인 요청), res_location(위치 확인 응답)
										logDate: new Date()
									});

									log_chat.save();
									
									next();
								}
							});
						} else {
							next();
						}
					}
				} else {
					next();
				}
			} else {
				next();
			}
		}, function(next) {
			res.send({ result: error_code.SUCCESS });
			next();
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function updateDriverConfirmData(req, res) {
	if(svr_utils.checkParams(['driverId', 'name', 'driverLicense', 'insuranceCompanyId', 'insuranceNumber'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
	    function(next) {
	    	Driver.findById(new ObjectId(req.params.driverId)).exec(next);
	    },
	    function(_driverInfo, next) {
	    	if (!_driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
	    	
	    	Driver.find({insuranceNumber: req.params.insuranceNumber}).exec(function (err, list) {
	    		if(err) {
	    			next(err);
	    		} else {
	    			next(null, _driverInfo, list);
	    		}
	    	});
	    }, function(_driverInfo, _list, next) {
	    	if(_list.length > 0) {
	    		res.send({ result: error_code.DUPLICATE_INSURANCE_NUMBER });
				return;
	    	}
	    	
	    	InsuranceCompany.findById(new ObjectId(req.params.insuranceCompanyId)).exec(function(err, _company) {
	    		if(err) {
	    			next(err);
	    		} else {
	    			next(null, _driverInfo, _company);
	    		}
	    	});
	    }, function(_driverInfo, _company, next) {
	    	if(!_company) {
	    		res.send({ result: error_code.NOT_ALLOW_INSURANCE_COMPANY });
				return;
	    	}
	    	
	    	var regExp = new RegExp('^\\d{2}[-]\\d{2}[-]\\d{6}[-]\\d{2}$', 'g');
	    	if(!regExp.test(req.params.driverLicense)) {	// 면허번호 체크
	    		res.send({ result: error_code.NOT_ALLOW_DRIVER_LICENSE });
				return;
	    	}
	    	
	    	regExp = new RegExp(_company.regExp, 'g');
	    	if(!regExp.test(req.params.insuranceNumber)) {	// 보험번호 체크
	    		res.send({ result: error_code.NOT_ALLOW_INSURANCE_NUMBER });
				return;
	    	} else {
	    		var nowYear = new Date().getFullYear();
	    		var splitStr = req.params.insuranceNumber.split("-");

	    		if(splitStr[0].length == 4) {
	    			var compYear = parseInt(splitStr[0]);
	    			if(nowYear < compYear) {
	    				res.send({ result: error_code.NOT_ALLOW_INSURANCE_NUMBER });
	    				return;
	    			}
	    		}
	    	}
	    	
	    	_driverInfo.name = req.params.name;
	    	_driverInfo.driverLicense = req.params.driverLicense;
	    	_driverInfo.insuranceCompanyId = req.params.insuranceCompanyId;
	    	_driverInfo.insuranceNumber = req.params.insuranceNumber;
	    	_driverInfo.updatedDate = new Date();
	    	
	    	_driverInfo.save(function(err) {
				if(err) {
					next(err);
				} else {
					res.send({ result: error_code.SUCCESS, driver: _driverInfo.getData() });
					next(null, _driverInfo);
				}
			});
		}, function(_driver, next) {
			if(_driver != null) {
				var driverInfo = _driver.getData();
				var log_driver = new _LogDriver({
					driverId: driverInfo.driverId,
					gcmId: driverInfo.gcmId,
					phone: driverInfo.phone,
					macAddr: driverInfo.macAddr,
					name: driverInfo.name,
					driverLicense: driverInfo.driverLicense,
					insuranceCompanyId: driverInfo.insuranceCompanyId,
					insuranceNumber: driverInfo.insuranceNumber,
					logType: "update_confirm",
					logDate: new Date()
				});
				
				log_driver.save();	
			}
			
			next();
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function registerDriver (req, res) {
	var essentialParams = ["phone", "name", "cert", "birth", "gender", "macAddr", "image","number","automatic"];
	if (svr_utils.checkParams(essentialParams, req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function (next) {
			var driver = new Driver({
				gcmId: req.params.gcmId,
				email : req.params.email,
				phone: req.params.phone,
				name: req.params.name,
				birth: req.params.birth,
				gender: req.params.gender,
				macAddr: req.params.macAddr,
				image: req.params.image,
				worksAt: req.params.worksAt,
				address: req.params.address,
				insurance: req.params.insurance,
				star: 0,
				starnum: 0
			});

			driver.cert = {
				number: req.params.number,
				automatic: req.params.automatic
			}

			driver.save(function (err) {
				next(err, driver);
			});
		}, function (driver, next) {
			res.send({ result: error_code.SUCCESS, driver: driver.getData() });
			next(null, driver);
		}, function(_driver, next) {
			var driverInfo = _driver.getData();
			var log_driver = new _LogDriver({
				driverId: driverInfo.driverId,
				gcmId: driverInfo.gcmId,
				email : driverInfo.email,
				phone: driverInfo.phone,
				name: driverInfo.name,
				birth: driverInfo.birth,
				gender: driverInfo.gender,
				macAddr: driverInfo.macAddr,
				image: driverInfo.image,
				worksAt: driverInfo.worksAt,
				address: driverInfo.address,
				insurance: driverInfo.insurance,
				cert: {
					number: driverInfo.cert.number,
					automatic: driverInfo.cert.automatic
				},
				star: 0,
				starnum: 0,
				logType: "reg_full",
				logDate: new Date()
			});
			
			log_driver.save();
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function EditDriver (req,res) {
	if (svr_utils.checkParams(['driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
	}
	
	Async.waterfall([
		function (next) {
			Driver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function (driver, next) {
			if (!driver) {
				// TODO 예외처리를 해주자.
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}

			if (req.params.hasOwnProperty("address")) driver.address = req.params.address;
			if (req.params.hasOwnProperty("worksAt")) driver.worksAt = req.params.worksAt;
			if (req.params.hasOwnProperty("email")) driver.email = req.params.email;
			if (req.params.hasOwnProperty("insurance")) driver.insurance = req.params.insurance;
			if (req.params.hasOwnProperty("phone")) driver.phone = req.params.phone;
			if (req.params.hasOwnProperty("name")) driver.name = req.params.name;
			if (req.params.hasOwnProperty("birth")) driver.birth = req.params.birth;
			if (req.params.hasOwnProperty("gender")) driver.gender = req.params.gender;
			if (req.params.hasOwnProperty("macAddr")) driver.macAddr = req.params.macAddr;
			if (req.params.hasOwnProperty("image")) driver.image = req.params.image;
			if (req.params.hasOwnProperty("isvaild")) driver.isVaild = req.params.isVaild;
			if (req.params.hasOwnProperty("number")) driver.cert.number = req.params.number;
			if (req.params.hasOwnProperty("automatic")) driver.cert.automatic = req.params.automatic;

			driver.save(function (err) {
				if (err) {
					next(err);
				} else {
					res.send({ result: error_code.SUCCESS, driver: driver.getData() });
					next(null, driver);
				}
			});
		}, function(_driver, next) {
			var driverInfo = _driver.getData();
			var log_driver = new _LogDriver({
				driverId: driverInfo.driverId,
				gcmId: driverInfo.gcmId,
				email : driverInfo.email,
				phone: driverInfo.phone,
				name: driverInfo.name,
				birth: driverInfo.birth,
				gender: driverInfo.gender,
				macAddr: driverInfo.macAddr,
				image: driverInfo.image,
				worksAt: driverInfo.worksAt,
				address: driverInfo.address,
				insurance: driverInfo.insurance,
				cert: {
					number: driverInfo.cert.number,
					automatic: driverInfo.cert.automatic
				},
				star: driverInfo.star,
				starnum: driverInfo.starnum,
				logType: "edit",
				logDate: new Date()
			});
			
			log_driver.save();
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function insertInsuranceCompany(req, res) {
	if (svr_utils.checkParams(['companyName', 'insuranceNumberFormat'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
	}
	
	Async.waterfall([
 		function (next) {
 			InsuranceCompany.find({companyName: req.params.companyName}).exec(next);
 		}, function (company, next) {
 			if (company.length > 0) {
 				res.send({ result: error_code.DUPLICATE_INSURANCE_COMPANY });
				return;
 			} else {
 				var company = new InsuranceCompany({
 					companyName: req.params.companyName,
 					numberFormat : req.params.insuranceNumberFormat,
 					regExp : req.params.regExp
 				});

 				company.save(function (err) {
 					if(err) {
 						next(err);
 					} else {
 						res.send({ result: error_code.SUCCESS });
 						next(null, company);
 					}
 				});
 			}
 		}, function(company, next) {
 			var log_company = new _LogInsuranceCompany({
 				companyName: company.companyName,
 				numberFormat: company.insuranceNumberFormat,
 				regExp : company.regExp,
 				logType: "regist",
 				logDate: new Date()
 			});
 			
 			log_company.save();
 			
 			next();
 		}
 	], function (err) {
 		if (err) errFunction(err, req, res);
 	});
}

exports.initialize = initialize;
exports.registerDriver = registerDriver;
exports.EditDriver = EditDriver;
exports.tempRegisterDriver = tempRegisterDriver;
exports.checkVersion = checkVersion;
exports.updateDriverConfirmData = updateDriverConfirmData;
exports.checkDriverConfirmData = checkDriverConfirmData;
exports.insertInsuranceCompany = insertInsuranceCompany;
exports.logOut = logOut;
