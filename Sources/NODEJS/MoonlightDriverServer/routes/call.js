var Async		= require('async');
var Call		= DB.model('Call');
var Driver      = DB.model('Driver');
var Location    = DB.model('Location');
var CallHistory = DB.model('CallHistory');
var _LogCall	= DB.model('_LogCall');
var ObjectId	= DB.Types.ObjectId;

function initialize (server) {
	server.get('/call', callList);
	server.get('/call/:callId', callOne);
    server.post('/call', createCall);
	server.post('/call/:callId/catch', catchCall);
	server.post('/call/:callId/location/driver', driverLocation);
	server.post('/call/:callId/cancel', cancelCall);
	server.post('/call/:callId/start', startCall);
	server.post('/call/:callId/stop', stopCall);
	server.get('/call/history/list', callHistory);
}

// Call List 가져오는 API이다.
// API : HTTP GET /call
function callList (req, res) {
	if (svr_utils.checkParams(['type', 'lat', 'lng', 'dist'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function (next) {
			var lat = parseFloat(req.params.lat);
		    var lng = parseFloat(req.params.lng);
		    var range = parseFloat(req.params.dist);
		    
			Call.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
				},
				{ 
					"$match": { "status": "wait" }
				}
			], function(err, calls) {
				next(err, calls);
			});
		}, function (calls, next) {
            var retCalls = [];
            for(var i = 0; i < calls.length; i++) {
            	var retCall = {
            		callId:		calls[i]._id,
            		distance:	calls[i].distance,
            		callType:	calls[i].callType,
            		start:		calls[i].start,
            		through:	calls[i].through,
            		end:		calls[i].end,
            		money:		calls[i].money,
            		driver:		calls[i].driver,
            		user:		calls[i].user,
            		isValid:	calls[i].isValid,
            		isStart:	calls[i].isStart,
            		isDone:		calls[i].isDone,
            		byCard:		calls[i].byCard,
            		etc:		calls[i].etc
            	};
            	
            	retCalls.push(retCall);
            }

            // PROFIT.
			res.send({ result: error_code.SUCCESS, list: retCalls });
            next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

// Call 단일 가져오는 API이다.
// API : HTTP GET /call/:callId
function callOne (req, res) {
	if (svr_utils.checkParams(['callId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function (next) {
			Call.findById(new ObjectId(req.params.callId)).exec(next);
		}, function (call, next) {
			if (!call) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
			}

			call = call.getData();
			call.user.realPhone = undefined;
			res.send({ result: error_code.SUCCESS, call: call });
		}
	], function (err) {
        if (err) errFunction(err, req, res);
	});
}

// Call 생성하는 API이다.
// API : HTTP POST /call
function createCall (req, res) {
	var essentialParams = ['phone', 'start_lat', 'start_lng', 'start_text', 'end_lat', 'end_lng', 'end_text', 'money', 'byCard', 'etc'];
	if (svr_utils.checkParams(essentialParams, req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function (next) {
			var through = {
				text: null,
				location: {
					type: "Point",
					coordinates: []
				}
			};
			
			if(req.params.through_text) {
				through.text = req.params.through_text,
				through.location.coordinates = [parseFloat(req.params.through_lng), parseFloat(req.params.through_lat)];
			}
			
			var call = new Call({
				callType: "normal",
				start: {
					text:   req.params.start_text,
					location: {
						type: "Point",
						coordinates: [parseFloat(req.params.start_lng), parseFloat(req.params.start_lat)]
					}
				},
				through: through,
				end: {
					text:   req.params.end_text,
					location: {
						type: "Point",
						coordinates: [parseFloat(req.params.end_lng), parseFloat(req.params.end_lat)]
					}
				},
				money: parseInt(req.params.money),
				byCard: Boolean(parseInt(req.params.byCard)),
				user: {
					realPhone:  req.params.phone,
					phone:      req.params.phone
				},
				driver: {
					driverId: null,
					phone: null
				},
				etc: req.params.etc,
				status: "wait"
			});
			
            call.save(function (err) {
                next(err, call);
            });
        }, function (call, next) {
        	var callInfo = call.getData();
        	
            res.send({ result: error_code.SUCCESS, call: callInfo });
            
            var log_call = new _LogCall({
            	callId: callInfo.callId,
            	callType: callInfo.callType,
            	status: callInfo.status,
				start: {
					text:   callInfo.start.text,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					text: callInfo.through.text,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					text: callInfo.end.text,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				money: callInfo.money,
				byCard: callInfo.byCard,
				user: {
					realPhone:  callInfo.user.realPhone,
					phone:      callInfo.user.phone
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone
				},
				etc: callInfo.etc,
				logType: "create"
            });
            
            log_call.save();
            
            next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

// Call 단일 가져오는 API이다.
// API : HTTP POST /call/:callId/catch
function catchCall (req, res) {
	if (svr_utils.checkParams(['callId', 'driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

//    if (!req.headers['token']) {
//        res.send({ result: error_code.NOT_AUTHORIZED });
//        return;
//    }

	Async.waterfall([
		function (next) {
//			Driver.authorize(req.headers['token']).exec(next);
			Driver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function (driver, next) {
			if (!driver) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}

            // Call 검색!
			Call.findById(new ObjectId(req.params.callId)).exec(function (err, call) {
				next(err, driver, call);
			});
		}, function (driver, call, next) {
			if (!call) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
			}

			if (call.status == "catch") {
				res.send({ result: error_code.ERD_CATCHED_CALL });
				return;
			} else if (call.status == "start") {
				res.send({ result: error_code.START_CALL });
				return;
			} else if (call.status == "end") {
				res.send({ result: error_code.CALL_DONE });
				return;
			}

			call.status = "catch";
			call.driver = {
				driverId: driver._id,
				phone: driver.phone
			};

			call.save(function (err) {
                next(err, call);
            });
		}, function (call, next) {
			res.send({ result: error_code.SUCCESS });
			
			var callInfo = call.getData();
			
			var log_call = new _LogCall({
            	callId: callInfo.callId,
            	callType: callInfo.callType,
            	status: callInfo.status,
				start: {
					text:   callInfo.start.text,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					text: callInfo.through.text,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					text: callInfo.end.text,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				money: callInfo.money,
				byCard: callInfo.byCard,
				user: {
					realPhone:  callInfo.user.realPhone,
					phone:      callInfo.user.phone
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone
				},
				etc: callInfo.etc,
				logType: "catch"
            });
            
            log_call.save();
            
            next();
		}
	], function (err) {
        if (err) errFunction(err, req, res);
	});
}

// 기사의 Location을 등록하는 API이다.
// API : HTTP POST /call/:callId/location/driver
function driverLocation (req, res) {
    if (svr_utils.checkParams(['callId', 'lat', 'lng'], req.params)) {
        res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
    }

    if (!req.headers['token']) {
        res.send({ result: error_code.NOT_AUTHORIZED });
        return;
    }

    Async.waterfall([
        function (next) {
            Driver.authorize(req.headers['token']).exec(next);
        }, function (driver, next) {
            if (!driver) {
                // 토큰에 맞는 기사가 없다 ㅠㅠ
                res.send({ result: error_code.NOT_AUTHORIZED });
                return;
            }

            // Call 검색!
            Call.findById(new ObjectId(req.params.callId)).exec(function (err, call) {
                next(err, driver, call);
            });
        }, function (driver, call, next) {
            if (!call) {
                // 으앙 콜이 없다.
                res.send({ result: error_code.NO_SUCH_CALL });
                return;
            }

            if (call.driver.driverId != driver._id) {
                // 권한도 없는데...
                res.send({ result: error_code.NOT_PERMITTED });
                return;
            }

            if (call.isDone) {
                // 이미 끝난 콜 예외처리.
                res.send({ result: error_code.NOT_PERMITTED });
                return;
            }

            var location = new Location();
            location.lat = parseFloat(req.params.lat);
            location.lng = parseFloat(req.params.lng);
            location.callId = call._id;
            location.userType = true;
            location.typeId = driver._id;
            location.date = new Date().getTime();

            location.save(function (err) {
                // 에러가 있으면 에러를 뿜고, 없으면 사용자 위치를 조회하자.
                if (err) next(err);
                else Location.find({ callId: call._id, userType: false }).sort({ date: -1 }).exec(function (err, locs) {
                    next(err, driver, call, locs);
                });
            });
        }, function (driver, call, locations, next) {
            var result = { result: error_code.SUCCESS };

            if (locations.length >= 0) {
                result['location'] = locations[0].getData();
                result['location']['distance'] = svr_utils.getDistance(parseFloat(req.params.lat), parseFloat(req.params.lng), result['location'].lat, result['location'].lng);
            }

            res.send(result);
            next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

// Call을 Cancel하는 API이다.
// API : HTTP POST /call/:callId/cancel
function cancelCall (req, res) {
	if (svr_utils.checkParams(['callId', 'driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
//	if (!req.headers['token']) {
//		res.send({ result: error_code.NOT_AUTHORIZED });
//		return;
//	}
	
	Async.waterfall([
		function (next) {
//			Driver.authorize(req.headers['token']).exec(next);
			Driver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function (driver, next) {
			if (!driver) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
			
			Call.findById(new ObjectId(req.params.callId)).exec(function (err, call) {
				next(err, driver, call);
			});
		}, function (driver, call, next) {
			if (!call) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
			}
			
			if (call.driver.driverId != driver._id) {
				res.send({ result: error_code.NOT_PERMITTED });
				return;
			}
			
			if (call.status == "wait") {
				res.send({ result: error_code.NOT_CATCH_CALL });
				return;
			} else if (call.status == "start") {
				res.send({ result: error_code.START_CALL });
				return;
			} else if (call.status == "end") {
				res.send({ result: error_code.CALL_DONE });
				return;
			}
			
			// TODO : 벌금 항목을 여기에 넣어주자.
			call.driver = {
				driverId: null,
				phone: null
			};
			
			call.status = "wait";
			call.save(function (err) {
				next(err, call);
			});
		}, function (call, next) {
			res.send({ result: error_code.SUCCESS });
			
			var callInfo = call.getData();
			
			var log_call = new _LogCall({
				callId: callInfo.callId,
				callType: callInfo.callType,
				status: callInfo.status,
				start: {
					text:   callInfo.start.text,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					text: callInfo.through.text,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					text: callInfo.end.text,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				money: callInfo.money,
				byCard: callInfo.byCard,
				user: {
					realPhone:  callInfo.user.realPhone,
					phone:      callInfo.user.phone
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone
				},
				etc: callInfo.etc,
				logType: "cancel"
            });
            
            log_call.save();
            
			next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

// Call의 운행 시작을 알리는 API이다.
// API : HTTP POST /call/:callId/start
function startCall (req, res) {
	if (svr_utils.checkParams(['callId', 'driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
//	if (!req.headers['token']) {
//		res.send({ result: error_code.NOT_AUTHORIZED });
//		return;
//	}
	
	Async.waterfall([
		function (next) {
//			Driver.authorize(req.headers['token']).exec(next);
			Driver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function (driver, next) {
			if (!driver) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
			
			Call.findById(new ObjectId(req.params.callId)).exec(function (err, call) {
				next(err, driver, call);
			});
		}, function (driver, call, next) {
			if (!call) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
			}
			
			if (call.driver.driverId != driver._id) {
				res.send({ result: error_code.NOT_PERMITTED });
				return;
			}
			
			if (call.status == "wait") {
				res.send({ result: error_code.NOT_CATCH_CALL });
				return;
			} else if (call.status == "start") {
				res.send({ result: error_code.START_CALL });
				return;
			} else if (call.status == "end") {
				res.send({ result: error_code.CALL_DONE });
				return;
			}
			
			call.status = "start";
			call.save(function (err) {
				next(err, call);
			});
		}, function (call, next) {
			res.send({ result: error_code.SUCCESS });
			
			var callInfo = call.getData();
			
			var log_call = new _LogCall({
				callId: callInfo.callId,
				callType: callInfo.callType,
				status: callInfo.status,
				start: {
					text:   callInfo.start.text,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					text: callInfo.through.text,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					text: callInfo.end.text,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				money: callInfo.money,
				byCard: callInfo.byCard,
				user: {
					realPhone:  callInfo.user.realPhone,
					phone:      callInfo.user.phone
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone
				},
				etc: callInfo.etc,
				logType: "start"
            });
            
            log_call.save();
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

// Call의 운행 종료를 알리는 API이다.
// API : HTTP POST /call/:callId/stop
function stopCall (req, res) {
	if (svr_utils.checkParams(['callId', 'driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
//	if (!req.headers['token']) {
//		res.send({ result: error_code.NOT_AUTHORIZED });
//		return;
//	}
	
	Async.waterfall([
		function (next) {
//			Driver.authorize(req.headers['token']).exec(next);
			Driver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function (driver, next) {
			if (!driver) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
			
			Call.findById(new ObjectId(req.params.callId)).exec(function (err, call) {
				next(err, driver, call);
			});
		}, function (driver, call, next) {
			if (!call) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
			}
			
			if (call.driver.driverId != driver._id) {
				res.send({ result: error_code.NOT_PERMITTED });
				return;
			}
			
			if (call.status == "wait") {
				res.send({ result: error_code.NOT_CATCH_CALL });
				return;
			} else if (call.status == "catch") {
				res.send({ result: error_code.NOT_START_CALL });
				return;
			} else if (call.status == "end") {
				res.send({ result: error_code.CALL_DONE });
				return;
			}
			
			call.status = "end";
			call.save(function (err) {
				next(err, call);
			});
		}, function (call, next) {
			res.send({ result: error_code.SUCCESS });
			
			var callInfo = call.getData();
			
			var log_call = new _LogCall({
				callId: callInfo.callId,
				callType: callInfo.callType,
				status: callInfo.status,
				start: {
					text:   callInfo.start.text,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					text: callInfo.through.text,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					text: callInfo.end.text,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				money: callInfo.money,
				byCard: callInfo.byCard,
				user: {
					realPhone:  callInfo.user.realPhone,
					phone:      callInfo.user.phone
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone
				},
				etc: callInfo.etc,
				logType: "stop"
            });
            
            log_call.save();
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function callHistory(req, res) {
	if (svr_utils.checkParams(['selectedType'], req.params)) {
        res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
    }

	var lastMonth;
	var thirdLastMonth;

	Async.waterfall([
			function(next) {
				CallHistory.aggregate([
					{$group: {_id: '$date'}},
					{$sort: {_id: -1}},
					{ $limit : 3 }
				], function(err, recentDate) {
					next(err, recentDate);
				});
			},
			function(recentDate, next) {
				lastMonth = recentDate[0]["_id"];
				thirdLastMonth = recentDate[2]["_id"];
				
				var gu_ret = {};
				var dong_ret = {};
				var selectedType = req.params.selectedType;
				var paramMonth = 0;
				if(selectedType == 0) {
					paramMonth = lastMonth;
				} else if(selectedType == 1) {
					paramMonth = thirdLastMonth;
				} else if(selectedType == 2) {
                    var searchYear = new Date().getFullYear() - 1;
					var searchMonth = new Date().getMonth() + 1;
					if(searchMonth < 10) {
                        searchMonth = "0" + searchMonth;
                    }

                    paramMonth = searchYear + "" + searchMonth;
                }
				
				CallHistory.aggregate([
						{
							"$match": {
								"date" : { "$gte": paramMonth }
							}
						},
						{
							"$group": {
								"_id": {
									"si_name": "$si_name",
									"gu_name": "$gu_name",
									"dong_name": "$dong_name"
								},
								"call_count": { "$avg": "$call_count" },
								"count": { "$sum": 1 }
							}
						}
					], function(err, history) {
						next(err, history);
					}
				);
			},
			function(_history, next) {
				var history_count = _history.length;
				var gu_ret = {};
				var dong_ret = {};
				for(var i = 0; i < history_count; i++) {
					var gu_name = _history[i]._id.gu_name;
					var dong_name = gu_name + "_" + _history[i]._id.dong_name;
					
					if(!dong_ret[dong_name]) {
						dong_ret[dong_name] = {
							si_name: _history[i]._id.si_name,
							gu_name: _history[i]._id.gu_name,
							dong_name: _history[i]._id.dong_name,
							call_count: 0
						};
					}
					
					if(!gu_ret[gu_name]) {
						gu_ret[gu_name] = {
							si_name: _history[i]._id.si_name,
							gu_name: _history[i]._id.gu_name,
							call_count: 0
						};
					}

					dong_ret[dong_name].call_count = _history[i].call_count;
					gu_ret[gu_name].call_count = gu_ret[gu_name].call_count + _history[i].call_count;
				}

				res.send({result: error_code.SUCCESS, dong_data: dong_ret, gu_data: gu_ret, total_count: history_count});
				next();
			}
		], function(err) {
			if(err) {
				errFunction(err, req, res);
			}
		}
	);
}

exports.initialize = initialize;
exports.callList = callList;
exports.callOne = callOne;
exports.createCall = createCall;
exports.catchCall = catchCall;
exports.driverLocation = driverLocation;
exports.cancelCall = cancelCall;
exports.startCall = startCall;
exports.stopCall = stopCall;
exports.callHistory = callHistory;
