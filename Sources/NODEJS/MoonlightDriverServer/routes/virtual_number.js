var Async				= require('async');
var ShuttleInfo			= DB.model('ShuttleInfo');
var Driver				= DB.model('Driver');
var User				= DB.model('User');
var ChauffeurCompany	= DB.model('ChauffeurCompany');
var ObjectId			= DB.Types.ObjectId;

function initialize (server) {
	server.post("/virtual_number/register", registerVirtualNumber);
	server.post("/virtual_number/unregister", unregisterVirtualNumber);
}

function registerVirtualNumber(req, res) {
	if(svr_utils.checkParams(["type", "sendId", "recvId"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function (next) {
			if(req.params.type == "shuttle") {
				Driver.findById(new ObjectId(req.params.sendId)).exec(next);
			} else if(req.params.type == "company") {
				User.findById(new ObjectId(req.params.sendId)).exec(next);
			} else {
				res.send({ result: error_code.NOT_ENOUGH_PARAMS });
				return;
			}
 		},
 		function(sender_info, next) {
 			if(!sender_info) {
 				if(req.params.type == "shuttle") {
 					res.send({ result: error_code.NO_SUCH_DRIVER });
 				} else if(req.params.type == "company") {
 					res.send({ result: error_code.NO_SUCH_USER });
 				} else {
 					res.send({ result: error_code.NOT_ENOUGH_PARAMS });
 				}
 				
				return;
 			}
 			
 			if(req.params.type == "shuttle") {
 				ShuttleInfo.findById(new ObjectId(req.params.recvId)).exec(function(err, receiver_info) {
 	 				next(err, receiver_info, sender_info);
 	 			});
 			} else if(req.params.type == "company") {
 				ChauffeurCompany.findById(new ObjectId(req.params.recvId)).exec(function(err, receiver_info) {
 	 				next(err, receiver_info, sender_info);
 	 			});
 			} else {
				res.send({ result: error_code.NOT_ENOUGH_PARAMS });
				return;
			}
 		},
 		function(receiver_info, sender_info, next) {
 			if(!receiver_info) {
 				if(req.params.type == "shuttle") {
 					res.send({ result: error_code.NO_SUCH_SHUTTLE });
 				} else if(req.params.type == "company") {
 					res.send({ result: error_code.NO_SUCH_CHAUFFEUR_COMPANY });
 				} else {
 					res.send({ result: error_code.NOT_ENOUGH_PARAMS });
 				}
				return;
 			}
 			
 			var phone_number;
 			var gcmId;
 			if(req.params.type == "shuttle") {
 				phone_number = receiver_info.phone;
 				if(phone_number) {
 					var phone_number_arr = phone_number.split(",");
 					phone_number = phone_number_arr[0];
 				}
 				
 				gcmId = sender_info.gcmId;
			} else if(req.params.type == "company") {
				phone_number = receiver_info.telNumber;
 				gcmId = sender_info.gcmId;
			} else {
				res.send({ result: error_code.NOT_ENOUGH_PARAMS });
				return;
			}
 			
 			if(phone_number) {
 				var unique_id = svr_utils.getUniqueId();
 				
 				workerPushData[unique_id] = {
 					event_type: "send_register_req",
 					unique_id : unique_id,
 					gcmId : gcmId,
 					org_number : phone_number
 				};
 				
 				redis_pub.publish("virtualNumberReq", JSON.stringify(workerPushData[unique_id]));
// 				process.send({msg_type: 'virtual_number', event_type: "send_register_req", gcmId: gcmId, org_number: phone_number});
 				res.send({ result: error_code.SUCCESS });
 			} else {
 				res.send({ result: error_code.FAILED_REGISTER_VIRTUAL_NUMBER });
 			}
 			
			next();
 		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function unregisterVirtualNumber(req, res) {
	if(svr_utils.checkParams(["type", "sendId", "virtual_number"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function (next) {
			if(req.params.type == "shuttle") {
				Driver.findById(new ObjectId(req.params.sendId)).exec(next);
			} else if(req.params.type == "company") {
				User.findById(new ObjectId(req.params.sendId)).exec(next);
			} else {
				res.send({ result: error_code.NOT_ENOUGH_PARAMS });
				return;
			}
 		},
 		function(sender_info, next) {
 			if(!sender_info) {
 				if(req.params.type == "shuttle") {
 					res.send({ result: error_code.NO_SUCH_DRIVER });
 				} else if(req.params.type == "company") {
 					res.send({ result: error_code.NO_SUCH_USER });
 				} else {
 					res.send({ result: error_code.NOT_ENOUGH_PARAMS });
 				}
 				
				return;
 			}
 			
 			redis_pub.publish("virtualNumberReq", JSON.stringify({event_type: "send_unregister_req", virtual_number: req.params.virtual_number}));
// 			process.send({msg_type: 'virtual_number', event_type: "send_unregister_req", virtual_number: req.params.virtual_number});
			
 			res.send({ result: error_code.SUCCESS });
 			
			next();
 		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

exports.initialize = initialize;
exports.registerVirtualNumber = registerVirtualNumber;
exports.unregisterVirtualNumber = unregisterVirtualNumber;
