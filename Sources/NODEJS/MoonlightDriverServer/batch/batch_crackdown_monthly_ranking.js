global.rootPath				= __dirname + "/..";
global.config				= require(rootPath + '/inc/config');
var schedule				= require('node-schedule');
var DB						= require('../lib/Database');
var Logger					= require(rootPath + '/lib/Logger');
var Crackdown				= DB.model('Crackdown');
var CrackdownRankMonthly	= DB.model('CrackdownRankMonthly');
var SVRUtils				= require(rootPath + '/lib/SVRUtils');

global.logger 		= new Logger(config.LOG.FILE.INFO, config.LOG.FILE.ERROR);
global.svr_utils	= new SVRUtils();

var now = new Date();
var startDate = new Date(now.getFullYear(), now.getMonth() - 1, 1);
var endDate = new Date(now.getFullYear(), now.getMonth(), 1);

Crackdown.aggregate([
	{
		"$match" : {
			"from" : "카대리",
			"$and" : [
				{ "createTime" : {"$gte" : startDate.getTime()} },
				{ "createTime" : {"$lt" : endDate.getTime()} }
			]
		}
	},
	{
		"$group": {
			"_id" : { "userId" : "$userId", "registrant" : "$registrant", "phone" : "$phone"},
			"photoCount" : { 
				"$sum": {
					"$cond" : [
						{ "$eq" : [ "$reportType", "photo" ] }, 1, 0
					]
				}
			},
			"normalCount" : { 
				"$sum": {
					"$cond" : [
						{ "$eq" : [ "$reportType", "normal" ] }, 1, 0
					]
				}
			},
			"voiceCount" : { 
				"$sum": {
					"$cond" : [
						{ "$eq" : [ "$reportType", "voice" ] }, 1, 0
					]
				}
			},
			"point": {
				"$sum" : {
					"$cond" : [
						{ "$eq" : [ "$reportType", "photo" ] }, 2, 1
					]
				}
			}
		}
	}
], function(err, monthlyData) {
	if(err) {
		logger.error(err.stack);
	} else {
		var createTime = new Date().getTime();
		var startDateStr = startDate.getFullYear() + "-" + (startDate.getMonth() + 1  < 10 ? ('0' + (startDate.getMonth() + 1)) : (startDate.getMonth() + 1));
		
		CrackdownRankMonthly.remove({"month" : startDateStr}).exec(function(_err, _count) {
			if(_err) {
				logger.error(err.stack);
			} else {
				if(monthlyData && monthlyData.length > 0) {
					var listSize = monthlyData.length;
					for(var i = 0; i < listSize; i++) {
						var monthlyRankData = new CrackdownRankMonthly({
							month:			startDateStr,
							userId:			monthlyData[i]._id.userId,
							registrant: 	monthlyData[i]._id.registrant,
							phone: 			monthlyData[i]._id.phone,
							photoCount:		monthlyData[i].photoCount,
							normalCount: 	monthlyData[i].normalCount,
							voiceCount: 	monthlyData[i].voiceCount,
							totalCount:		monthlyData[i].photoCount + monthlyData[i].normalCount + monthlyData[i].voiceCount, 
							point:			monthlyData[i].point,
							updateTime:		createTime,
							createTime: 	createTime
						});
						
						(function(monthlyRankData, i) {
							monthlyRankData.save(function(err) {
								if(err) {
									logger.error(err.stack);
								} else {
									logger.info(monthlyRankData.userId + " : " + monthlyRankData.totalCount + "(" + monthlyRankData.point + "point)");
								}
								
								if(i == listSize - 1) {
									process.exit(1);
								}
							});	
						})(monthlyRankData, i);
					}
				} else {
					var loopCount = svr_utils.getRandomInt(3, 5);
					
					for(var i = 0; i < loopCount; i++) {
						var photoCount = svr_utils.getRandomInt(1, 20);
						var normalCount = svr_utils.getRandomInt(1, 20);
						var voiceCount = svr_utils.getRandomInt(1, 20);
						var totalPoint = (photoCount * 2) + (normalCount * 1) + (voiceCount * 1);
						
						var monthlyRankData = new CrackdownRankMonthly({
							month:			startDateStr,
							userId:			"",
							registrant: 	svr_utils.getRandPhoneNumber(),
							phone:			"",
							photoCount:		photoCount,
							normalCount: 	normalCount,
							voiceCount: 	voiceCount,
							totalCount:		photoCount + normalCount + voiceCount, 
							point:			totalPoint,
							updateTime:		createTime,
							createTime: 	createTime
						});
						
						(function(monthlyRankData, i) {
							monthlyRankData.save(function(err) {
								if(err) {
									logger.error(err.stack);
								} else {
									logger.info(monthlyRankData.userId + " : " + monthlyRankData.totalCount + "(" + monthlyRankData.point + "point)");
								}
								
								if(i == loopCount - 1) {
									process.exit(1);
								}
							});	
						})(monthlyRankData, i);
					}
				}
			}
		});
	}
});

process.on("uncaughtException", function (err) {
	logger.error("batch process uncaughtException: " + err.stack);
});
